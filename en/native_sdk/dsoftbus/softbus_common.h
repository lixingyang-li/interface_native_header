/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup SoftBus
 * @{
 *
 * @brief Provides secure, high-speed communications between devices.
 *
 * This module implements unified distributed communication management of nearby devices and provides link-independent device discovery
 * and transmission interfaces to support service publishing and data transmission.
 * @since 1.0
 * @version 1.0
 */

/**
 * @file softbus_common.h
 *
 * @brief Declares common constants and structs of DSoftBus.
 *
 * 
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef SOFTBUS_CLIENT_COMMON_H
#define SOFTBUS_CLIENT_COMMON_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Declares the permission for DSoftBus distributed data synchronization.
 *
 * @since 1.0
 * @version 1.0
*/
#define OHOS_PERMISSION_DISTRIBUTED_DATASYNC "ohos.permission.DISTRIBUTED_DATASYNC"

/**
 * @brief Declares the permission for DSoftBus networking.
 *
 * @since 1.0
 * @version 1.0
*/
#define OHOS_PERMISSION_DISTRIBUTED_SOFTBUS_CENTER "ohos.permission.DISTRIBUTED_SOFTBUS_CENTER"

/**
 * @brief Defines the length of a Bluetooth device MAC address, including the null terminator <b>\0</b>.
 *
 * @since 1.0
 * @version 1.0
 */
#define BT_MAC_LEN 18

/**
 * @brief Defines the length of a DSoftBus network ID, including the null terminator <b>\0</b>.
 *
 * @since 1.0
 * @version 1.0
 */
#define NETWORK_ID_BUF_LEN 65

/**
 * @brief Defines the length of a device UDID, including the null terminator <b>\0</b>.
 *
 * @since 1.0
 * @version 1.0
 */
#define UDID_BUF_LEN 65

/**
 * @brief Defines the length of a UDID hash value.
 *
 * @since 1.0
 * @version 1.0
 */
#define UDID_HASH_LEN 32

/**
 * @brief Defines the length of a UUID, including the null terminator <b>\0</b>.
 *
 * @since 1.0
 * @version 1.0
 */
#define UUID_BUF_LEN 65

/**
 * @brief Defines the maximum length of an IP address, including the null terminator <b>\0</b>. IPv6 addresses are supported.
 *
 * @since 1.0
 * @version 1.0
 */
#define IP_STR_MAX_LEN 46

/**
 * @brief Defines the maximum length of an account hash code.
 *
 * @since 1.0
 * @version 1.0
 */
#define MAX_ACCOUNT_HASH_LEN 96

/**
 * @brief Defines the maximum length of the account ID hash code calculated using SHA-256.
 *
 * @since 1.0
 * @version 1.0
 */
#define SHA_256_HASH_LEN 32

/**
 * @brief Defines the length of the hexadecimal string converted from a hash code calculated using SHA-256.
 *
 */
#define SHA_256_HEX_HASH_LEN 65

/**
 * @brief Defines the maximum length of the capability data.
 *
 * @since 1.0
 * @version 1.0
 */
#define MAX_CAPABILITYDATA_LEN 513

/**
 * @brief Defines the maximum length of custom data.
 *
 * @since 1.0
 * @version 1.0
 */
#define MAX_CUST_DATA_LEN 219

/**
 * @brief Defines the maximum number of bitmap capabilities.
 *
 * @since 1.0
 * @version 1.0
 */
#define MAX_CAPABILITY_NUM 2

/**
 * @brief Defines the maximum length of a device name.
 *
 * @since 1.0
 * @version 1.0
 */
#define MAX_DEVICE_NAME_LEN 65

/**
 * @brief Defines the maximum length of a device ID.
 *
 * @since 1.0
 * @version 1.0
 */
#define MAX_DEVICE_ID_LEN 96

/**
 * @brief Defines the size of the value buffer.
 *
 * @since 1.0
 * @version 1.0
 */
#define NUM_BUF_SIZE 4

/**
 * @brief Defines the maximum length of a node short address.
 *
 * @since 1.0
 * @version 1.0
 */
#define SHORT_ADDRESS_MAX_LEN 8

/**
 * @brief Enumerates the connection address types.
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum {
    CONNECTION_ADDR_WLAN = 0, /**< WLAN */
    CONNECTION_ADDR_BR,       /**< Basic Rate (BR) */
    CONNECTION_ADDR_BLE,      /**< Bluetooth Low Energy (BLE) */
    CONNECTION_ADDR_ETH,      /**< Ethernet */
    CONNECTION_ADDR_MAX       /**< Invalid type */
} ConnectionAddrType;

/**
 * @brief Defines the address of a device added to the Local Neural Network (LNN).
 *
 * @since 1.0
 * @version 1.0
 */
typedef struct {
    ConnectionAddrType type;   /**< Connection address type {@link ConnectionAddrType} */
    /**
     * @brief Defines address Information.
     *
     * @since 1.0
     * @version 1.0
     */
    union {
        /**
         * @brief Defines a BR address.
         *
         * @since 1.0
         * @version 1.0
         */
        struct BrAddr {
            char brMac[BT_MAC_LEN];     /**< BR MAC address */
        } br;
        /**
         * @brief Defines a BLE address.
         *
         * @since 1.0
         * @version 1.0
         */
        struct BleAddr {
            char bleMac[BT_MAC_LEN];          /**< BLE MAC address */
            uint8_t udidHash[UDID_HASH_LEN];  /**< UDID hash value */
        } ble;
        /**
         * @brief Defines an IPv4 or IPv6 address.
         *
         * @since 1.0
         * @version 1.0
         */
        struct IpAddr {
            char ip[IP_STR_MAX_LEN];    /**< IP address */
            uint16_t port;              /**< Port number */
        } ip;
    } info;
    char peerUid[MAX_ACCOUNT_HASH_LEN]; /**< Peer UID */
} ConnectionAddr;

/**
 * @brief Enumerates the DSoftBus device discovery modes.
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum  {
    DISCOVER_MODE_PASSIVE = 0x55,       /**< Passive discovery */
    DISCOVER_MODE_ACTIVE  = 0xAA        /**< Active discovery */
} DiscoverMode;

/**
 * @brief Enumerates the media used for service discovery.
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum {
    AUTO = 0,     /**< Auto */
    BLE = 1,      /**< Bluetooth */
    COAP = 2,     /**< CoAP */
    USB = 3,      /**< USB */
    MEDIUM_BUTT   /**< Invalid value */
} ExchangeMedium;

/**
 * @brief Enumerates the service publishing frequencies.
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum {
    LOW = 0,         /**< Low */
    MID = 1,         /**< Medium */
    HIGH = 2,        /**< High */
    SUPER_HIGH = 3,  /**< Super-high */
    FREQ_BUTT        /**< Invalid value */
} ExchangeFreq;

/**
 * @brief Enumerates the supported capabilities published by a device.
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum {
    HICALL_CAPABILITY_BITMAP = 0,         /**< MeeTime */
    PROFILE_CAPABILITY_BITMAP = 1,        /**< Video reverse connection in the smart domain */
    HOMEVISIONPIC_CAPABILITY_BITMAP = 2,  /**< Gallery in Vision */
    CASTPLUS_CAPABILITY_BITMAP,           /**< Cast+ */
    AA_CAPABILITY_BITMAP,                 /**< Input method in Vision */
    DVKIT_CAPABILITY_BITMAP,              /**< Device virtualization tool package */
    DDMP_CAPABILITY_BITMAP,               /**< Distributed middleware */
    OSD_CAPABILITY_BITMAP                 /**< OSD capability */
} DataBitMap;

/**
 * @brief Defines the mapping between bitmaps and supported capabilities.
 *
 * @since 1.0
 * @version 1.0
 */
typedef struct {
    DataBitMap bitmap;  /**< Bitmap {@link DataBitMap} */
    char *capability;   /**< Capability {@link g_capabilityMap} */
} CapabilityMap;

/**
 * @brief Defines the mapping between bitmaps and capabilities.
 *
 * @since 1.0
 * @version 1.0
 */
static const CapabilityMap g_capabilityMap[] = {
    {HICALL_CAPABILITY_BITMAP, (char *)"hicall"},               /**< Hicall */
    {PROFILE_CAPABILITY_BITMAP, (char *)"profile"},             /**< Profile */
    {HOMEVISIONPIC_CAPABILITY_BITMAP, (char *)"homevisionPic"}, /**< HomeVisionPic */
    {CASTPLUS_CAPABILITY_BITMAP, (char *)"castPlus"},           /**< Cast+ */
    {AA_CAPABILITY_BITMAP, (char *)"aaCapability"},             /**< aaCapability */
    {DVKIT_CAPABILITY_BITMAP, (char *)"dvKit"},                 /**< DVKit */
    {DDMP_CAPABILITY_BITMAP, (char *)"ddmpCapability"},         /**< DDMP */
    {OSD_CAPABILITY_BITMAP, (char *)"osdCapability"},           /**< OSD */
};

/**
 * @brief Defines the published service information.
 *
 * @since 1.0
 * @version 1.0
 */
typedef struct {
    int publishId;                  /**< Service ID */
    DiscoverMode mode;              /**< Service discovery mode {@link Discovermode} */
    ExchangeMedium medium;          /**< Medium used for service publishing {@link ExchangeMedium} */
    ExchangeFreq freq;              /**< Service publishing frequency {@link ExchangeFre} */
    const char *capability;         /**< Published service capability {@link g_capabilityMap} */
    unsigned char *capabilityData;  /**< Capability data */
    unsigned int dataLen;           /**< Maximum length of the capability data */
    bool ranging;                   /**< Whether the distance of the discovered device is reported */
} PublishInfo;

/**
 * @brief Defines the service subscription information.
 *
 * @since 1.0
 * @version 1.0
 */
typedef struct {
    int subscribeId;                /**< Service ID */
    DiscoverMode mode;              /**< Service discovery mode {@link Discovermode} */
    ExchangeMedium medium;          /**< Medium used for service publishing {@link ExchangeMedium} */
    ExchangeFreq freq;              /**< Service publishing frequency {@link ExchangeFre} */
    bool isSameAccount;             /**< Whether to discover only devices with the same account */
    bool isWakeRemote;              /**< Whether to discover dormant devices */
    const char *capability;         /**< Capability {@link g_capabilityMap} of the device to discover */
    unsigned char *capabilityData;  /**< Capability data subscribed */
    unsigned int dataLen;           /**< Maximum length of the capability data */
} SubscribeInfo;

/**
 * @brief Enumerates the DSoftBus heartbeat cycles.
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum {
    HIGH_FREQ_CYCLE = 30,      /**< 30 seconds */
    MID_FREQ_CYCLE = 60,       /**< 60 seconds */
    LOW_FREQ_CYCLE = 5 * 60,   /**< 5 * 60 seconds */
} ModeCycle;

/**
 * @brief Enumerates the durations of a single heartbeat.
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum {
    DEFAULT_DURATION = 60,     /**< 60 seconds */
    NORMAL_DURATION = 10 * 60, /**< 10 * 60 seconds */
    LONG_DURATION = 30 * 60,   /**< 30 * 60 seconds */
} ModeDuration;

/**
 * @brief Defines the device type.
 *
 * @since 1.0
 * @version 1.0
 */
typedef enum {
    SMART_SPEAKER = 0x00, /**< Smart speaker */
    DESKTOP_PC,           /**< PC */
    LAPTOP,               /**< Laptop */
    SMART_PHONE,          /**< Smartphone */
    SMART_PAD,            /**< Tablet */
    SMART_WATCH,          /**< Smart watch */
    SMART_CAR,            /**< Smart car */
    CHILDREN_WATCH,       /**< Smart watch for kids */
    SMART_TV,             /**< Smart TV */
} DeviceType;

/**
 * @brief Defines the device information.
 *
 * @since 1.0
 * @version 1.0
 */
typedef struct {
    char devId[MAX_DEVICE_ID_LEN];        /**< Device ID. */
    char accountHash[MAX_ACCOUNT_HASH_LEN];    /**< Account hash value. */
    DeviceType devType;                        /**< Device type. For details, see {@link DISC_MAX_DEVICE_NAME_LEN}. */
    char devName[MAX_DEVICE_NAME_LEN];    /**< Device name. */
    bool isOnline;                             /**< Whether the device is online. */
    unsigned int addrNum;                      /**< Number of connected addressed. */
    ConnectionAddr addr[CONNECTION_ADDR_MAX];  /**< Connection address information {@link ConnectionAddr}. */
    unsigned int capabilityBitmapNum;          /**< Number of capabilities. */
    unsigned int capabilityBitmap[MAX_CAPABILITY_NUM];   /**< Capability bitmap. */
    char custData[MAX_CUST_DATA_LEN];     /**< Custom data. */
    int32_t range;                             /**< Distance of the discovered device, in cm. */
} DeviceInfo;

/**
 * @brief Defines the additional device information for internal use.
 *
 * @since 1.0
 * @version 1.0
 */
typedef struct {
    ExchangeMedium medium; /**< Medium used for device discovery. For details, see {@link ExchangeMedium}. */
} InnerDeviceInfoAddtions;

#ifdef __cplusplus
}
#endif
#endif
/** @} */
