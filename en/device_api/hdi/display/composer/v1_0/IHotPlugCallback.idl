/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief Defines driver interfaces of the display module.
 *
 * This module provides driver interfaces for upper-layer graphics services, including layer management, device control, and display buffer management.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IHotPlugCallback.idl
 *
 * @brief Declares the callback for hot plug events.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the display module interfaces.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.composer.v1_0;

import ohos.hdi.display.composer.v1_0.DisplayComposerType;

 /**
 * @brief Defines a callback for hot plug events.
 *
 * @since 3.2
 * @version 1.0
 */

[callback] interface IHotPlugCallback {
    OnHotPlug([in] unsigned int outputId, [in] boolean connected);
}
/** @} */
