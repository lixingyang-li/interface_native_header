/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief Provides unified, standard USB driver APIs to implement USB driver access.
 *
 * Using the APIs provided by the USB driver module, USB service developers can implement the following functions:
 * enabling/disabling a device, obtaining a device descriptor, obtaining a file descriptor, enabling/disabling an interface, 
 * reading/writing data in bulk mode, setting or obtaining device functions, and binding or unbinding a subscriber.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IUsbdSubscriber.idl
 *
 * @brief Defines subscribers for USB driver APIs.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the USB driver module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.usb.v1_0;

import ohos.hdi.usb.v1_0.UsbTypes;

/**
 * @brief Defines a subscriber for the USB driver.
 *
 * When a USB device is connected to or disconnected, <b>DeviceEvent</b> will be called.
 * When the status of a USB port changes, <b>PortChangedEvent</b> will be called.
 */
[callback] interface IUsbdSubscriber {
    /**
     * @brief Defines a device status change event.
     *
     * @param UsbInfo USB device information.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    DeviceEvent([in] struct USBDeviceInfo info);

    /**
     * @brief Defines a port change event.
     *
     * @param info USB port information.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    PortChangedEvent([in] struct PortInfo info);
}
/** @} */
