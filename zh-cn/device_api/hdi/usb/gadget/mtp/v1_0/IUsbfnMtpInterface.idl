/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiUsbfnMtp
 * @{
 *
 * @brief 为mtp服务提供统一的API，以访问usb mtp/ptp驱动程序。
 *
 * mtp服务可以获得usb mtp/ptp驱动程序对象或代理，然后调用该对象或代理提供的API来传输不同类型的mtp/ptp数据包。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IUsbfnMtpInterface.idl
 *
 * @brief 声明usb模块提供的API，用于获取usb信息、订阅或取消订阅usb数据、启用或禁用usb、设置usb数据报告模式以及设置准确性和测量范围等usb选项。
 *
 * 模块包路径：ohos.hdi.usb.gadget.mtp.v1_0
 *
 * 引用：ohos.hdi.usb.gadget.mtp.v1_0.UsbfnMtpTypes
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.usb.gadget.mtp.v1_0;

import ohos.hdi.usb.gadget.mtp.v1_0.UsbfnMtpTypes;

/**
 * @brief 定义在usb上执行基本操作的函数。
 *
 * 操作包括获取usb信息、订阅或取消订阅usb数据、启用或禁用usb、设置usb数据报告模式以及设置准确性和测量范围等usb选项。
 *
 * @since 4.0
 * @version 1.0
 */
interface IUsbfnMtpInterface {

    /**
     * @brief 打开USB MTP/PTP驱动程序。
     *
     * @param 无需参数。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    Start();

    /**
     * @brief 关闭USB MTP/PTP驱动程序。
     *
     * @param 无需参数。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    Stop();

    /**
     * @brief 通过USB MTP/PTP驱动程序读取数据。
     *
     * @param data表示USB MTP/PTP驱动程序读取的数据。
     *
     * @return 如果操作失败，则返回读取的字节数、-1或其他负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Read([out] unsigned char[] data);

    /**
     * @brief 通过USB MTP/PTP驱动程序写入数据。
     *
     * @param 通过USB MTP/PTP驱动程序写入数据。data表示数据写入USB MTP/PT驱动程序。
     *
     * @return 如果操作失败，则返回写入的字节数、<b>-1</b>或其他负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Write([in] unsigned char[] data);

    /**
     * @brief 通过USB MTP/PTP驱动程序接收文件。
     *
     * 代理处理文件管理，包括fopen/fclose/feek/fread/fwrite和偏移量信息，Stub负责数据处理。
     *
     * @param mfs 指示mtp文件切片信息。
     *
     * @return 如果接收成功，则返回<b>0</b>；如果操作失败，则返回<b>-1</b>或其他负值。
     *
     * @since 4.0
     * @version 1.0
     */
    ReceiveFile([in] struct UsbFnMtpFileSlice mfs);

    /**
     * @brief 通过USB MTP/PTP驱动程序发送文件。
     *
     * 代理处理文件管理，包括fopen/fclose/feek/fread/fwrite和偏移量信息，Stub负责数据处理。
     *
     * @param mfs 指示mtp文件范围信息，使用的是数据包标头。
     *
     * @return 如果接收成功，则返回<b>0</b>；如果操作失败，则返回<b>-1</b>或其他负值。
     *
     * @since 4.0
     * @version 1.0
     */
    SendFile([in] struct UsbFnMtpFileSlice mfs);

    /**
     * @brief 通过USB MTP/PTP驱动程序发送事件数据。
     *
     * @param data 指示事件数据写入USB MTP/PTP驱动程序。
     *
     * @return 如果接收成功，则返回<b>0</b>；如果操作失败，则返回<b>-1</b>或其他负值。
     *
     * @since 4.0
     * @version 1.0
     */
    SendEvent([in] unsigned char[] eventData);

    /**
     * @brief 初始化USB MTP/PTP驱动程序。由usb_host使用。
     *
     * @param 无需参数。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    Init();

    /**
     * @brief 释放USB MTP/PTP驱动程序。由usb_host使用。
     *
     * @param 无需参数。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    Release();
}
/** @} */
