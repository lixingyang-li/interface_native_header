/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiUsbfnMtp
 * @{
 *
 * @brief 为mtp服务提供统一的API，以访问usb mtp/ptp驱动程序。
 *
 * mtp服务可以获得usb mtp/ptp驱动程序对象或代理，然后调用该对象或代理提供的API来传输不同类型的mtp/ptp数据包。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file UsbfnMtpTypes.idl
 *
 * @brief 定义usb模块使用的数据，包括usb信息，并上报了usb数据。
 *
 * 模块包路径：ohos.hdi.usb.gadget.mtp.v1_0
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.usb.gadget.mtp.v1_0;

/**
 * @brief 标准设备描述符，对应于USB协议中的<b>标准设备描述符</b>。
 *
 * @since 4.0
 * @version 1.0
 */
struct UsbFnMtpFileSlice {
    /** 文件描述符。 */
    FileDescriptor fd;
    /** 管道。 */
    long offset;
    /** 数据长度 */
    long length;
    /** 请求命令 */
    unsigned short command;
    /** 传输id */
    unsigned int transactionId;
};
/** @} */