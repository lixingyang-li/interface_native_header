/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief 提供统一的USB驱动标准接口，实现USB驱动接入。
 *
 * 上层USB服务开发人员可以根据USB驱动模块提供的标准接口获取如下功能：打开/关闭设备，获取设备描述符，获取文件描述符，打开/关闭接口，批量读取/写入数据，
 * 设置/获取设备功能，绑定/解绑订阅者等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file UsbTypes.idl
 *
 * @brief USB驱动相关的数据类型。
 *
 * 模块包路径：ohos.hdi.usb.v1_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.usb.v1_0;

/**
 * @brief USB设备端口信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct PortInfo {
    /** USB设备端口ID。 */
    int portId;
    /** USB设备电源角色。 */
    int powerRole;
    /** USB设备数据角色。 */
    int dataRole;
    /** USB设备模式。 */
    int mode;
};

/**
 * @brief USB设备地址信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct UsbDev {
    /** USB设备总线编号。 */
    unsigned char busNum;
    /** USB设备地址。 */
    unsigned char devAddr;
};

/**
 * @brief USB设备管道信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct UsbPipe {
    /** USB设备接口ID。 */
    unsigned char intfId;
    /** USB设备端点ID。 */
    unsigned char endpointId;
};

/**
 * @brief USB设备控制传输信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct UsbCtrlTransfer {
    /** 请求类型。 */
    int requestType;
    /** 请求命令。 */
    int requestCmd;
    /** 请求值。 */
    int value;
    /** 索引值。 */
    int index;
    /** 超时时间。 */
    int timeout;
};

/**
 * @brief USB设备信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct USBDeviceInfo {
    /** USB设备状态。 */
    int status;
    /** USB设备总线编号。 */
    int busNum;
    /** USB设备编号。 */
    int devNum;
};
/** @} */
