/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Sensor
 * @{
 *
 * @brief 传感器设备驱动对传感器服务提供通用的接口能力。
 *
 * 模块提供传感器服务对传感器驱动访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，实现获取传感器设备信息、订阅/取消订阅传感器数据、
 * 使能/去使能传感器、设置传感器模式、设置传感器精度、量程等可选配置等。
 *
 * @since 2.2
 * @version 1.0
 */

/**
 * @file ISensorCallback.idl
 *
 * @brief Sensor模块为Sensor服务提供数据上报的回调函数。
 *
 * 模块包路径：ohos.hdi.sensor.v1_0
 *
 * 引用：ohos.hdi.sensor.v1_0.SensorTypes
 *
 * @since 2.2
 * @version 1.0
 */

package ohos.hdi.sensor.v1_0;

import ohos.hdi.sensor.v1_0.SensorTypes;

/**
 * @brief 定义用于上报传感器数据的回调函数。
 *
 * 传感器用户订阅传感器数据，只在使能传感器后，传感器数据订阅者才能接收传感器数据。详见{@link ISensorInterface}。
 *
 * @since 2.2
 * @version 1.0
 */
[callback] interface ISensorCallback {
    /**
    * @brief 定义上报传感器数据的功能。
    *
    * @param event 上报的传感器数据，详见{@link HdfSensorEvents}。
    * 
    * @return 如果操作成功，则返回0。
    * @return 如果操作失败，则返回负值。
    *
    * @since 2.2
    * @version 1.0
    */
    OnDataEvent([in] struct HdfSensorEvents event);
}
/** @} */
