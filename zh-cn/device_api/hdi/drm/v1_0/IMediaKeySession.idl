/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiDrm
 * @{

 * @brief DRM模块接口定义。
 *
 * DRM是数字版权管理，用于对多媒体内容加密，以便保护价值内容不被非法获取，
 * DRM模块接口定义了DRM实例管理、DRM会话管理、DRM内容解密的接口。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IMediaKeySession.idl
 *
 * @brief 定义了DRM会话功能接口。
 *
 * 模块包路径：ohos.hdi.drm.v1_0
 *
 * 引用：
 * - ohos.hdi.drm.v1_0.MediaKeySystemTypes
 * - ohos.hdi.drm.v1_0.IMediaKeySessionCallback
 * - ohos.hdi.drm.v1_0.IMediaDecryptModule
 *
 * @since 4.1
 * @version 1.0
 */


package ohos.hdi.drm.v1_0;

import ohos.hdi.drm.v1_0.MediaKeySystemTypes;
import ohos.hdi.drm.v1_0.IMediaKeySessionCallback;
import ohos.hdi.drm.v1_0.IMediaDecryptModule;

/**
* @brief DRM会话功能接口，获取/设置许可证、查询当前会话许可证状态、删除当前会话许可证、释放许可证、恢复离线许可证、
* 判断是否需要安全内存中解密、获取解密模块实例、设置会话消息通知接口、销毁会话。
*
* @since 4.1
* @version 1.0
*/
interface IMediaKeySession {
    /**
     * @brief 产生获取许可证请求。
     *
     * @param mediaKeyRequestInfo 用于产生许可证请求的初始信息。
     * @param mediaKeyRequest 许可证请求。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GenerateMediaKeyRequest([in] struct MediaKeyRequestInfo mediaKeyRequestInfo, [out] struct MediaKeyRequest mediaKeyRequest);

    /**
     * @brief 处理许可证响应。
     *
     * @param mediaKeyResponse 待处理的许可证响应。
     * @param mediaKeyId 对于离线许可证类型，表示索引；在线许可证类型下，值为空。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    ProcessMediaKeyResponse([in] unsigned char[] mediaKeyResponse, [out] unsigned char[] mediaKeyId);

    /**
     * @brief 查询许可证状态（包括策略等信息）。
     *
     * @param mediaKeyStatus 许可证状态。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    CheckMediaKeyStatus([out] Map<String, String> mediaKeyStatus);

    /**
     * @brief 删除许可证。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    ClearMediaKeys();

    /**
     * @brief 产生离线许可证释放请求。
     *
     * @param mediaKeyId 离线许可证索引。
     * @param releaseRequest 离线许可证释放请求。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetOfflineReleaseRequest([in] unsigned char[] mediaKeyId, [out] unsigned char[] releaseRequest);

    /**
     * @brief 处理离线许可证释放响应。
     *
     * @param mediaKeyId 离线许可证索引。
     * @param response 离线许可证释放响应。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    ProcessOfflineReleaseResponse([in] unsigned char[] mediaKeyId, [in] unsigned char[] response);

    /**
     * @brief 恢复离线许可证至当前会话。
     *
     * @param mediaKeyId 离线许可证索引。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    RestoreOfflineMediaKeys([in] unsigned char[] mediaKeyId);

    /**
     * @brief 获取当前DRM会话的内容保护级别。
     *
     * @param level 内容保护级别。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetContentProtectionLevel([out] enum ContentProtectionLevel level);

    /**
     * @brief 判断是否需要安全内存用于存放解密后的数据。
     *
     * @param mimeType 待解密内容的MIME类型。
     * @param required 布尔值表示是否需要安全内存，true表示需要安全内存存储解密后的视频帧，
     * flase表示不需要安全内存存储解密后的视频帧。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    RequiresSecureDecoderModule([in] String mimeType, [out] boolean required);

    /**
     * @brief 设置DRM会话事件通知接口。
     *
     * @param sessionCallback DRM实例事件通知接口。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    SetCallback([in] IMediaKeySessionCallback sessionCallback);

    /**
     * @brief 获取解密模块实例。
     *
     * @param decryptModule 解密模块实例。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetMediaDecryptModule([out] IMediaDecryptModule decryptModule);

    /**
     * @brief 销毁DRM会话。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    Destroy();
};
/** @} */