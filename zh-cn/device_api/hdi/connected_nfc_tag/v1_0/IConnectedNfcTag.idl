/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiConnectedNfcTag
 * @{
 *
 * @brief 为nfc服务提供统一的访问nfc驱动的接口。
 *
 * 通过ConnectedNfcTag服务获取到ConnectedNFCTag驱动对象或者代理提供的API访问ConnectedNFCTag设备，这些API包含可以初始化
 * 或者去初始化ConnectedNfcTag驱动，往nfc卡中读写Ndef内容。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IConnectedNfcTag.idl
 *
 * @brief 读写NFC Tag的接口定义文件
 *
 * 模块包路径：ohos.hdi.connected_nfc_tag.v1_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.connected_nfc_tag.v1_0;

/**
 * @brief 声明ConnectedNfcTag模块提供的API，这些API包含初始化或者去初始化ConnectedNfcTag驱动，往nfc卡中读写Ndef内容。
 *
 * @since 3.2
 * @version 1.0
 */

interface IConnectedNfcTag {
    /**
     * @brief 初始化ConnecteNfcTag驱动。
     *
     * @return 初始化成功返回0，否则返回初始化失败原因。
     *
     * @since 3.2
     * @version 1.0
     */
    Init();

    /**
     * @brief 去初始化ConnecteNfcTag驱动。
     *
     * @return 去初始化成功返回0，否则返回去初始化失败原因。
     *
     * @since 3.2
     * @version 1.0
     */
    Uninit();

    /**
     * @brief 从连接的NFC卡中读取NDEF内容。
     *
     * @param ndefData 返回NDEF内容字符串。
     *
     *
     * @since 3.2
     * @version 1.0
     */
    ReadNdefTag([out] String ndefData);

    /**
     * @brief 往已连接的NFC卡中写入NDEF数据。
     * 
     * @param 待写入的NDEF数据字符串。
     * @return 写入成功返回0,否则返回写失败原因。
     *
     * @since 3.2
     * @version 1.0
     */
    WriteNdefTag([in] String ndefData);
}
/** @} */