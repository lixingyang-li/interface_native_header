/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup power
 * @{
 *
 * @brief 提供休眠/唤醒操作、订阅休眠/唤醒状态、运行锁管理的接口。
 *
 * 电源模块为电源服务提供的休眠/唤醒操作、订阅休眠/唤醒状态和运行锁管理的接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口对设备进行休眠/唤醒、订阅休眠/唤醒状态和管理运行锁。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file IPowerInterface.idl
 *
 * @brief 休眠/唤醒操作、订阅休眠/唤醒状态、运行锁管理的接口。
 *
 * 电源模块为电源服务提供休眠/唤醒操作、订阅休眠/唤醒状态和运行锁管理的接口。
 *
 * 模块包路径：ohos.hdi.power.v1_1
 *
 * 引用：
 * - ohos.hdi.power.v1_1.IPowerHdiCallback
 * - ohos.hdi.power.v1_1.PowerTypes
 * - ohos.hdi.power.v1_1.RunningLockTypes
 *
 * @since 3.1
 * @version 1.0
 */

package ohos.hdi.power.v1_1;

import ohos.hdi.power.v1_1.IPowerHdiCallback;
import ohos.hdi.power.v1_1.PowerTypes;
import ohos.hdi.power.v1_1.RunningLockTypes;

/**
 * @brief 休眠/唤醒操作、订阅休眠/唤醒状态、运行锁管理的接口。
 *
 * 
 * @since 3.1
 */
interface IPowerInterface {
    /**
     * @brief 注册休眠/唤醒状态的回调。
     *
     * @param ipowerHdiCallback 输入参数，服务注册的回调。
     *
     * @return HDF_SUCCESS 表示注册成功。
     * @return HDF_FAILED 表示注册失败。
     *
     * @see IPowerHdiCallback
     *
     * @since 3.1
     */
    RegisterCallback([in] IPowerHdiCallback ipowerHdiCallback);

    /**
     * @brief 执行设备休眠操作。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    StartSuspend();

    /**
     * @brief 执行设备唤醒操作。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    StopSuspend();

    /**
     * @brief 执行设备强制休眠操作。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    ForceSuspend();

    /**
     * @brief 打开运行锁，阻止休眠。
     *
     * @param name 输入参数，运行锁的名称。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     * @deprecated 从4.0版本起废弃，使用{@link WriteWakeCount}替代。
     */
    SuspendBlock([in] String name);

    /**
     * @brief 关闭运行锁，取消阻止休眠。
     *
     * @param name 输入参数，运行锁的名称。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     * @deprecated 从4.0版本起废弃，使用{@link WriteWakeCount}替代。
     */
    SuspendUnblock([in] String name);

    /**
     * @brief 获取电源的Dump信息。
     *
     * @param info 输出参数，电源的Dump信息。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    PowerDump([out] String info);

    /**
     * @brief 持有运行锁，阻止设备休眠。
     *
     * @param info 输入参数，运行锁信息。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 4.0
     */
    HoldRunningLock([in] struct RunningLockInfo info);

    /**
     * @brief 解除运行锁，解除设备休眠。
     *
     * @param info 输入参数，运行锁信息。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 4.0
     */
    UnholdRunningLock([in] struct RunningLockInfo info);
}
/** @} */
