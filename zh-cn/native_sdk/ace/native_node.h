/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧的UI能力，如UI组件创建销毁、树节点操作，属性设置，事件监听等。
 *
 * @since 12
 */

/**
 * @file native_node.h
 *
 * @brief 提供NativeNode接口的类型定义。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef ARKUI_NATIVE_NODE_H
#define ARKUI_NATIVE_NODE_H

#include "native_type.h"
#include "ui_input_event.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_NODE_SCOPE_NUM 1000

/**
 * @brief 提供ArkUI在Native侧可创建组件类型。
 *
 * @since 12
 */
typedef enum {
    /** 自定义节点。 */
    ARKUI_NODE_CUSTOM = 0,
    /** 文本。 */
    ARKUI_NODE_TEXT = 1,
    /** 文本段落。 */
    ARKUI_NODE_SPAN = 2,
    /** 文本图片段落。 */
    ARKUI_NODE_IMAGE_SPAN = 3,
    /** 图片。 */
    ARKUI_NODE_IMAGE = 4,
    /** 状态开关。 */
    ARKUI_NODE_TOGGLE = 5,
    /** 等待图标。 */
    ARKUI_NODE_LOADING_PROGRESS = 6,
    /** 单行文本输入。 */
    ARKUI_NODE_TEXT_INPUT = 7,
    /** 多行文本。 */
    ARKUI_NODE_TEXT_AREA = 8,
    /** 按钮。 */
    ARKUI_NODE_BUTTON = 9,
    /** 进度条。 */
    ARKUI_NODE_PROGRESS = 10,
    /** 复选框。 */
    ARKUI_NODE_CHECKBOX = 11,
    /** XComponent。 */
    ARKUI_NODE_XCOMPONENT = 12,
    /** 日期选择器组件。 */
    ARKUI_NODE_DATE_PICKER = 13,
    /** 时间选择组件。 */
    ARKUI_NODE_TIME_PICKER = 14,
    /** 滑动选择文本内容的组件。 */
    ARKUI_NODE_TEXT_PICKER = 15,
    /** 日历选择器组件。*/
    ARKUI_NODE_CALENDAR_PICKER = 16,
    /** 滑动条组件 */
    ARKUI_NODE_SLIDER = 17,
    /** 单选框 */
    ARKUI_NODE_RADIO = 18,
    /** 堆叠容器。 */
    ARKUI_NODE_STACK = MAX_NODE_SCOPE_NUM,
    /** 翻页容器。 */
    ARKUI_NODE_SWIPER,
    /** 滚动容器。 */
    ARKUI_NODE_SCROLL,
    /** 列表。 */
    ARKUI_NODE_LIST,
    /** 列表项。 */
    ARKUI_NODE_LIST_ITEM,
    /** 列表item分组。 */
    ARKUI_NODE_LIST_ITEM_GROUP,
    /** 垂直布局容器。 */
    ARKUI_NODE_COLUMN,
    /** 水平布局容器。 */
    ARKUI_NODE_ROW,
    /** 弹性布局容器。 */
    ARKUI_NODE_FLEX,
    /** 刷新组件。 */
    ARKUI_NODE_REFRESH,
    /** 瀑布流容器。 */
    ARKUI_NODE_WATER_FLOW,
    /** 瀑布流子组件。 */
    ARKUI_NODE_FLOW_ITEM,
} ArkUI_NodeType;

/**
 * @brief 定义{@link setAttribute}函数通用入参结构。
 *
 * @since 12
 */
typedef struct {
    /** 数字类型数组。*/
    const ArkUI_NumberValue* value;
    /** 数字类型数组大小。*/
    int32_t size;
    /** 字符串类型。*/
    const char* string;
    /** 对象类型。*/
    void* object;
} ArkUI_AttributeItem;

/**
 * @brief 定义ArkUI在Native侧可以设置的属性样式集合。
 *
 * @since 12
 */
typedef enum {
    /**
     * @brief 宽度属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：宽度数值，单位为vp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：宽度数值，单位为vp；\n
     *
     */
    NODE_WIDTH = 0,
    /**
     * @brief 高度属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：高度数值，单位为vp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：高度数值，单位为vp；\n
     *
     */
    NODE_HEIGHT,
    /**
     * @brief 背景色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：背景色数值，0xargb格式，形如 0xFFFF0000 表示红色；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：背景色数值，0xargb格式，形如 0xFFFF0000 表示红色；\n
     *
     */
    NODE_BACKGROUND_COLOR,
    /**
     * @brief 背景色图片属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string: 图片地址；\n
     * .value[0]?.i32：可选值，repeat参数，参数类型{@link ArkUI_ImageRepeat}，默认值为ARKUI_IMAGE_REPEAT_NONE；\n
     * .object：PixelMap 图片数据，参数类型为{@link ArkUI_DrawableDesciptor}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string: 图片地址；\n
     * .value[0].i32：repeat参数，参数类型{@link ArkUI_ImageRepeat}；\n
     * .object：PixelMap 图片数据， 参数类型为{@link ArkUI_DrawableDesciptor}；.object参数和.string参数二选一，不可同时设置。\n
     *
     */
    NODE_BACKGROUND_IMAGE,
    /**
     * @brief 内间距属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式有两种：\n
     * 1：上下左右四个位置的内间距值相等。\n
     * .value[0].f32：内间距数值，单位为vp；\n
     * 2：分别指定上下左右四个位置的内间距值。\n
     * .value[0].f32：上内间距数值，单位为vp；\n
     * .value[1].f32：右内间距数值，单位为vp；\n
     * .value[2].f32：下内间距数值，单位为vp；\n
     * .value[3].f32：左内间距数值，单位为vp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：上内间距数值，单位为vp；\n
     * .value[1].f32：右内间距数值，单位为vp；\n
     * .value[2].f32：下内间距数值，单位为vp；\n
     * .value[3].f32：左内间距数值，单位为vp；\n
     *
     */
    NODE_PADDING,
    /**
     * @brief 组件ID属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string: ID的内容；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string: ID的内容；\n
     *
     */
    NODE_ID,
    /**
     * @brief 设置组件是否可交互，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示不可交互，true表示可交互；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：0表示不可交互，1表示可交互；\n
     *
     */
    NODE_ENABLED,
    /**
     * @brief 外间距属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式有两种：\n
     * 1：上下左右四个位置的外间距值相等。\n
     * .value[0].f32：外间距数值，单位为vp；\n
     * 2：分别指定上下左右四个位置的外间距值。\n
     * .value[0].f32：上外间距数值，单位为vp；\n
     * .value[1].f32：右外间距数值，单位为vp；\n
     * .value[2].f32：下外间距数值，单位为vp；\n
     * .value[3].f32：左外间距数值，单位为vp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：上外间距数值，单位为vp；\n
     * .value[1].f32：右外间距数值，单位为vp；\n
     * .value[2].f32：下外间距数值，单位为vp；\n
     * .value[3].f32：左外间距数值，单位为vp；\n
     *
     */
    NODE_MARGIN,
    /**
     * @brief 设置组件平移，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： x轴移动距离，单位vp，默认值0；\n
     * .value[1].f32： y轴移动距离，单位vp，默认值0；\n
     * .value[2].f32： z轴移动距离，单位vp，默认值0。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32： x轴移动距离，单位vp；\n
     * .value[1].f32： y轴移动距离，单位vp；\n
     * .value[2].f32： z轴移动距离，单位vp。\n
     *
     */
    NODE_TRANSLATE,
    /**
     * @brief 设置组件缩放，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： x轴的缩放系数，默认值1；\n
     * .value[1].f32： y轴的缩放系数，默认值1。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32： x轴的缩放系数；\n
     * .value[1].f32： y轴的缩放系数。\n
     *
     */
    NODE_SCALE,
    /**
     * @brief 设置组件旋转，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 旋转轴向量x坐标，默认值0；\n
     * .value[1].f32： 旋转轴向量y坐标，默认值0；\n
     * .value[2].f32： 旋转轴向量z坐标，默认值0；\n
     * .value[3].f32： 旋转角度，默认值0；\n
     * .value[4].f32： 视距，即视点到z=0平面的距离，单位vp，默认值0。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 旋转轴向量x坐标；\n
     * .value[1].f32： 旋转轴向量y坐标；\n
     * .value[2].f32： 旋转轴向量z坐标；\n
     * .value[3].f32： 旋转角度；\n
     * .value[4].f32： 视距，即视点到z=0平面的距离，单位vp。\n
     *
     */
    NODE_ROTATE,
    /**
     * @brief 设置组件高光效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 亮度值，默认值1.0，推荐取值范围[0,2]。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 亮度值。\n
     *
     */
    NODE_BRIGHTNESS,
    /**
     * @brief 设置组件饱和度效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 饱和度值，默认值1.0，推荐取值范围[0,FLT_MAX]。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 饱和度值。\n
     *
     */
    NODE_SATURATION,
    /**
     * @brief 设置组件内容模糊效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 模糊半径，模糊半径越大越模糊，为0时不模糊。单位vp，默认值0.0。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 模糊半径，模糊半径越大越模糊，为0时不模糊。单位vp。\n
     *
     */
    NODE_BLUR,
    /**
     * @brief 设置组件颜色渐变效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32： 线性渐变的起始角度。0点方向顺时针旋转为正向角度，默认值：180； \n
     * .value[1].i32：线性渐变的方向，设置angle后不生效。数据类型{@link ArkUI_LinearGradientDirection} \n
     * .value[2].i32： 为渐变的颜色重复着色，默认值 false。 \n
     * .object: 指定某百分比位置处的渐变色颜色，设置非法颜色直接跳过： \n
     * colors：渐变色颜色颜色。 \n
     * stops：渐变位置。 \n
     * size：颜色个数。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 线性渐变的起始角度。\n
     * .value[1].i32：线性渐变的方向，设置angle后不生效。\n
     * .value[2].i32： 为渐变的颜色重复着色。\n
     * .object: 指定某百分比位置处的渐变色颜色，设置非法颜色直接跳过： \n
     * colors：渐变色颜色颜色。 \n
     * stops：渐变位置。 \n
     * size：颜色个数。 \n
     *
     */
    NODE_LINEAR_GRADIENT,
    /**
     * @brief 设置组件内容在元素绘制区域内的对齐方式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 对齐方式，数据类型{@link ArkUI_Alignment}，默认值ARKUI_ALIGNMENT_CENTER。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 对齐方式，数据类型{@link ArkUI_Alignment}。\n
     *
     */
    NODE_ALIGNMENT,
    /**
     * @brief 透明度属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：透明度数值，取值范围为0到1。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：透明度数值，取值范围为0到1。 \n
     *
     */
    NODE_OPACITY,
    /**
     * @brief 边框宽度属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * 1: .value[0].f32：统一设置四条边的边框宽度。 \n
     * 2: .value[0].f32：设置上边框的边框宽度。 \n
     * .value[1].f32：设置右边框的边框宽度。 \n
     * .value[2].f32：设置下边框的边框宽度。 \n
     * .value[3].f32：设置左边框的边框宽度。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：设置上边框的边框宽度。 \n
     * .value[1].f32：设置右边框的边框宽度。 \n
     * .value[2].f32：设置下边框的边框宽度。 \n
     * .value[3].f32：设置左边框的边框宽度。 \n
     *
     */
    NODE_BORDER_WIDTH,
    /**
     * @brief 边框圆角属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * 1: .value[0].f32：统一设置四条边的边框圆角。 \n
     * 2: .value[0].f32：设置左上角圆角半径。 \n
     * .value[1].f32：设置右上角圆角半径。 \n
     * .value[2].f32：设置左下角圆角半径。 \n
     * .value[3].f32：设置右下角圆角半径。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：设置左上角圆角半径。 \n
     * .value[1].f32：设置右上角圆角半径。 \n
     * .value[2].f32：设置左下角圆角半径。 \n
     * .value[3].f32：设置右下角圆角半径。 \n
     *
     */
    NODE_BORDER_RADIUS,
    /**
     * @brief 边框颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * 1: .value[0].u32：统一设置四条边的边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * 2: .value[0].u32：设置上侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[1].u32：设置右侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[2].u32：设置下侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[3].u32：设置左侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：设置上侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[1].u32：设置右侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[2].u32：设置下侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[3].u32：设置左侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     *
     */
    NODE_BORDER_COLOR,
    /**
     * @brief 边框线条样式属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * 1: .value[0].i32：统一设置四条边的边框线条样式，参数类型{@link ArkUI_BorderStyle}，默认值为ARKUI_BORDER_STYLE_SOLID。 \n
     * 2:.value[0].i32：设置上侧边框线条样式，参数类型{@linkArkUI_BorderStyle}，默认值为ARKUI_BORDER_STYLE_SOLID。 \n
     * .value[1].i32：设置右侧边框线条样式，参数类型{@link ArkUI_BorderStyle}，默认值为ARKUI_BORDER_STYLE_SOLID。 \n
     * .value[2].i32：设置下侧边框线条样式，参数类型{@link ArkUI_BorderStyle}，默认值为ARKUI_BORDER_STYLE_SOLID。 \n
     * .value[3].i32：设置左侧边框线条样式，参数类型{@link ArkUI_BorderStyle}，默认值为ARKUI_BORDER_STYLE_SOLID。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：上侧边框线条样式对应的数值。 \n
     * .value[1].i32：右侧边框线条样式对应的数值。 \n
     * .value[2].i32：下侧边框线条样式对应的数值。 \n
     * .value[3].i32：左侧边框线条样式对应的数值。 \n
     *
     */
    NODE_BORDER_STYLE,
    /**
     * @brief 组件的堆叠顺序属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：堆叠顺序数值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：堆叠顺序数值。 \n
     *
     */
    NODE_Z_INDEX,
    /**
     * @brief 组件是否可见属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制当前组件显示或隐藏，参数类型{@link ArkUI_Visibility}，默认值为ARKUI_VISIBILITY_VISIBLE。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制当前组件显示或隐藏，参数类型{@link ArkUI_Visibility}，默认值为ARKUI_VISIBILITY_VISIBLE。 \n
     *
     */
    NODE_VISIBILITY,
    /**
     * @brief 组件进行裁剪、遮罩处理属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制是否按照父容器边缘轮廓进行裁剪，0表示不裁切，1表示裁切。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制是否按照父容器边缘轮廓进行裁剪，0表示不裁切，1表示裁切。 \n
     *
     */
    NODE_CLIP,
    /**
     * @brief 组件上指定形状的裁剪，支持属性设置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式,共有5种类型： \n
     * 1.rect类型： \n
     * .value[0].i32：裁剪类型，参数类型{@link ArkUI_ClipType}，ARKUI_CLIP_TYPE_RECTANGLE； \n
     * .value[1].f32：矩形宽度； \n
     * .value[2].f32：矩形高度； \n
     * .value[3].f32：矩形圆角宽度； \n
     * .value[4].f32：矩形圆角高度； \n
     * 2.circle类型： \n
     * .value[0].i32：裁剪类型，参数类型{@link ArkUI_ClipType}，ARKUI_CLIP_TYPE_CIRCLE； \n
     * .value[1].f32：圆形宽度； \n
     * .value[2].f32：圆形高度； \n
     * 3.ellipse类型： \n
     * .value[0].i32：裁剪类型，参数类型{@link ArkUI_ClipType}，ARKUI_CLIP_TYPE_ELLIPSE； \n
     * .value[1].f32：椭圆形宽度； \n
     * .value[2].f32：椭圆形高度； \n
     * 4.path类型： \n
     * .value[0].i32：裁剪类型，参数类型{@link ArkUI_ClipType}，ARKUI_CLIP_TYPE_PATH； \n
     * .value[1].f32：路径宽度； \n
     * .value[2].f32：路径高度； \n
     * .string：路径绘制的命令字符串； \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式,共有5种类型： \n
     * 1.rect类型： \n
     * .value[0].i32：裁剪类型，参数类型{@link ArkUI_ClipType}，ARKUI_CLIP_TYPE_RECTANGLE； \n
     * .value[1].f32：矩形宽度； \n
     * .value[2].f32：矩形高度； \n
     * .value[3].f32：矩形圆角宽度； \n
     * .value[4].f32：矩形圆角高度； \n
     * 2.circle类型： \n
     * .value[0].i32：裁剪类型，参数类型{@link ArkUI_ClipType}，ARKUI_CLIP_TYPE_CIRCLE； \n
     * .value[1].f32：圆形宽度； \n
     * .value[2].f32：圆形高度； \n
     * 3.ellipse类型:： \n
     * .value[0].i32：裁剪类型，参数类型{@link ArkUI_ClipType}，ARKUI_CLIP_TYPE_ELLIPSE； \n
     * .value[1].f32：椭圆形宽度； \n
     * .value[2].f32：椭圆形高度； \n
     * 4.path类型： \n
     * .value[0].i32：裁剪类型，参数类型{@link ArkUI_ClipType}，ARKUI_CLIP_TYPE_PATH； \n
     * .value[1].f32：路径宽度； \n
     * .value[2].f32：路径高度； \n
     * .string：路径绘制的命令字符串； \n
     *
     */
    NODE_CLIP_SHAPE,
    /**
     * @brief 矩阵变换功能，可对图形进行平移、旋转和缩放等，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0...15].f32: 16个浮点数字。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0...15].f32: 16个浮点数字。 \n
     *
     */
    NODE_TRANSFORM,
    /**
     * @brief 触摸测试类型，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制当前组件的触摸测试类型，参数类型{@link ArkUI_HitTestMode}，默认值为ARKUI_HIT_TEST_MODE_DEFAULT。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制当前组件的触摸测试类型，参数类型{@link ArkKUI_HitTestMode}，默认值为ARKUI_HIT_TEST_MODE_DEFAULT。 \n
     *
     */
    NODE_HIT_TEST_BEHAVIOR,
    /**
     * @brief 元素左上角相对于父容器左上角偏移位置，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：x轴坐标。 \n
     * .value[1].f32: y轴坐标。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：x轴坐标。 \n
     * .value[1].f32: y轴坐标。 \n
     *
     */
    NODE_POSITION,
    /**
     * @brief 阴影效果属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置当前组件阴影效果，参数类型{@link ArkUI_ShadowStyle}。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置当前组件阴影效果，参数类型{@link ArkUI_ShadowStyle}。 \n
     *
     */
    NODE_SHADOW,
    /**
     * @brief 自定义阴影效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0]?.f32：阴影模糊半径，单位为vp；\n
     * .value[1]?.i32：是否开启智能取色，0代表不开启，1代表开启，默认不开启；\n
     * .value[2]?.f32：阴影X轴偏移量，单位为vp；\n
     * .value[3]?.f32：阴影Y轴偏移量，单位为vp；\n
     * .value[4]?.i32：阴影类型{@link ArkUI_ShadowType}，默认值为ARKUI_SHADOW_TYPE_COLOR；\n
     * .value[5]?.u32：阴影颜色，0xargb格式，形如 0xFFFF0000 表示红色；\n
     * .value[6]?.u32：阴影是否内部填充，，0表示不填充，1表示填充；\n
     *
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：阴影模糊半径，单位为vp；\n
     * .value[1].i32：是否开启智能取色；\n
     * .value[2].f32：阴影X轴偏移量，单位为vp；\n
     * .value[3].f32：阴影Y轴偏移量，单位为vp；\n
     * .value[4].i32：阴影类型{@link ArkUI_ShadowType}，默认值为ARKUI_SHADOW_TYPE_COLOR；\n
     * .value[5].u32：阴影颜色，0xargb格式，形如 0xFFFF0000 表示红色；\n
     * .value[6].u32：阴影是否内部填充，，0表示不填充，1表示填充；\n
     *
     */
    NODE_CUSTOM_SHADOW,
    /**
     * @brief 背景图片的宽高属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示图片的宽度值，取值范围[0,+∞)，单位为vp。\n
     * .value[1].f32 表示图片的高度值，取值范围[0,+∞)，单位为vp。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示图片的宽度值，单位为vp。\n
     * .value[1].f32 表示图片的高度值，单位为vp。\n
     *
     */
    NODE_BACKGROUND_IMAGE_SIZE,
    /**
     * @brief 背景图片的宽高样式属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示背景图片的宽高样式，取{@link ArkUI_ImageSize}枚举值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示背景图片的宽高样式，取{@link ArkUI_ImageSize}枚举值。\n
     *
     */
    NODE_BACKGROUND_IMAGE_SIZE_WITH_STYLE,
    /**
     * @brief 背景和内容之间的模糊属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示模糊类型，取{@link ArkUI_BlurStyle}枚举值。\n
     * .value[1]?.i32 表示深浅色模式，取{@link ArkUI_ColorMode}枚举值。\n
     * .value[2]?.i32 表示取色模式，取{@link ArkUI_AdaptiveColor}枚举值。\n
     * .value[3]?.f32 表示模糊效果程度，取[0.0,1.0]范围内的值。\n
     * .value[4]?.f32 表示灰阶模糊起始边界。\n
     * .value[5]?.f32 表示灰阶模糊终点边界。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示模糊类型，取{@link ArkUI_BlurStyle}枚举值。\n
     * .value[1].i32 表示深浅色模式，取{@link ArkUI_ColorMode}枚举值。\n
     * .value[2].i32 表示取色模式，取{@link ArkUI_AdaptiveColor}枚举值。\n
     * .value[3].f32 表示模糊效果程度，取[0.0,1.0]范围内的值。\n
     * .value[4].f32 表示灰阶模糊起始边界。\n
     * .value[5].f32 表示灰阶模糊终点边界。\n
     *
     */
    NODE_BACKGROUND_BLUR_STYLE,
    /**
     * @brief 图形变换和转场的中心点属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0]?.f32 表示中心点X轴坐标值，单位为vp \n
     * .value[1]?.f32 表示中心点Y轴坐标，单位为vp \n
     * .value[2]?.f32 表示中心点Z轴坐标，单位为vp \n
     * .value[3]?.f32 表示中心点X轴坐标的百分比位置，如0.2表示百分之20的位置，该属性覆盖value[0].f32，默认值:0.5f。\n
     * .value[4]?.f32 表示中心点Y轴坐标的百分比位置，如0.2表示百分之20的位置，该属性覆盖value[1].f32，默认值:0.5f。\n
     * .value[5]?.f32 表示中心点Z轴坐标的百分比位置，如0.2表示百分之20的位置，该属性覆盖value[2].f32，默认值:0.0f。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示中心点X轴坐标，单位为vp \n
     * .value[1].f32 表示中心点Y轴坐标，单位为vp \n
     * .value[2].f32 表示中心点Z轴坐标，单位为vp \n
     * 注：如果设置坐标百分比位置，属性获取方法返回计算后的vp为单位的值。
     *
     */
    NODE_TRANSFORM_CENTER,
    /**
     * @brief 转场时的透明度效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示起终点的透明度值 \n
     * .value[1].i32 表示动画时长，单位ms \n
     * .value[2].i32 表示动画曲线类型，取{@link ArkUI_AnimationCurve}枚举值 \n
     * .value[3]?.i32 表示动画延迟时长，单位ms \n
     * .value[4]?.i32 表示动画播放次数 \n
     * .value[5]?.i32 表示动画播放模式，取{@link ArkUI_AnimationPlayMode}枚举值 \n
     * .value[6]?.f32 表示动画播放速度 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示起终点的透明度值 \n
     * .value[1].i32 表示动画时长，单位ms \n
     * .value[2].i32 表示动画曲线类型，取{@link ArkUI_AnimationCurve}枚举值 \n
     * .value[3].i32 表示动画延迟时长，单位ms \n
     * .value[4].i32 表示动画播放次数 \n
     * .value[5].i32 表示动画播放模式，取{@link ArkUI_AnimationPlayMode}枚举值 \n
     * .value[6].f32 表示动画播放速度 \n
     *
     */
    NODE_OPACITY_TRANSITION,
    /**
     * @brief 转场时的旋转效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示横向旋转分量。\n
     * .value[1].f32 表示纵向的旋转分量。\n
     * .value[2].f32 表示竖向的旋转分量。\n
     * .value[3].f32 表示角度。\n
     * .value[4].f32 表示视距，默认值：0.0f。\n
     * .value[5].i32 表示动画时长，单位ms。\n
     * .value[6].i32 表示动画曲线类型，取{@link ArkUI_AnimationCurve}枚举值。\n
     * .value[7]?.i32 表示动画延迟时长，单位ms。\n
     * .value[8]?.i32 表示动画播放次数。\n
     * .value[9]?.i32 表示动画播放模式，取{@link ArkUI_AnimationPlayMode}枚举值。\n
     * .value[10]?.f32 表示动画播放速度。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示横向旋转分量。\n
     * .value[1].f32 表示纵向的旋转分量。\n
     * .value[2].f32 表示竖向的旋转分量。\n
     * .value[3].f32 表示角度。\n
     * .value[4].f32 表示视距。\n
     * .value[5].i32 表示动画时长，单位ms。\n
     * .value[6].i32 表示动画曲线类型，取{@link ArkUI_AnimationCurve}枚举值。\n
     * .value[7].i32 表示动画延迟时长，单位ms。\n
     * .value[8].i32 表示动画播放次数。\n
     * .value[9].i32 表示动画播放模式，取{@link ArkUI_AnimationPlayMode}枚举值。\n
     * .value[10].f32 表示动画播放速度。\n
     *
     */
    NODE_ROTATE_TRANSITION,
    /**
     * @brief 转场时的缩放效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 横向放大倍数。\n
     * .value[1].f32 纵向放大倍数。\n
     * .value[2].f32 竖向放大倍数。\n
     * .value[3].i32 表示动画时长，单位ms。\n
     * .value[4].i32 表示动画曲线类型，取{@link ArkUI_AnimationCurve}枚举值。\n
     * .value[5]?.i32 表示动画延迟时长，单位ms。\n
     * .value[6]?.i32 表示动画播放次数。\n
     * .value[7]?.i32 表示动画播放模式，取{@link ArkUI_AnimationPlayMode}枚举值。\n
     * .value[8]?.f32 表示动画播放速度。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 横向放大倍数。\n
     * .value[1].f32 纵向放大倍数。\n
     * .value[2].f32 竖向放大倍数。\n
     * .value[3].i32 表示动画时长，单位ms。\n
     * .value[4].i32 表示动画曲线类型，取{@link ArkUI_AnimationCurve}枚举值。\n
     * .value[5].i32 表示动画延迟时长，单位ms。\n
     * .value[6].i32 表示动画播放次数。\n
     * .value[7].i32 表示动画播放模式，取{@link ArkUI_AnimationPlayMode}枚举值。\n
     * .value[8].f32 表示动画播放速度。\n
     *
     */
    NODE_SCALE_TRANSITION,
    /**
     * @brief 转场时的平移效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * value[0].f32 表示横向平移距离值，单位为vp \n
     * value[1].f32 表示纵向平移距离值，单位为vp \n
     * value[2].f32 表示竖向平移距离值，单位为vp \n
     * value[3].i32 表示动画时长，单位ms。\n
     * value[4].i32 表示动画曲线类型，取{@link ArkUI_AnimationCurve}枚举值。\n
     * value[5]?.i32 表示动画延迟时长，单位ms。\n
     * value[6]?.i32 表示动画播放次数。\n
     * value[7]?.i32 表示动画播放模式，取{@link ArkUI_AnimationPlayMode}枚举值。\n
     * value[8]?.f32 表示动画播放速度。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * value[0].f32 表示横向平移距离值，单位为vp \n
     * value[1].f32 表示纵向平移距离值，单位为vp \n
     * value[2].f32 表示竖向平移距离值，单位为vp \n
     * value[3].i32 表示动画时长，单位ms。\n
     * value[4].i32 表示动画曲线类型，取{@link ArkUI_AnimationCurve}枚举值。\n
     * value[5].i32 表示动画延迟时长，单位ms。\n
     * value[6].i32 表示动画播放次数。\n
     * value[7].i32 表示动画播放模式，取{@link ArkUI_AnimationPlayMode}枚举值。\n
     * value[8].f32 表示动画播放速度。\n
     *
     */
    NODE_TRANSLATE_TRANSITION,
    /**
     * @brief 转场时从屏幕边缘滑入和滑出的效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * value[0].i32 参数类型{@link ArkUI_TransitionEdge} \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * value[0].i32 参数类型{@link ArkUI_TransitionEdge} \n
     *
     */
    NODE_MOVE_TRANSITION,

    /**
     * @brief 获焦属性，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：参数类型为1或者0。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：参数类型为1或者0。
     *
     */
    NODE_FOCUSABLE,

    /**
     * @brief 默认焦点属性，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * value[0].i32：参数类型为1或者0。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * value[0].i32：参数类型为1或者0。
     *
     */
    NODE_DEFAULT_FOCUS,

    /**
     * @brief 触摸热区属性，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .data[0].f32：触摸点相对于组件左上角的x轴坐标,单位为vp。 \n
     * .data[1].f32：触摸点相对于组件左上角的y轴坐标,单位为vp。 \n
     * .data[2].f32：触摸热区的宽度 ，单位为%。 \n
     * .data[3].f32：触摸热区的高度，单位为%。 \n
     * .data[4...].f32:可以设置多个手势响应区域，顺序和上述一致。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .data[0].f32：触摸点相对于组件左上角的x轴坐标,单位为vp。 \n
     * .data[1].f32：触摸点相对于组件左上角的y轴坐标,单位为vp。 \n
     * .data[2].f32：触摸热区的宽度 ，单位为%。 \n
     * .data[3].f32：触摸热区的高度，单位为%。 \n
     * .data[4...].f32:可以设置多个手势响应区域，顺序和上述一致。
     *
     */
    NODE_RESPONSE_REGION,

    /**
     * @brief 遮罩文本属性，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .string 遮罩文本；\n
     * .value[0]?.i32：可选值，浮层相对于组件的位置，参数类型{@link ArkUI_Alignment}，
     *  默认值为ARKUI_ALIGNMENT_TOP_START。 \n
     * .value[1]?.f32：可选值，浮层基于自身左上角的偏移量X，单位为vp。 \n
     * .value[2]?.f32：可选值，浮层基于自身左上角的偏移量Y，单位为vp。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .string 遮罩文本； \n
     * .value[0].i32：浮层相对于组件的位置，参数类型{@link ArkUI_Alignment}，
     *  默认值为ARKUI_ALIGNMENT_TOP_START。 \n
     * .value[1].f32：浮层基于自身左上角的偏移量X，单位为vp。 \n
     * .value[2].f32：浮层基于自身左上角的偏移量Y，单位为vp。
     *
     *
     */
    NODE_OVERLAY,
    /**
     * @brief 角度渐变效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0]?.f32:为角度渐变的中心点，即相对于当前组件左上角的坐标,X轴坐标 \n
     * .value[1]?.f32:为角度渐变的中心点，即相对于当前组件左上角的坐标,Y轴坐标 \n
     * .value[2]?.f32:角度渐变的起点，默认值0。 \n
     * .value[3]?.f32:角度渐变的终点，默认值0。 \n
     * .value[4]?.f32:角度渐变的旋转角度，默认值0。 \n
     * .value[5]?.i32:为渐变的颜色重复着色，0表示不重复着色，1表示重复着色 \n
     * .object: 指定某百分比位置处的渐变色颜色，设置非法颜色直接跳过： \n
     * colors：渐变色颜色颜色。 \n
     * stops：渐变位置。 \n
     * size：颜色个数。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32:为角度渐变的中心点，即相对于当前组件左上角的坐标,X轴坐标 \n
     * .value[1].f32:为角度渐变的中心点，即相对于当前组件左上角的坐标,Y轴坐标 \n
     * .value[2].f32:角度渐变的起点，默认值0。 \n
     * .value[3].f32:角度渐变的终点，默认值0。 \n
     * .value[4].f32:角度渐变的旋转角度，默认值0。 \n
     * .value[5].i32:为渐变的颜色重复着色，0表示不重复着色，1表示重复着色 \n
     * .object: 指定某百分比位置处的渐变色颜色，设置非法颜色直接跳过： \n
     * colors：渐变色颜色颜色。 \n
     * stops：渐变位置。 \n
     * size：颜色个数。 \n
     *
     */
    NODE_SWEEP_GRADIENT,
    /**
     * @brief 径向渐变渐变效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0]?.f32:为径向渐变的中心点，即相对于当前组件左上角的坐标,X轴坐标 \n
     * .value[1]?.f32:为径向渐变的中心点，即相对于当前组件左上角的坐标,Y轴坐标 \n
     * .value[2]?.f32:径向渐变的半径，默认值0。 \n
     * .value[3]?.i32:为渐变的颜色重复着色，0表示不重复着色，1表示重复着色 \n
     * .object: 指定某百分比位置处的渐变色颜色，设置非法颜色直接跳过： \n
     * colors：渐变色颜色颜色。 \n
     * stops：渐变位置。 \n
     * size：颜色个数。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32:为径向渐变的中心点，即相对于当前组件左上角的坐标,X轴坐标 \n
     * .value[1].f32:为径向渐变的中心点，即相对于当前组件左上角的坐标,Y轴坐标 \n
     * .value[2].f32:径向渐变的半径，默认值0。 \n
     * .value[3].i32:为渐变的颜色重复着色，0表示不重复着色，1表示重复着色 \n
     * .object: 指定某百分比位置处的渐变色颜色，设置非法颜色直接跳过： \n
     * colors：渐变色颜色颜色。 \n
     * stops：渐变位置。 \n
     * size：颜色个数。 \n
     *
     */
    NODE_RADIAL_GRADIENT,
    /**
     * @brief 组件上加上指定形状的遮罩，支持属性设置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式,共有5种类型： \n
     * 1.rect类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型，参数类型{@link ArkUI_MaskType}，ARKUI_MASK_TYPE_RECTANGLE； \n
     * .value[4].f32：矩形宽度； \n
     * .value[5].f32：矩形高度； \n
     * .value[6].f32：矩形圆角宽度； \n
     * .value[7].f32：矩形圆角高度； \n
     * 2.circle类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型，参数类型{@link ArkUI_MaskType}，ARKUI_MASK_TYPE_CIRCLE； \n
     * .value[4].f32：圆形宽度； \n
     * .value[5].f32：圆形高度； \n
     * 3.ellipse类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型，参数类型{@link ArkUI_MaskType}，ARKUI_MASK_TYPE_ELLIPSE； \n
     * .value[4].f32：椭圆形宽度； \n
     * .value[5].f32：椭圆形高度； \n
     * 4.path类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型，参数类型{@link ArkUI_MaskType}，ARKUI_MASK_TYPE_PATH； \n
     * .value[4].f32：路径宽度； \n
     * .value[5].f32：路径高度； \n
     * .string：路径绘制的命令字符串； \n
     * 4.progress类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型，参数类型{@link ArkUI_MaskType}，ARKUI_MASK_TYPE_PROSGRESS； \n
     * .value[4].f32：进度遮罩的当前值； \n
     * .value[5].f32：进度遮罩的最大值； \n
     * .value[6].u32：进度遮罩的颜色； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式,共有5种类型： \n
     * 1.rect类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型； \n
     * .value[4].f32：矩形宽度； \n
     * .value[5].f32：矩形高度； \n
     * .value[6].f32：矩形圆角宽度； \n
     * .value[7].f32：矩形圆角高度； \n
     * 2.circle类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型； \n
     * .value[4].f32：圆形宽度； \n
     * .value[5].f32：圆形高度； \n
     * 3.ellipse类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型； \n
     * .value[4].f32：椭圆形宽度； \n
     * .value[5].f32：椭圆形高度； \n
     * 4.path类型： \n
     * .value[0].u32：填充颜色，0xargb类型； \n
     * .value[1].u32：描边颜色，0xargb类型； \n
     * .value[2].f32：描边宽度，单位为vp； \n
     * .value[3].i32：遮罩类型； \n
     * .value[4].f32：路径宽度； \n
     * .value[5].f32：路径高度； \n
     * .string：路径绘制的命令字符串； \n
     * 5.progress类型： \n
     * .value[0].i32：遮罩类型； \n
     * .value[1].f32：进度遮罩的当前值； \n
     * .value[2].f32：进度遮罩的最大值； \n
     * .value[3].u32：进度遮罩的颜色； \n
     *
     */
    NODE_MASK,
    /**
     * @brief 当前控件背景与子节点内容进行混合，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制当前组件的混合模式类型，参数类型{@link ArkUI_BlendMode}，默认值为ARKUI_BLEND_MODE_NONE。 \n
     * .value[1].?i32：blendMode实现方式是否离屏，参数类型{@link ArkUI_BlendApplyType}，默认值为ARKUI_BLEND_APPLY_TYPE_FAST。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制当前组件的混合模式类型，参数类型{@link ArkUI_BlendMode}，默认值为ARKUI_BLEND_MODE_NONE。 \n
     * .value[1].i32：blendMode实现方式是否离屏，参数类型{@link ArkUI_BlendApplyType}，默认值为ARKUI_BLEND_APPLY_TYPE_FAST。 \n
     *
     */
    NODE_BLEND_MODE,
    /**
     * @brief 设置容器元素内主轴方向上的布局，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置容器元素内主轴方向上的布局类型， \n
     * 参数类型{@link ArkUI_Direction}，默认值为ARKUI_DIRECTION_AUTO。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置容器元素内主轴方向上的布局类型， \n
     * 参数类型{@link ArkUI_Direction}，默认值为ARKUI_DIRECTION_AUTO。 \n
     *
     */
    NODE_DIRECTION,
    /**
     * @brief 约束尺寸属性，组件布局时，进行尺寸范围限制，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：最小宽度，单位vp； \n
     * .value[1].f32：最大宽度，单位vp； \n
     * .value[2].f32：最小高度，单位vp； \n
     * .value[3].f32：最大高度，单位vp； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：最小宽度，单位vp； \n
     * .value[1].f32：最大宽度，单位vp； \n
     * .value[2].f32：最小高度，单位vp； \n
     * .value[3].f32：最大高度，单位vp； \n
     *
     */
    NODE_CONSTRAINT_SIZE,
    /**
     * @brief 灰度效果属性，支持属性设置，属性重置和属性获取接口.
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：灰度转换比例，范围0-1之间，比如0.5指按照50%进行灰度处理； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：灰度转换比例，范围0-1之间； \n
     *
     */
    NODE_GRAY_SCALE,
    /**
     * @brief 反转输入的图像比例属性，支持属性设置，属性重置和属性获取接口.
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：图像反转比例，范围0-1之间，比如0.5指按照50%进行反转处理； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：图像反转比例，范围0-1之间； \n
     *
     */
    NODE_INVERT,
    /**
     * @brief 图像转换为深褐色比例属性，支持属性设置，属性重置和属性获取接口.
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：图像转换为深褐色比例，范围0-1之间，比如0.5指按照50%进行深褐色处理； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：图像转换为深褐色比例，范围0-1之间； \n
     *
     */
    NODE_SEPIA,
    /**
     * @brief 对比度属性，支持属性设置，属性重置和属性获取接口.
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：对比度，等于1时为原图，越大则对比度越高，取值范围：[0, 10)； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：对比度，取值范围：[0, 10)； \n
     *
     */
    NODE_CONTRAST,
    /**
     * @brief 前景颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式，支持两种入参格式：\n
     * 1：.value[0].u32：颜色数值，0xargb类型，如0xFFFF0000表示红色；\n
     * 2：.value[0].i32：颜色数值枚举{@link ArkUI_ColoringStrategy}；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb类型；\n
     *
     */
    NODE_FOREGROUND_COLOR,

    /**
     * @brief 组件子元素相对组件自身的额外偏移属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示x轴方向的偏移值, 单位为vp。 \n
     * .value[1].f32 表示y轴方向的偏移值, 单位为vp。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示x轴方向的偏移值, 单位为vp。 \n
     * .value[1].f32 表示y轴方向的偏移值, 单位为vp。 \n
     *
     */
    NODE_OFFSET,
    /**
     * @brief 组件子元素在位置定位时的锚点属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示锚点x坐标值, 单位为vp \n
     * .value[1].f32 表示锚点y坐标值, 单位为vp \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示锚点x坐标值, 单位为vp \n
     * .value[1].f32 表示锚点y坐标值, 单位为vp \n
     *
     */
    NODE_MARK_ANCHOR,
    /**
     * @brief 背景图在组件中显示位置，即相对于组件左上角的坐标，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示x轴方向的位置, 单位为vp。 \n
     * .value[1].f32 表示y轴方向的位置, 单位为vp。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示x轴方向的位置, 单位为vp。 \n
     * .value[1].f32 表示y轴方向的位置, 单位为vp。 \n
     *
     */
    NODE_BACKGROUND_IMAGE_POSITION,
    /**
     * @brief 相对容器中子组件的对齐规则属性，支持属性设置，属性重置，获取属性接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0]?.i32 左对齐规则相对锚点组件的对齐方式，取{@link ArkUI_HorizontalAlignment}枚举值。\n
     * .value[1]?.i32 横向居中规则相对锚点组件的对齐方式，取{@link ArkUI_HorizontalAlignment}枚举值。\n
     * .value[2]?.i32 右对齐规则相对锚点组件的对齐方式，取{@link ArkUI_HorizontalAlignment}枚举值。\n
     * .value[3]?.i32 顶部对齐规则相对锚点组件的对齐方式，取{@link ArkUI_VerticalAlignment}枚举值。\n
     * .value[4]?.i32 纵向居中规则相对锚点组件的对齐方式，取{@link ArkUI_VerticalAlignment}枚举值。\n
     * .value[5]?.i32 底部对齐规则相对锚点组件的对齐方式，取{@link ArkUI_VerticalAlignment}枚举值。\n
     * .value[6]?.f32 水平方向的bias值。\n
     * .value[7]?.f32 垂直方向的bias值。\n
     * .string 锚点组件id。多个用,分隔，格式为"leftAnchorId,horizontalAnchorId,rightAnchorId,
     * topAnchorId,verticalAnchorId,bottomAnchorId"，不存在的id用空字符串代替。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 左对齐规则相对锚点组件的对齐方式，取{@link ArkUI_HorizontalAlignment}枚举值。\n
     * .value[1].i32 横向居中规则相对锚点组件的对齐方式，取{@link ArkUI_HorizontalAlignment}枚举值。\n
     * .value[2].i32 右对齐规则相对锚点组件的对齐方式，取{@link ArkUI_HorizontalAlignment}枚举值。\n
     * .value[3].i32 顶部对齐规则相对锚点组件的对齐方式，取{@link ArkUI_VerticalAlignment}枚举值。\n
     * .value[4].i32 纵向居中规则相对锚点组件的对齐方式，取{@link ArkUI_VerticalAlignment}枚举值。\n
     * .value[5].i32 底部对齐规则相对锚点组件的对齐方式，取{@link ArkUI_VerticalAlignment}枚举值。\n
     * .value[6].f32 水平方向的bias值。\n
     * .value[7].f32 垂直方向的bias值。\n
     * .string 锚点组件id。多个用,分隔，格式为"leftAnchorId,horizontalAnchorId,rightAnchorId,
     * topAnchorId,verticalAnchorId,bottomAnchorId"，不存在的id用空字符串代替。
     *
     */
    NODE_ALIGN_RULES,
    /**
     * @brief 设置子组件在父容器交叉轴的对齐格式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置子组件在父容器交叉轴的对齐格式类型， \n
     * 参数类型{@link ArkUI_ItemAlignment}，默认值为ARKUI_ITEM_ALIGNMENT_AUTO。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置子组件在父容器交叉轴的对齐格式类型， \n
     * 参数类型{@link ArkUI_ItemAlignment}，默认值为ARKUI_ITEM_ALIGNMENT_AUTO。 \n
     *
     */
    NODE_ALIGN_SELF,
    /**
     * @brief 组件的基准尺寸，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：组件的基准尺寸数值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：组件的基准尺寸数值。 \n
     *
     */
    NODE_FLEX_GROW,
    /**
     * @brief 父容器压缩尺寸分配给此属性所在组件的比例，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：父容器压缩尺寸分配给此属性所在组件的比例数值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：父容器压缩尺寸分配给此属性所在组件的比例数值。 \n
     *
     */
    NODE_FLEX_SHRINK,
    /**
     * @brief 设置组件的基准尺寸，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：组件的基准尺寸数值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：组件的基准尺寸数值。 \n
     *
     */
    NODE_FLEX_BASIS,
    /**
     * @brief 无障碍组属性, 支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：为1时表示该组件及其所有子组件为一整个可以选中的组件
     * 无障碍服务将不再关注其子组件内容。参数类型为1或者0。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：为1时表示该组件及其所有子组件为一整个可以选中的组件
     * 无障碍服务将不再关注其子组件内容。参数类型为1或者0。
     *
     */
    NODE_ACCESSIBILITY_GROUP,

    /**
     * @brief 无障碍文本属性，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .string：无障碍文本。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .string：无障碍文本。
     *
     */
    NODE_ACCESSIBILITY_TEXT,
    /**
     * @brief 无障碍辅助服务模式，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：辅助服务模式，参数类型{@link ArkUI_AccessibilityMode}。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：辅助服务模式，参数类型{@link ArkUI_AccessibilityMode}。
     *
     */
    NODE_ACCESSIBILITY_MODE,

    /**
     * @brief 无障碍说明属性，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .string：无障碍说明。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .string：无障碍说明。
     *
     */
    NODE_ACCESSIBILITY_DESCRIPTION,

    /**
     * @brief 组件获取焦点属性，支持属性设置，属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：参数类型为1或者0。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：参数类型为1或者0。
     *
     */
    NODE_FOCUS_STATUS,
    /**
     * @brief 设置组件的宽高比，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：组件的宽高比，输入值为 width/height。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：组件的宽高比，width/height的比值。 \n
     *
     */
    NODE_ASPECT_RATIO,
    /**
     * @brief Row/Column/Flex 布局下的子组件布局权重参数，支持属性设置、属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：子组件占主轴尺寸的权重。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：子组件占主轴尺寸的权重。 \n
     *
     */
    NODE_LAYOUT_WEIGHT,
    /**
     * @brief Row/Column/Flex(单行) 布局下的子组件在布局容器中显示的优先级，支持属性设置、属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：子组件在父容器中的显示优先级。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：子组件在父容器中的显示优先级。 \n
     *
     */
    NODE_DISPLAY_PRIORITY,
    /**
     * @brief 设置元素的外描边宽度。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：元素左边的外描边宽度。 \n
     * .value[1].f32：元素上边的外描边宽度。 \n
     * .value[2].f32：元素右边的外描边宽度。 \n
     * .value[3].f32：元素下边的外描边宽度。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：元素左边的外描边宽度。 \n
     * .value[1].f32：元素上边的外描边宽度。 \n
     * .value[2].f32：元素右边的外描边宽度。 \n
     * .value[3].f32：元素下边的外描边宽度。 \n
     *
     */
    NODE_OUTLINE_WIDTH,

    /**
     * @brief 设置宽高动画过程中的组件内容填充方式，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 内容填充方式，使用{@link ArkUI_RenderFit}枚举值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 内容填充方式，使用{@link ArkUI_RenderFit}枚举值。\n
     *
     */
    NODE_RENDER_FIT,

    /**
     * @brief 外描边颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * 1: .value[0].u32：统一设置四条边的边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * 2: .value[0].u32：设置上侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[1].u32：设置右侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[2].u32：设置下侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[3].u32：设置左侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：设置上侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[1].u32：设置右侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[2].u32：设置下侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * .value[3].u32：设置左侧边框颜色，使用0xargb表示，如0xFFFF11FF。 \n
     *
     */
    NODE_OUTLINE_COLOR,

    /**
     * @brief 设置高宽尺寸，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：宽度数值，单位为vp；\n
     * .value[1].f32：高度数值，单位为vp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：宽度数值，单位为vp；\n
     * .value[1].f32：高度数值，单位为vp；\n
     *
     */
    NODE_SIZE,

    /**
     * @brief 设置当前组件和子组件是否先整体离屏渲染绘制后再与父控件融合绘制，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：参数类型为1或者0。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：参数类型为1或者0。
     *
     */
    NODE_RENDER_GROUP,

    /**
     * @brief 为组件添加颜色叠加效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：叠加的颜色，使用0xargb表示，如0xFFFF11FF。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：叠加的颜色，使用0xargb表示，如0xFFFF11FF。 \n
     *
     */
    NODE_COLOR_BLEND,

    /**
     * @brief 为当前组件提供内容模糊能力，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示内容模糊样式，取{@link ArkUI_BlurStyle}枚举值。\n
     * .value[1]?.i32 表示内容模糊效果使用的深浅色模式，取{@link ArkUI_ThemeColorMode}枚举值。\n
     * .value[2]?.i32 表示内容模糊效果使用的取色模式，取{@link ArkUI_AdaptiveColor}枚举值。\n
     * .value[3]?.i32 表示灰阶模糊参数，取值范围为[0,127]。\n
     * .value[4]?.i32 表示灰阶模糊参数，取值范围为[0,127]。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示内容模糊样式，取{@link ArkUI_BlurStyle}枚举值。\n
     * .value[1].i32 表示内容模糊效果使用的深浅色模式，取{@link ArkUI_ThemeColorMode}枚举值。\n
     * .value[2].i32 表示内容模糊效果使用的取色模式，取{@link ArkUI_AdaptiveColor}枚举值。\n
     * .value[3].i32 表示灰阶模糊参数，取值范围为[0,127]。\n
     * .value[4].i32 表示灰阶模糊参数，取值范围为[0,127]。\n
     *
     */
    NODE_FOREGROUND_BLUR_STYLE,

    /**
     * @brief 宽度属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：宽度数值，单位为百分比；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：宽度数值，单位为百分比；\n
     *
     */
    NODE_WIDTH_PERCENT,
    /**
     * @brief 高度属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：高度数值，单位为百分比；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：高度数值，单位为百分比；\n
     *
     */
    NODE_HEIGHT_PERCENT,
    /**
     * @brief 内间距属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式有两种：\n
     * 1：上下左右四个位置的内间距值相等。\n
     * .value[0].f32：内间距数值，单位为百分比；\n
     * 2：分别指定上下左右四个位置的内间距值。\n
     * .value[0].f32：上内间距数值，单位为百分比；\n
     * .value[1].f32：右内间距数值，单位为百分比；\n
     * .value[2].f32：下内间距数值，单位为百分比；\n
     * .value[3].f32：左内间距数值，单位为百分比；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：上内间距数值，单位为百分比；\n
     * .value[1].f32：右内间距数值，单位为百分比；\n
     * .value[2].f32：下内间距数值，单位为百分比；\n
     * .value[3].f32：左内间距数值，单位为百分比；\n
     *
     */
    NODE_PADDING_PERCENT,
    /**
     * @brief 外间距属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式有两种：\n
     * 1：上下左右四个位置的外间距值相等。\n
     * .value[0].f32：外间距数值，单位为百分比；\n
     * 2：分别指定上下左右四个位置的外间距值。\n
     * .value[0].f32：上外间距数值，单位为百分比；\n
     * .value[1].f32：右外间距数值，单位为百分比；\n
     * .value[2].f32：下外间距数值，单位为百分比；\n
     * .value[3].f32：左外间距数值，单位为百分比；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：上外间距数值，单位为百分比；\n
     * .value[1].f32：右外间距数值，单位为百分比；\n
     * .value[2].f32：下外间距数值，单位为百分比；\n
     * .value[3].f32：左外间距数值，单位为百分比；\n
     *
     */
    NODE_MARGIN_PERCENT,
    /**
     * @brief text组件设置文本内容属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string 表示文本内容 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string 表示文本内容 \n
     */
    NODE_TEXT_CONTENT = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TEXT,
    /**
     * @brief 组件字体颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：字体颜色数值，0xargb格式，形如 0xFFFF0000 表示红色；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：字体颜色数值，0xargb格式；\n
     *
     */
    NODE_FONT_COLOR,
    /**
     * @brief 组件字体大小属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：字体大小数值，单位为fp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：字体大小数值，单位为fp；\n
     *
     */
    NODE_FONT_SIZE,
    /**
     * @brief 组件字体样式属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：字体样式{@link ArkUI_FontStyle}，默认值为ARKUI_FONT_STYLE_NORMAL；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：字体样式{@link ArkUI_FontStyle}；\n
     *
     */
    NODE_FONT_STYLE,
    /**
     * @brief 组件字体粗细属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：字体粗细样式{@link ArkUI_FontWeight}，默认值为ARKUI_FONT_WEIGHT_NORMAL；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：字体粗细样式{@link ArkUI_FontWeight}；\n
     *
     */
    NODE_FONT_WEIGHT,
    /**
     * @brief 文本行高属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示lineHeight值，单位为fp \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示lineHeight值，单位为fp \n
     *
     */
    NODE_TEXT_LINE_HEIGHT,
    /**
     * @brief 置文本装饰线样式及其颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：文本装饰线类型{@link ArkUI_TextDecorationType}，默认值为ARKUI_TEXT_DECORATION_TYPE_NONE；\n
     * .value[1]?.u32：可选值，装饰线颜色，0xargb格式，形如 0xFFFF0000 表示红色；\n
     * .value[2]?.i32：文本装饰线样式{@link ArkUI_TextDecorationStyle}；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：文本装饰线类型{@link ArkUI_TextDecorationType}；\n
     * .value[1].u32：装饰线颜色，0xargb格式。\n
     * .value[2].i32：文本装饰线样式{@link ArkUI_TextDecorationStyle}；\n
     *
     */
    NODE_TEXT_DECORATION,
    /**
     * @brief 文本大小写属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示文本大小写类型 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示文本大小写类型 \n
     *
     */
    NODE_TEXT_CASE,
    /**
     * @brief 文本字符间距属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示字符间距值，单位为fp \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 表示字符间距值，单位为fp \n
     *
     */
    NODE_TEXT_LETTER_SPACING,
    /**
     * @brief 文本最大行数属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示最大行数 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示最大行数 \n
     *
     */
    NODE_TEXT_MAX_LINES,
    /**
     * @brief 文本水平对齐方式, 支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：表示文本水平对齐方式，取{@link ArkUI_TextAlignment}枚举值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：表示文本水平对齐方式，取{@link ArkUI_TextAlignment}枚举值。\n
     *
     */
    NODE_TEXT_ALIGN,
    /**
     * @brief 文本超长时的显示方式属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：表示文本超长时的显示方式。{@ArkUI_TextOverflow}\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：表示文本超长时的显示方式。{@ArkUI_TextOverflow}\n
     *
     */
    NODE_TEXT_OVERFLOW,
    /**
     * @brief Text字体列表属性，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .string：字体字符串，多个用,分隔。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .string：字体字符串，多个用,分隔。
     *
     */
    NODE_FONT_FAMILY,
    /**
     * @brief 文本复制粘贴属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：复制粘贴方式{@link ArkUI_CopyOptions}，默认值为ARKUI_COPY_OPTIONS_NONE；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：复制粘贴方式{@link ArkUI_CopyOptions}。\n
     *
     */
    NODE_TEXT_COPY_OPTION,
    /**
     * @brief 文本基线的偏移量属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：偏移量数值，单位为fp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：偏移量数值，单位为fp。\n
     *
     */
    NODE_TEXT_BASELINE_OFFSET,
    /**
     * @brief 文字阴影效果属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：阴影模糊半径，单位为vp；\n
     * .value[1].i32：阴影类型{@link ArkUI_ShadowType}，默认值为ARKUI_SHADOW_TYPE_COLOR；\n
     * .value[2].u32：阴影颜色，0xargb格式，形如 0xFFFF0000 表示红色；\n
     * .value[3].f32：阴影X轴偏移量，单位为vp；\n
     * .value[4].f32：阴影Y轴偏移量，单位为vp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：阴影模糊半径，单位为vp；\n
     * .value[1].i32：阴影类型{@link ArkUI_ShadowType}；\n
     * .value[2].u32：阴影颜色，0xargb格式；\n
     * .value[3].f32：阴影X轴偏移量，单位为vp；\n
     * .value[4].f32：阴影Y轴偏移量，单位为vp；\n
     *
     */
    NODE_TEXT_TEXT_SHADOW,
    /**
     * @brief Text最小显示字号，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32：文本最小显示字号，单位FP。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：文本最小显示字号，单位FP。
     *
     */
    NODE_TEXT_MIN_FONT_SIZE,

    /**
     * @brief Text最大显示字号，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32：文本最大显示字号 单位FP。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：文本最大显示字号 单位FP。
     *
     */
    NODE_TEXT_MAX_FONT_SIZE,

    /**
     * @brief Text样式，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .string?：可选值 字体列表，使用多个字体，使用','进行分割。 \n
     * .value[0].f32：文本尺寸 单位FP。 \n
     * .value[1]?.i32：可选值，文本的字体粗细，参数类型{@link ArkUI_FontWeight}。
     * 默认值为ARKUI_FONT_WEIGHT_NORMAL。 \n
     * .value[2]?.i32：可选值，字体样式，参数类型{@link ArkUI_FontStyle}。
     *  默认值为ARKUI_FONT_STYLE_NORMAL。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .string：字体列表，使用多个字体，使用','进行分割。 \n
     * .value[0].f32：文本尺寸 单位FP。 \n
     * .value[1].i32：文本的字体粗细，参数类型{@link ArkUI_FontWeight}。
     * 默认值为ARKUI_FONT_WEIGHT_NORMAL。 \n
     * .value[2].i32：字体样式，参数类型{@link ArkUI_FontStyle}。
     *  默认值为ARKUI_FONT_STYLE_NORMAL。
     *
     */
    NODE_TEXT_FONT,

    /**
     * @brief Text自适应高度的方式，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：参数类型{@link ArkUI_TextHeightAdaptivePolicy}。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：参数类型{@link ArkUI_TextHeightAdaptivePolicy}。
     *
     */
    NODE_TEXT_HEIGHT_ADAPTIVE_POLICY,
    /**
     * @brief 文本首行缩进属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32: 表示首行缩进值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32: 表示首行缩进值。\n
     *
     */
    NODE_TEXT_INDENT,
    /**
     * @brief 文本断行规则属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32: 参数类型{@link ArkUI_WordBreak}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32: 参数类型{@link ArkUI_WordBreak}。\n
     *
     */
    NODE_TEXT_WORD_BREAK,
    /**
     * @brief 设置文本省略位置，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32: 参数类型{@link ArkUI_EllipsisMode}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32: 参数类型{@link ArkUI_EllipsisMode}。\n
     *
     */
    NODE_TEXT_ELLIPSIS_MODE,
    /**
     * @brief 设置文本特性效果，设置NODE_FONT_FEATURE属性，
     * NODE_FONT_FEATURE是 OpenType 字体的高级排版能力，\n
     * 如支持连字、数字等宽等特性，一般用在自定义字体中，其能力需要字体本身支持,
     * 支持属性设置，属性重置，属性获取接口。\n
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string 符合文本特性格式的字符串，格式为normal | <feature-tag-value>,\n
     * <feature-tag-value>的格式为：<string> [ <integer> | on | off ],\n
     * <feature-tag-value>的个数可以有多个，中间用','隔开,例如，使用等宽数字的输入格式为：ss01 on。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string 表示文本特性的内容，多个文本特性之间使用逗号分隔。\n
     *
     */
    NODE_FONT_FEATURE,
    /**
     * @brief 设置使能文本识别。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：使能文本识别，默认值false。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：使能文本识别。\n
     *
     */
    NODE_TEXT_ENABLE_DATA_DETECTOR,
    /**
     * @brief 设置文本识别配置。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0...].i32: 实体类型数组，参数类型{@link ArkUI_TextDataDetectorType}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0...].i32：实体类型数组，参数类型{@link ArkUI_TextDataDetectorType}。\n
     *
     */
    NODE_TEXT_ENABLE_DATA_DETECTOR_CONFIG,
    /**
     * @brief 文本内容属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string 表示span的文本内容。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string 表示span的文本内容。\n
     *
     */
    NODE_SPAN_CONTENT = MAX_NODE_SCOPE_NUM * ARKUI_NODE_SPAN,
    /**
     * @brief 文本背景色属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32 表示文本背景颜色，0xargb格式，形如0xFFFF0000 表示红色。\n
     * 第二个参数为文本背景圆角设置，支持如下两种设置方式：\n
     * 1：.value[1].f32：四个方向的圆角半径统一设置，单位为vp。\n
     * 2: .value[1].f32：设置左上角圆角半径，单位为vp。 \n
     * .value[2].f32：设置右上角圆角半径，单位为vp。 \n
     * .value[3].f32：设置左下角圆角半径，单位为vp。 \n
     * .value[4].f32：设置右下角圆角半径，单位为vp。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：文本背景颜色，0xargb格式。\n
     * .value[1].f32：左上角圆角半径，单位为vp。 \n
     * .value[2].f32：右上角圆角半径，单位为vp。 \n
     * .value[3].f32：左下角圆角半径，单位为vp。 \n
     * .value[4].f32：右下角圆角半径，单位为vp。 \n
     *
     */
    NODE_SPAN_TEXT_BACKGROUND_STYLE,
    /**
     * @brief imageSpan组件图片地址属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string 表示imageSpan的图片地址 \n
     * .object 表示 PixelMap 图片数据，参数类型为{@link ArkUI_DrawableDesciptor}。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string 表示imageSpan的图片地址 \n
     * .object 表示 PixelMap 图片数据，参数类型为{@link ArkUI_DrawableDesciptor}；.object参数和.string参数二选一，不可同时设置。 \n
     *
     */
    NODE_IMAGE_SPAN_SRC = MAX_NODE_SCOPE_NUM * ARKUI_NODE_IMAGE_SPAN,
    /**
     * @brief 图片基于文本的对齐方式属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示图片基于文本的对齐方式，取{@link ArkUI_ImageSpanAlignment}枚举值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示图片基于文本的对齐方式，取{@link ArkUI_ImageSpanAlignment}枚举值。\n
     *
     */
    NODE_IMAGE_SPAN_VERTICAL_ALIGNMENT,
    /**
     * @brief image组件设置图片地址属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string 表示image组件地址。 \n
     * .object 表示 PixelMap 图片数据，参数类型为{@link ArkUI_DrawableDesciptor}；.object参数和.string参数二选一，不可同时设置。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string 表示image组件地址。 \n
     * .object 表示 PixelMap 图片数据，参数类型为{@link ArkUI_DrawableDesciptor}。\n
     *
     */
    NODE_IMAGE_SRC = MAX_NODE_SCOPE_NUM * ARKUI_NODE_IMAGE,
    /**
     * @brief 图片填充效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示图片填充效果，取{@link ArkUI_ObjectFit}枚举值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示图片填充效果，取{@link ArkUI_ObjectFit}枚举值。\n
     *
     */
    NODE_IMAGE_OBJECT_FIT,
    /**
     * @brief 图片插值效果效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示插值效果效果，取{@link ArkUI_ImageInterpolation}枚举值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示插值效果效果，取{@link ArkUI_ImageInterpolation}枚举值。 \n
     *
     */
    NODE_IMAGE_INTERPOLATION,
    /**
     * @brief 图片重复样式属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示图片重复样式，取{@link ArkUI_ImageRepeat}枚举值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示图片重复样式，取{@link ArkUI_ImageRepeat}枚举值。 \n
     *
     */
    NODE_IMAGE_OBJECT_REPEAT,
    /**
     * @brief 图片滤镜效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 ~ .value[19].f32 表示滤镜矩阵数组。 \n
     * .size  表示滤镜数组大小 5*4。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 ~ .value[19].f32 表示滤镜矩阵数组。 \n
     * .size  表示滤镜数组大小 5*4。 \n
     *
     */
    NODE_IMAGE_COLOR_FILTER,
    /**
     * @brief 图源自动缩放属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示是否缩放布尔值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示是否缩放布尔值。\n
     *
     */
    NODE_IMAGE_AUTO_RESIZE,
    /**
     * @brief 占位图地址属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string 表示image组件占位图地址。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string 表示image组件占位图地址。\n
     *
     */
    NODE_IMAGE_ALT,
    /**
     * @brief 图片拖拽效果属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示是否支持拖拽，设置为true表示支持。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 表示是否支持拖拽。\n
     *
     */
    NODE_IMAGE_DRAGGABLE,
    /**
     * @brief 图片渲染模式属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 参数类型{@link ArkUI_ImageRenderMode}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 参数类型{@link ArkUI_ImageRenderMode}。\n
     *
     */
    NODE_IMAGE_RENDER_MODE,
    /**
     * @brief 设置图片的显示尺寸是否跟随图源尺寸，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32，设置图片的显示尺寸是否跟随图源尺寸，1表示跟随，0表示不跟随，默认值为0。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32，1表示图片的显示尺寸跟随图源尺寸，0表示图片的显示尺寸不跟随图源尺寸。\n
     *
     */
    NODE_IMAGE_FIT_ORIGINAL_SIZE,
    /**
     * @brief 设置填充颜色，设置后填充颜色会覆盖在图片上，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：填充色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：填充色数值，0xargb格式。\n
     *
     */
    NODE_IMAGE_FILL_COLOR,
    /**
     * @brief 设置图像拉伸时，可调整大小的图像选项。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 图片左部拉伸时，保持不变距离。单位vp。\n
     * .value[1].f32 图片上部拉伸时，保持不变距离。单位vp。\n
     * .value[2].f32 图片右部拉伸时，保持不变距离。单位vp。\n
     * .value[3].f32 图片下部拉伸时，保持不变距离。单位vp。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32 图片左部拉伸时，保持不变距离。单位vp。\n
     * .value[1].f32 图片上部拉伸时，保持不变距离。单位vp。\n
     * .value[2].f32 图片右部拉伸时，保持不变距离。单位vp。\n
     * .value[3].f32 图片下部拉伸时，保持不变距离。单位vp。\n
     *
     */
    NODE_IMAGE_RESIZABLE,
    /**
     * @brief 组件打开状态的背景颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：背景色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：背景色数值，0xargb格式。\n
     *
     */
    NODE_TOGGLE_SELECTED_COLOR = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TOGGLE,
    /**
     * @brief Switch类型的圆形滑块颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：圆形滑块颜色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：圆形滑块颜色数值，0xargb格式。\n
     *
     */
    NODE_TOGGLE_SWITCH_POINT_COLOR,
    /**
     * @brief Switch类型的开关值，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：设置开关的值，true表示开启。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：设置开关的值。\n
     *
     */
    NODE_TOGGLE_VALUE,
    /**
     * @brief 组件关闭状态的背景颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：背景色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：背景色数值，0xargb格式。\n
     *
     */
    NODE_TOGGLE_UNSELECTED_COLOR,

    /**
     * @brief 加载进度条前景色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：前景颜色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：前景颜色数值，0xargb格式。\n
     *
     */
    NODE_LOADING_PROGRESS_COLOR = MAX_NODE_SCOPE_NUM * ARKUI_NODE_LOADING_PROGRESS,
    /**
     * @brief LoadingProgress动画显示属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false时不显示动画，true时可以显示动画；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：0时不显示动画，1时可以显示动画。\n
     *
     */
    NODE_LOADING_PROGRESS_ENABLE_LOADING,

    /**
     * @brief 单行文本输入框的默认提示文本内容属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string：默认提示文本的内容。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string：默认提示文本的内容。\n
     *
     */
    NODE_TEXT_INPUT_PLACEHOLDER = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TEXT_INPUT,
    /**
     * @brief 单行文本输入框的默认文本内容属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string：默认文本的内。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string：默认文本的内容。\n
     *
     */
    NODE_TEXT_INPUT_TEXT,
    /**
     * @brief 光标颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：光标颜色数值，0xargb格式，形如 0xFFFF0000 表示红色；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：光标颜色数值，0xargb格式。\n
     *
     */
    NODE_TEXT_INPUT_CARET_COLOR,
    /**
     * @brief 光标风格属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：光标宽度数值，单位为vp；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：光标宽度数值，单位为vp。\n
     *
     */
    NODE_TEXT_INPUT_CARET_STYLE,
    /**
     * @brief 单行文本输入框下划线属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示不展示下划线，true表示展示下划线；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：0表示不展示下划线，1表示展示下划线。\n
     *
     */
    NODE_TEXT_INPUT_SHOW_UNDERLINE,
    /**
     * @brief 输入框支持的最大文本数属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：最大文本数的数字，无单位。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：最大文本数的数字。\n
     *
     */
    NODE_TEXT_INPUT_MAX_LENGTH,
    /**
     * @brief 回车键类型属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：回车键类型枚举{@link ArkUI_EnterKeyType}，默认值为ARKUI_ENTER_KEY_TYPE_DONE。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：回车键类型枚举{@link ArkUI_EnterKeyType}。\n
     *
     */
    NODE_TEXT_INPUT_ENTER_KEY_TYPE,
    /**
     * @brief 无输入时默认提示文本的颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式。\n
     *
     */
    NODE_TEXT_INPUT_PLACEHOLDER_COLOR,
    /**
     * @brief 无输入时默认提示文本的字体配置（包括大小、字重、样式、字体列表）属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0]?.f32：可选字体大小数值，默认值16.0，单位为fp；\n
     * .value[1]?.i32：可选字体样式{@link ArkUI_FontStyle}，默认值为ARKUI_FONT_STYLE_NORMAL；\n
     * .value[2]?.i32：可选字体粗细样式{@link ArkUI_FontWeight}，默认值为ARKUI_FONT_WEIGHT_NORMAL；\n
     * ?.string: 字体族内容，多个字体族之间使用逗号分隔，形如“字重；字体族1，字体族2”。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：字体大小数值，单位为fp；\n
     * .value[1].i32：字体样式{@link ArkUI_FontStyle}；\n
     * .value[2].i32：字体粗细样式{@link ArkUI_FontWeight}；\n
     * .string: 字体族内容，多个字体族之间使用逗号分隔。\n
     *
     */
    NODE_TEXT_INPUT_PLACEHOLDER_FONT,
    /**
     * @brief 聚焦时是否绑定输入法属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示聚焦不拉起输入法，true表示拉起。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：0表示聚焦不拉起输入法，1表示拉起。\n
     *
     */
    NODE_TEXT_INPUT_ENABLE_KEYBOARD_ON_FOCUS,
    /**
     * @brief 输入框的类型属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：输入框类型枚举{@link ArkUI_TextInputType}，默认值为ARKUI_TEXTINPUT_TYPE_NORMAL。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：输入框类型枚举{@link ArkUI_TextInputType}。\n
     *
     */
    NODE_TEXT_INPUT_TYPE,
    /**
     * @brief 输入框文本选中时的背景色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式。\n
     *
     */
    NODE_TEXT_INPUT_SELECTED_BACKGROUND_COLOR,
    /**
     * @brief 密码输入模式时是否显示末尾图标属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示不显示图标，true表示显示图标；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：0表示不显示图标，1表示显示图标。\n
     *
     */
    NODE_TEXT_INPUT_SHOW_PASSWORD_ICON,
    /**
     * @brief 控制单行文本输入框编辑态属性，支持属性设置。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示退出编辑态，true表示维持现状。\n
     * \n
     * 属性获取方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示退出编辑态，true表示维持现状。
     *
     */
    NODE_TEXT_INPUT_EDITING,
    /**
     * @brief 单行文本右侧清除按钮样式属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：按钮样式{@link ArkUI_CancelButtonStyle}，默认值为ARKUI_CANCELBUTTON_STYLE_INPUT；\n
     * .value[1]?.f32：图标大小数值，单位为vp；\n
     * .value[2]?.u32：按钮图标颜色数值，0xargb格式，形如 0xFFFF0000 表示红色；\n
     * ?.string：按钮图标地址，入参内容为图片本地地址，例如 /pages/icon.png。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：按钮样式{@link ArkUI_CancelButtonStyle}；\n
     * .value[1].f32：图标大小数值，单位为vp；\n
     * .value[2].u32：按钮图标颜色数值，0xargb格式；\n
     * .string：按钮图标地址。\n
     *
     */
    NODE_TEXT_INPUT_CANCEL_BUTTON,
    /**
     * @brief 单行文本设置文本选中并高亮的区域，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：选中文本的起始位置；\n
     * .value[1].i32：选中文本的终止位置；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：选中文本的起始位置；\n
     * .value[1].i32：选中文本的终止位置；\n
     *
     */
    NODE_TEXT_INPUT_TEXT_SELECTION,
    /**
    * @brief 开启下划线时，支持配置下划线颜色。
    *
    * 主题配置的默认下划线颜色为'0x33182431'。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].u32：typing，必填，键入时下划线颜色，0xargb类型；\n
    * .value[1].u32：normal，必填，非特殊状态时下划线颜色，0xargb类型；\n
    * .value[2].u32：error，必填，错误时下划线颜色，0xargb类型；\n
    * .value[3].u32：disable，必填，禁用时下划线颜色，0xargb类型；\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .value[0].u32：typing，键入时下划线颜色，0xargb类型；\n
    * .value[1].u32：normal，非特殊状态时下划线颜色，0xargb类型；\n
    * .value[2].u32：error，错误时下划线颜色，0xargb类型；\n
    * .value[3].u32：disable，禁用时下划线颜色，0xargb类型；\n
    *
    */
    NODE_TEXT_INPUT_UNDERLINE_COLOR,
    /**
    * @brief 设置是否启用自动填充。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 是否启用自动填充，默认值true。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 是否启用自动填充。\n
    *
    */
    NODE_TEXT_INPUT_ENABLE_AUTO_FILL,
    /**
    * @brief 自动填充类型。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 参数类型{@link ArkUI_TextInputContentType}。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 参数类型{@link ArkUI_TextInputContentType}。\n
    *
    */
    NODE_TEXT_INPUT_CONTENT_TYPE,
    /**
    * @brief 定义生成密码的规则。在触发自动填充时，所设置的密码规则会透传给密码保险箱，用于新密码的生成。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .string： 定义生成密码的规则。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .string： 定义生成密码的规则。\n
    *
    */
    NODE_TEXT_INPUT_PASSWORD_RULES,
    /**
    * @brief 设置当初始状态，是否全选文本。不支持内联模式。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 是否全选文本，默认值：false。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 是否全选文本。\n
    *
    */
    NODE_TEXT_INPUT_SELECT_ALL,
    /**
    * @brief 通过正则表达式设置输入过滤器。匹配表达式的输入允许显示，不匹配的输入将被过滤。仅支持单个字符匹配，不支持字符串匹配。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .string： 正则表达式。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .string： 正则表达式。\n
    *
    */
    NODE_TEXT_INPUT_INPUT_FILTER,
    /**
    * @brief 设置输入框为默认风格或内联输入风格。
    *
    * 内联输入风格只支持InputType.Normal类型
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 参数类型{@link ArkUI_TextInputStyle}。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 参数类型{@link ArkUI_TextInputStyle}。\n
    *
    */
    NODE_TEXT_INPUT_STYLE,
    /**
    * @brief 设置或获取光标所在位置信息。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * 设置输入光标的位置。
    * .value[0].i32： 从字符串开始到光标所在位置的字符长度。\n
    * 
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * 返回当前光标所在位置信息。在当前帧更新光标位置同时调用该接口，该接口不生效
    * value[0].i32：光标所在位置的索引值。\n
    * value[1].f32：光标相对输入框的x坐标位值。\n
    * value[2].f32：光标相对输入框的y坐标位值。\n
    */
    NODE_TEXT_INPUT_CARET_OFFSET,
    /**
    * @brief 获取已编辑文本内容区域相对组件的位置和大小。
    * 
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * value[0].f32：水平方向横坐标。\n
    * value[1].f32：竖直方向纵坐标。\n
    * value[2].f32：内容宽度大小。\n
    * value[3].f32：内容高度大小。\n
    *
    */
    NODE_TEXT_INPUT_CONTENT_RECT,
    /**
    * @brief 获取已编辑文本内容的行数。
    * 
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * value[0].i32：已编辑文本内容行数。\n
    *
    */
    NODE_TEXT_INPUT_CONTENT_LINE_COUNT,

    /**
     * @brief 设置长按、双击输入框或者右键输入框时，是否不弹出文本选择菜单，支持属性设置，属性重置和属性获取接口。
     * 
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 长按、双击输入框或者右键输入框时，是否不弹出文本选择菜单。默认值false。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 长按、双击输入框或者右键输入框时，是否不弹出文本选择菜单。\n
     *
     */
    NODE_TEXT_INPUT_SELECTION_MENU_HIDDEN,
    /**
     * @brief 设置输入框在submit状态下，触发回车键是否失焦。
     * 
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：是否失焦。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：是否失焦。\n
     *
     */
    NODE_TEXT_INPUT_BLUR_ON_SUBMIT,
    /**
     * @brief 设置自定义键盘。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .object：自定义键盘，参数类型{@Link ArkUI_NodeHandle}。\n
     * .value[0]?.i32：设置自定义键盘是否支持避让功能，默认值false。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .object：自定义键盘，参数类型{@Link ArkUI_NodeHandle}。\n
     * .value[0].i32：设置自定义键盘是否支持避让功能。\n
     *
     */
    NODE_TEXT_INPUT_CUSTOM_KEYBOARD,
    /**
     * @brief 文本断行规则属性，支持属性设置，属性重置，属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32: 参数类型{@link ArkUI_WordBreak}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32: 参数类型{@link ArkUI_WordBreak}。\n
     *
     */
    NODE_TEXT_INPUT_WORD_BREAK,
    /**
     * @brief 多行文本输入框的默认提示文本内容属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string：默认提示文本的内容。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string：默认提示文本的内容。\n
     *
     */
    NODE_TEXT_AREA_PLACEHOLDER = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TEXT_AREA,
    /**
     * @brief 多行文本输入框的默认文本内容属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string：默认文本的内容。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string：默认文本的内容。\n
     *
     */
    NODE_TEXT_AREA_TEXT,
    /**
     * @brief 输入框支持的最大文本数属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：最大文本数的数字。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：最大文本数的数字。\n
     *
     */
    NODE_TEXT_AREA_MAX_LENGTH,
    /**
     * @brief 无输入时默认提示文本的颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式。\n
     *
     */
    NODE_TEXT_AREA_PLACEHOLDER_COLOR,
    /**
     * @brief 无输入时默认提示文本的字体配置（包括大小、字重、样式、字体列表）属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0]?.f32：可选字体大小数值，默认值16.0，单位为fp；\n
     * .value[1]?.i32：可选字体样式{@link ArkUI_FontStyle}，默认值为ARKUI_FONT_STYLE_NORMAL；\n
     * .value[2]?.i32：可选字体粗细样式{@link ArkUI_FontWeight}，默认值为ARKUI_FONT_WEIGHT_NORMAL；\n
     * ?.string: 字体族内容，多个字体族之间使用逗号分隔，形如“字重；字体族1，字体族2”。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：字体大小数值，单位为fp；\n
     * .value[1].i32：字体样式{@link ArkUI_FontStyle}；\n
     * .value[2].i32：字体粗细样式{@link ArkUI_FontWeight}；\n
     * .string: 字体族内容，多个字体族之间使用逗号分隔。\n
     *
     */
    NODE_TEXT_AREA_PLACEHOLDER_FONT,
    /**
     * @brief 光标颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：背景色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：背景色数值，0xargb格式。\n
     *
     */
    NODE_TEXT_AREA_CARET_COLOR,
    /**
     * @brief 控制多行文本输入框编辑态属性，支持属性设置。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示退出编辑态，true表示维持现状。\n
     * \n
     * 属性获取方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示退出编辑态，true表示维持现状。\n
     *
     */
    NODE_TEXT_AREA_EDITING,
    /**
     * @brief 输入框的类型属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：输入框类型枚举{@link ArkUI_TextAreaType}，默认值为ARKUI_TEXTAREA_TYPE_NORMAL。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：输入框类型枚举{@link ArkUI_TextAreaType}。\n
     *
     */
    NODE_TEXT_AREA_TYPE,
    /**
     * @brief 设置输入的字符数超过阈值时是否显示计数器并设置计数器样式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：是否开启计数器，值为true时为开启。\n
     * .value[1]?.f32：可输入字符数占最大字符限制的百分比值，超过此值时显示计数器，取值范围1-100，小数时向下取整。\n
     * .value[2]?.i32：输入字符超出限制时是否高亮边框。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：是否开启计数器。\n
     * .value[1].f32：可输入字符数占最大字符限制的百分比值，超过此值时显示计数器，取值范围1-100。\n
     * .value[2].i32：输入字符超出限制时是否高亮边框，默认高亮。\n
     *
     */
    NODE_TEXT_AREA_SHOW_COUNTER,

    /**
     * @brief 设置长按、双击输入框或者右键输入框时，是否不弹出文本选择菜单，支持属性设置，属性重置和属性获取接口。
     * 
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 长按、双击输入框或者右键输入框时，是否不弹出文本选择菜单。默认值false。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 长按、双击输入框或者右键输入框时，是否不弹出文本选择菜单。\n
     *
     */
    NODE_TEXT_AREA_SELECTION_MENU_HIDDEN,
    /**
     * @brief 设置多行输入框在submit状态下，触发回车键是否失焦。
     * 
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：是否失焦。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：是否失焦。\n
     *
     */
    NODE_TEXT_AREA_BLUR_ON_SUBMIT,
    /**
    * @brief 通过正则表达式设置输入过滤器。匹配表达式的输入允许显示，不匹配的输入将被过滤。仅支持单个字符匹配，不支持字符串匹配。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .string： 正则表达式。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .string： 正则表达式。\n
    *
    */
    NODE_TEXT_AREA_INPUT_FILTER,
    /**
     * @brief 设置文本选中底板颜色，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式。\n
     *
     */
    NODE_TEXT_AREA_SELECTED_BACKGROUND_COLOR,
    /**
     * @brief 设置输入法回车键类型，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：回车键类型枚举{@link ArkUI_EnterKeyType}，默认值为ARKUI_ENTER_KEY_TYPE_DONE。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：回车键类型枚举{@link ArkUI_EnterKeyType}。\n
     *
     */
    NODE_TEXT_AREA_ENTER_KEY_TYPE,
    /**
     * @brief 设置TextArea通过点击以外的方式获焦时，是否绑定输入法，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：false表示聚焦不拉起输入法，true表示拉起，默认值为true。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：0表示聚焦不拉起输入法，1表示拉起。\n
     *
     */
    NODE_TEXT_AREA_ENABLE_KEYBOARD_ON_FOCUS,
    /**
    * @brief 设置或获取光标所在位置信息。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * 设置输入光标的位置。
    * .value[0].i32： 从字符串开始到光标所在位置的字符长度。\n
    * 
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * 返回当前光标所在位置信息。在当前帧更新光标位置同时调用该接口，该接口不生效
    * value[0].i32：光标所在位置的索引值。\n
    * value[1].f32：光标相对输入框的x坐标位值。\n
    * value[2].f32：光标相对输入框的y坐标位值。\n
    */
    NODE_TEXT_AREA_CARET_OFFSET,
    /**
    * @brief 获取已编辑文本内容区域相对组件的位置和大小。
    * 
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * value[0].f32：水平方向横坐标。\n
    * value[1].f32：竖直方向纵坐标。\n
    * value[2].f32：内容宽度大小。\n
    * value[3].f32：内容高度大小。\n
    *
    */
    NODE_TEXT_AREA_CONTENT_RECT,
    /**
    * @brief 获取已编辑文本内容的行数。
    * 
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * value[0].i32：已编辑文本内容行数。\n
    *
    */
    NODE_TEXT_AREA_CONTENT_LINE_COUNT,
    /**
     * @brief 组件在获焦状态下，调用该接口设置文本选择区域并高亮显示，且只有在selectionStart小于selectionEnd时，文字才会被选取、高亮显示。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：选中文本的起始位置；\n
     * .value[1].i32：选中文本的终止位置；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：选中文本的起始位置；\n
     * .value[1].i32：选中文本的终止位置；\n
     *
     */
    NODE_TEXT_AREA_TEXT_SELECTION,
    /**
    * @brief 设置是否启用自动填充。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 是否启用自动填充，默认值true。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 是否启用自动填充。\n
    *
    */
    NODE_TEXT_AREA_ENABLE_AUTO_FILL,
    /**
    * @brief 自动填充类型。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 参数类型{@link ArkUI_TextInputContentType}。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32： 参数类型{@link ArkUI_TextInputContentType}。\n
    *
    */
    NODE_TEXT_AREA_CONTENT_TYPE,
    /**
     * @brief button按钮的文本内容属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string：默认文本的内容。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string：默认文本的内容。\n
     *
     */
    NODE_BUTTON_LABEL = MAX_NODE_SCOPE_NUM * ARKUI_NODE_BUTTON,

    /**
     * @brief Button按钮的样式属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置Button按钮的样式，参数类型{@link ArkUI_ButtonType}，默认值为ARKUI_BUTTON_TYPE_CAPSULE。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：获取Button按钮的样式，参数类型{@link ArkUI_ButtonType}，默认值为ARKUI_BUTTON_TYPE_CAPSULE。 \n
     *
     */
    NODE_BUTTON_TYPE,

    /**
     * @brief 进度条的当前进度值属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：进度条当前值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：进度条当前值。\n
     *
     */
    NODE_PROGRESS_VALUE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_PROGRESS,
    /**
     * @brief 进度条的总长属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：进度条总长。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：进度条总长。\n
     *
     */
    NODE_PROGRESS_TOTAL,
    /**
     * @brief 进度条显示进度值的颜色属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式，形如 0xFFFF0000 表示红色。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：颜色数值，0xargb格式。\n
     *
     */
    NODE_PROGRESS_COLOR,
    /**
     * @brief 进度条的类型属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：进度条类型枚举值{@link ArkUI_ProgressType}，默认值为ARKUI_PROGRESS_TYPE_LINEAR。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：进度条类型枚举值{@link ArkUI_ProgressType}。\n
     *
     */
    NODE_PROGRESS_TYPE,

    /**
     * @brief CheckBox多选框是否选中，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：1表示选中，0表示不选中。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：1表示选中，0表示不选中。
     * 
     */
    NODE_CHECKBOX_SELECT = MAX_NODE_SCOPE_NUM * ARKUI_NODE_CHECKBOX,

    /**
     * @brief CheckBox多选框选中状态颜色，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].u32：多选框选中状态颜色, 类型为0xargb，如0xFF1122FF。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：多选框选中状态颜色, 类型为0xargb，如0xFF1122FF。
     *
     */
    NODE_CHECKBOX_SELECT_COLOR,

    /**
     * @brief CheckBox多选框非选中状态边框颜色，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].u32：边框颜色, 类型为0xargb，如0xFF1122FF。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：边框颜色, 类型为0xargb，如0xFF1122FF。
     * 
     */
    NODE_CHECKBOX_UNSELECT_COLOR,

    /**
     * @brief CheckBox多选框内部图标样式，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].u32：边框颜色, 类型为0xargb，如0xFF1122FF；\n
     * .value[1]?.f32：可选，内部图标大小，单位vp；\n
     * .value[2]?.f32：可选，内部图标粗细，单位vp，默认值2。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：边框颜色, 类型为0xargb，如0xFF1122FF；\n
     * .value[1].f32：内部图标大小，单位vp；\n
     * .value[2].f32：内部图标粗细，单位vp，默认值2。\n
     *
     */
    NODE_CHECKBOX_MARK,

    /**
     * @brief CheckBox组件形状, 支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：组件形状，参数类型{@link ArkUI_CheckboxShape}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：组件形状，参数类型{@link ArkUI_CheckboxShape}。
     *
     */
    NODE_CHECKBOX_SHAPE,

    /**
     * @brief XComponent组件ID属性，支持属性设置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string: ID的内容。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string: ID的内容。\n
     *
     */
    NODE_XCOMPONENT_ID = MAX_NODE_SCOPE_NUM * ARKUI_NODE_XCOMPONENT,
    /**
     * @brief XComponent的类型，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：字体样式{@link ArkUI_XComponentType}，默认值为ARKUI_XCOMPONENT_TYPE_SURFACE；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：字体样式{@link ArkUI_XComponentType}。\n
     *
     */
    NODE_XCOMPONENT_TYPE,
    /**
     * @brief 设置XComponent的宽高，支持属性设置和获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：宽数值，单位为px；\n
     * .value[1].u32：高数值，单位为px；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：宽数值，单位为px；\n
     * .value[1].u32：高数值，单位为px；\n
     *
     */
    NODE_XCOMPONENT_SURFACE_SIZE,

    /**
     * @brief 设置日期选择器组件的日期是否显示农历，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 是否显示农历，默认值false。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 是否显示农历。
     *
     */
    NODE_DATE_PICKER_LUNAR = MAX_NODE_SCOPE_NUM * ARKUI_NODE_DATE_PICKER,
    /**
     * @brief 设置日期选择器组件选择器的起始日期，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 日期，默认值"1970-1-1"。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 日期。\n
     *
     */
    NODE_DATE_PICKER_START,
    /**
     * @brief 设置日期选择器组件选择器的结束日期，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 日期，默认值"2100-12-31"。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 日期。\n
     *
     */
    NODE_DATE_PICKER_END,
    /**
     * @brief 设置日期选择器组件选中项的日期，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 日期，默认值"2024-01-22"。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 日期。
     *
     */
    NODE_DATE_PICKER_SELECTED,
    /**
     * @brief 设置日期选择器组件的所有选项中最上和最下两个选项的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型\n
     *       入参2： 文本大小，数字类型，单位fp\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       入参4： 文本字体列表，使用 ',' 进行分割\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型\n
     *       参数2： 文本大小，数字类型，单位fp\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       参数4： 文本字体列表，使用 ',' 进行分割\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_DATE_PICKER_DISAPPEAR_TEXT_STYLE,
    /**
     * @brief 设置日期选择器组件的所有选项中除了最上、最下及选中项以外的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型\n
     *       入参2： 文本大小，数字类型，单位fp\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       入参4： 文本字体列表，使用 ',' 进行分割\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型\n
     *       参数2： 文本大小，数字类型，单位fp\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       参数4： 文本字体列表，使用 ',' 进行分割\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_DATE_PICKER_TEXT_STYLE,
    /**
     * @brief 设置日期选择器组件的选中项的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型\n
     *       入参2： 文本大小，数字类型，单位fp\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       入参4： 文本字体列表，使用 ',' 进行分割\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型\n
     *       参数2： 文本大小，数字类型，单位fp\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       参数4： 文本字体列表，使用 ',' 进行分割\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_DATE_PICKER_SELECTED_TEXT_STYLE,
    /**
     * @brief 设置时间选择组件选中项的时间，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 时间，默认值当前系统时间。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 时间。
     *
     */

    NODE_TIME_PICKER_SELECTED = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TIME_PICKER,
    /**
     * @brief 设置时间选择组件展示时间是否为24小时制，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 是否为24小时制，默认值false。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 是否为24小时制。
     *
     */
    NODE_TIME_PICKER_USE_MILITARY_TIME,
    /**
     * @brief 设置时间选择组件所有选项中最上和最下两个选项的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型\n
     *       入参2： 文本大小，数字类型，单位fp\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       入参4： 文本字体列表，使用 ',' 进行分割\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型\n
     *       参数2： 文本大小，数字类型，单位fp\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       参数4： 文本字体列表，使用 ',' 进行分割\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_TIME_PICKER_DISAPPEAR_TEXT_STYLE,
    /**
     * @brief 设置时间选择组件所有选项中除了最上、最下及选中项以外的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型\n
     *       入参2： 文本大小，数字类型，单位fp\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       入参4： 文本字体列表，使用 ',' 进行分割\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型\n
     *       参数2： 文本大小，数字类型，单位fp\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       参数4： 文本字体列表，使用 ',' 进行分割\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_TIME_PICKER_TEXT_STYLE,
    /**
     * @brief 设置时间选择组件选中项的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型\n
     *       入参2： 文本大小，数字类型，单位fp\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       入参4： 文本字体列表，使用 ',' 进行分割\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型\n
     *       参数2： 文本大小，数字类型，单位fp\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       参数4： 文本字体列表，使用 ',' 进行分割\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_TIME_PICKER_SELECTED_TEXT_STYLE,

    /**
     * @brief 设置滑动选择文本选择器的选择列表，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：使用的选择器类型{@link ArkUI_TextPickerRangeType}，默认值为ARKUI_TEXTPICKER_RANGETYPE_SINGLE；\n
     * ?.string：针对不同选择器类型有如下输入范式：\n
     * 1：单列选择器，入参格式为用分号分隔的一组字符串；\n
     * 2：多列选择器，支持多对纯文本字符串对，多对之间使用分号分隔，每对内部使用逗号分隔；\n
     * ?.object：针对不同选择器类型有如下输入范式：\n
     * 1：单列支持图片的选择器，输入结构体为{@link ARKUI_TextPickerRangeContent}；\n
     * 2：多列联动选择器，输入结构体为{@link ARKUI_TextPickerCascadeRangeContent}；\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：使用的选择器类型{@link ArkUI_TextPickerRangeType}；\n
     * ?.string：针对不同选择器类型有如下输出范式：\n
     * 1：单列选择器，输出格式为用分号分隔的一组字符串；\n
     * 2：多列选择器，输出多对纯文本字符串对，多对之间使用分号分隔，每对内部使用逗号分隔；\n
     * ?.object：针对不同选择器类型有如下输出范式：\n
     * 1：单列支持图片的选择器，输出结构体为{@link ARKUI_TextPickerRangeContent}；\n
     * 2：多列联动选择器，输出结构体为{@link ARKUI_TextPickerCascadeRangeContent}；\n
     *
     */
    NODE_TEXT_PICKER_OPTION_RANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TEXT_PICKER,
    /**
     * @brief 设置滑动选择文本内容的组件默认选中项在数组中的索引值，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：索引值，如存在多个索引值则逐个添加。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32：索引值，如存在多个索引值则逐个添加；\n
     *
     */
    NODE_TEXT_PICKER_OPTION_SELECTED,
    /**
     * @brief 设置滑动选择文本内容的组件默认选中项的值，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string：选中项的值，如存在多个值则逐个添加，用分号分隔。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string：选中项的值，如存在多个值则逐个添加，用分号分隔；\n
     *
     */
    NODE_TEXT_PICKER_OPTION_VALUE,
    /**
     * @brief 设置滑动选择文本内容的组件所有选项中最上和最下两个选项的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型\n
     *       入参2： 文本大小，数字类型，单位fp\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       入参4： 文本字体列表，使用 ',' 进行分割\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型\n
     *       参数2： 文本大小，数字类型，单位fp\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       参数4： 文本字体列表，使用 ',' 进行分割\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_TEXT_PICKER_DISAPPEAR_TEXT_STYLE,
    /**
     * @brief 设置滑动选择文本内容的组件所有选项中除了最上、最下及选中项以外的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型\n
     *       入参2： 文本大小，数字类型，单位fp\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       入参4： 文本字体列表，使用 ',' 进行分割\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型\n
     *       参数2： 文本大小，数字类型，单位fp\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")\n
     *       参数4： 文本字体列表，使用 ',' 进行分割\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_TEXT_PICKER_TEXT_STYLE,
    /**
     * @brief 设置滑动选择文本内容的组件选中项的文本颜色、字号、字体粗细，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .string： 入参5个，格式为字符串，以 ';' 分割：\n
     *       入参1： 文本颜色，#argb类型；\n
     *       入参2： 文本大小，数字类型，单位fp；\n
     *       入参3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")；\n
     *       入参4： 文本字体列表，使用 ',' 进行分割；\n
     *       入参5： 文本样式，字符串枚举("normal", "italic")；\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .string： 参数5个，格式为字符串，以 ';' 分割：\n
     *       参数1： 文本颜色，#argb类型；\n
     *       参数2： 文本大小，数字类型，单位fp；\n
     *       参数3： 文本粗细，字符串枚举("bold", "normal", "bolder", "lighter", "medium", "regular")；\n
     *       参数4： 文本字体列表，使用 ',' 进行分割；\n
     *       参数5： 文本样式，字符串枚举("normal", "italic")；\n
     *       如 "#ff182431;14;normal;Arial,HarmonyOS Sans;normal" 。\n
     *
     */
    NODE_TEXT_PICKER_SELECTED_TEXT_STYLE,
    /**
     * @brief 设置滑动选择文本内容的组件默认选中项在数组中的索引值，支持属性设置，属性重置和属性获取接口。
     *
     * {@link ArkUI_AttributeItem}参数类型：\n
     * .value[0...].i32：默认选中项在数组中的索引值数组。
     *
     */
    NODE_TEXT_PICKER_SELECTED_INDEX,
    /**
     * @brief Picker组件可循环滚动属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：false表示不可循环，true表示可循环。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * value[0].i32：0表示不可循环，1表示可循环。\n
     *
     */
    NODE_TEXT_PICKER_CAN_LOOP,
    /**
     * @brief Picker各选择项的高度属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：子项高度属性，单位为vp。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * value[0].f32：子项高度属性，单位为vp。 \n
     *
     */
    NODE_TEXT_PICKER_DEFAULT_PICKER_ITEM_HEIGHT,
    /**
     * @brief 设置日历选中态底板圆角半径的参数，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 日历选中态底板圆角半径，取值范围[0,+∞)，其中取值为0表示底板样式为直角矩形；
     * 取值范围为(0, 16)时，底板样式为圆角矩形；取值范围为[16,+∞)时，底板样式为圆形。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 日历选中态底板圆角半径，取值范围[0,+∞)，其中取值为0表示底板样式为直角矩形；
     * 取值范围为(0, 16)时，底板样式为圆角矩形；取值范围为[16,+∞)时，底板样式为圆形。\n
     *
     */
    NODE_CALENDAR_PICKER_HINT_RADIUS = MAX_NODE_SCOPE_NUM * ARKUI_NODE_CALENDAR_PICKER,
    /**
     * @brief 设置日历选择选中日期的参数，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32： 选中的年。\n
     * .value[1].u32： 选中的月。\n
     * .value[2].u32： 选中的日。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32： 选中的年。\n
     * .value[1].u32： 选中的月。\n
     * .value[2].u32： 选中的日。\n
     *
     */
    NODE_CALENDAR_PICKER_SELECTED_DATE,
    /**
     * @brief 设置日历选择器与入口组件的对齐方式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 对齐方式类型，参数类型{@link ArkUI_CalendarAlignment}。\n
     * .value[1]?.f32： 按照对齐方式对齐后，选择器相对入口组件的x轴方向相对偏移。\n
     * .value[2]?.f32： 按照对齐方式对齐后，选择器相对入口组件的y轴方向相对偏移。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 对齐方式类型，参数类型{@link ArkUI_CalendarAlignment}。\n
     * .value[1].f32： 按照对齐方式对齐后，选择器相对入口组件的x轴方向相对偏移。\n
     * .value[2].f32： 按照对齐方式对齐后，选择器相对入口组件的y轴方向相对偏移。\n
     *
     */
    NODE_CALENDAR_PICKER_EDGE_ALIGNMENT,
    /**
     * @brief 设置日历选择器入口区的文本颜色、字号、字体粗细。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0]?.u32： 入口区的文本颜色。\n
     * .value[1]?.f32： 入口区的文本字号，单位为fp。\n
     * .value[2]?.i32： 入口区的文本字体粗细，参数类型{@link ArkUI_FontWeight}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32： 入口区的文本颜色。\n
     * .value[1].f32： 入口区的文本字号，单位为fp。\n
     * .value[2].i32： 入口区的文本字体粗细，参数类型{@link ArkUI_FontWeight}。\n
     *
     */
    NODE_CALENDAR_PICKER_TEXT_STYLE,
    /**
     * @brief Slider滑块的颜色，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].u32：滑块的颜色, 类型为0xargb，如0xFF1122FF。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：滑块的颜色, 类型为0xargb，如0xFF1122FF。
     *
     */
    NODE_SLIDER_BLOCK_COLOR = MAX_NODE_SCOPE_NUM * ARKUI_NODE_SLIDER,

    /**
     * @brief Slider滑轨的背景颜色，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].u32：背景颜色, 类型为0xargb，如0xFF1122FF。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：背景颜色, 类型为0xargb，如0xFF1122FF。
     *
     */
    NODE_SLIDER_TRACK_COLOR,

    /**
     * @brief Slider滑轨的已滑动部分颜色，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].u32：已滑动部分颜色, 类型为0xargb，如0xFF1122FF。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].u32：已滑动部分颜色, 类型为0xargb，如0xFF1122FF。
     *
     */
    NODE_SLIDER_SELECTED_COLOR,

    /**
     * @brief 设置是否显示步长刻度值，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：是否显示步长刻度值，1表示显示，0表示不显示，默认值为0。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：是否显示步长刻度值，1表示显示，0表示不显示，默认值为0。\n
     *
     */
    NODE_SLIDER_SHOW_STEPS,

    /**
     * @brief Slider滑块形状参数，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：形状参数。参数类型{@link ArkUI_SliderBlockStyle}。\n
     * .string? 可选值，根据形状参数而定。\n
     * ARKUI_SLIDER_BLOCK_STYLE_IMAGE: 滑块图片资源。如/pages/common/icon.png。\n
     * ARKUI_SLIDER_BLOCK_STYLE_SHAPE: 滑块使用的自定义形状。\n
     * 共有5种类型： \n
     * 1.rect类型： \n
     * .value[1].i32：裁剪类型，参数类型{@link ArkUI_ShapeType}，ARKUI_SHAPE_TYPE_RECTANGLE； \n
     * .value[2].f32：矩形宽度； \n
     * .value[3].f32：矩形高度； \n
     * .value[4].f32：矩形圆角宽度； \n
     * .value[5].f32：矩形圆角高度； \n
     * 2.circle类型： \n
     * .value[1].i32：裁剪类型，参数类型{@link ArkUI_ShapeType}，ARKUI_SHAPE_TYPE_CIRCLE； \n
     * .value[2].f32：圆形宽度； \n
     * .value[3].f32：圆形高度； \n
     * 3.ellipse类型： \n
     * .value[1].i32：裁剪类型，参数类型{@link ArkUI_ShapeType}，ARKUI_SHAPE_TYPE_ELLIPSE； \n
     * .value[2].f32：椭圆形宽度； \n
     * .value[3].f32：椭圆形高度； \n
     * 4.path类型： \n
     * .value[1].i32：裁剪类型，参数类型{@link ArkUI_ShapeType}，ARKUI_SHAPE_TYPE_PATH； \n
     * .value[2].f32：路径宽度； \n
     * .value[3].f32：路径高度； \n
     * .string：路径绘制的命令字符串； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：形状参数。参数类型{@link ArkUI_SliderBlockStyle}。\n
     * .string? 可选值，根据形状参数而定。\n
     * ARKUI_SLIDER_BLOCK_STYLE_IMAGE: 滑块图片资源。如/pages/common/icon.png。\n
     * ARKUI_SLIDER_BLOCK_STYLE_SHAPE: 滑块使用的自定义形状。\n
      * 共有5种类型： \n
     * 1.rect类型： \n
     * .value[1].i32：裁剪类型，参数类型{@link ArkUI_ShapeType}，ARKUI_SHAPE_TYPE_RECTANGLE； \n
     * .value[2].f32：矩形宽度； \n
     * .value[3].f32：矩形高度； \n
     * .value[4].f32：矩形圆角宽度； \n
     * .value[5].f32：矩形圆角高度； \n
     * 2.circle类型： \n
     * .value[1].i32：裁剪类型，参数类型{@link ArkUI_ShapeType}，ARKUI_SHAPE_TYPE_CIRCLE； \n
     * .value[2].f32：圆形宽度； \n
     * .value[3].f32：圆形高度； \n
     * 3.ellipse类型： \n
     * .value[1].i32：裁剪类型，参数类型{@link ArkUI_ShapeType}，ARKUI_SHAPE_TYPE_ELLIPSE； \n
     * .value[2].f32：椭圆形宽度； \n
     * .value[3].f32：椭圆形高度； \n
     * 4.path类型： \n
     * .value[1].i32：裁剪类型，参数类型{@link ArkUI_ShapeType}，ARKUI_SHAPE_TYPE_PATH； \n
     * .value[2].f32：路径宽度； \n
     * .value[3].f32：路径高度； \n
     * .string：路径绘制的命令字符串； \n
     *
     */
    NODE_SLIDER_BLOCK_STYLE,

    /**
     * @brief slider进度值，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32：进度值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：进度值。
     *
     */
    NODE_SLIDER_VALUE,

    /**
     * @brief slider最小值，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32：进度值的最小值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：进度值的最小值。
     *
     */
    NODE_SLIDER_MIN_VALUE,

    /**
     * @brief slider最大值，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32：进度值的最大值。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：进度值的最大值。
     *
     */
    NODE_SLIDER_MAX_VALUE,

    /**
     * @brief Slider滑动步长，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32：滑动步长，取值范围：[0.01, 100]。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：滑动步长，取值范围：[0.01, 100]。
     *
     */
    NODE_SLIDER_STEP,

    /**
     * @brief Slider滑动条滑动方向，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：显示样式，参数类型{@link ArkUI_SliderDirection}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：显示样式，参数类型{@link ArkUI_SliderDirection}。
     *
     */
    NODE_SLIDER_DIRECTION,

    /**
     * @brief Slider滑动条取值范围是否反向，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：是否反向，1表示反向，0表示不反向。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：是否反向，1表示反向，0表示不反向。
     *
     */
    NODE_SLIDER_REVERSE,

    /**
     * @brief Slider的滑块与滑轨显示样式，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：显示样式，参数类型{@link ArkUI_SliderStyle}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：显示样式，参数类型{@link ArkUI_SliderStyle}。
     *
     */
    NODE_SLIDER_STYLE,

    /**
     * @brief Slider滑块的滑轨粗细属性，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32：滑轨的粗细，单位为vp；当参数NODE_SLIDER_STYLE的值设置为ARKUI_SLIDER_STYLE_OUT_SET时为4.0vp，设置为ARKUI_SLIDER_STYLE_IN_SET时为20.0vp \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：滑轨的粗细，单位为vp； \n
     *
     */
    NODE_SLIDER_TRACK_THICKNESS,

    /**
     * @brief 设置单选框的选中状态，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：单选框的选中状态，默认值false。
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：单选框的选中状态。
     *
     */
    NODE_RADIO_CHECKED = MAX_NODE_SCOPE_NUM * ARKUI_NODE_RADIO,
    /**
     * @brief 设置单选框选中状态和非选中状态的样式，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0]?.u32：开启状态底板颜色, 类型为0xargb，默认值为0xFF007DFF。\n
     * .value[1]?.u32：关闭状态描边颜色, 类型为0xargb，默认值为0xFF182431。\n
     * .value[2]?.u32：开启状态内部圆饼颜色, 类型为0xargb，默认值为0xFFFFFFFF。\n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0]?.u32：开启状态底板颜色, 类型为0xargb，默认值为0xFF007DFF。\n
     * .value[1]?.u32：关闭状态描边颜色, 类型为0xargb，默认值为0xFF182431。\n
     * .value[2]?.u32：开启状态内部圆饼颜色, 类型为0xargb，默认值为0xFFFFFFF。\n
     *
     */
    NODE_RADIO_STYLE,

    /**
     * @brief 设置子组件在容器内的对齐方式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 对齐方式，数据类型{@link ArkUI_Alignment}，默认值ARKUI_ALIGNMENT_CENTER。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 对齐方式，数据类型{@link ArkUI_Alignment}。\n
     *
     */
    NODE_STACK_ALIGN_CONTENT = MAX_NODE_SCOPE_NUM * ARKUI_NODE_STACK,

    /**
     * @brief 设置滚动条状态，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 滚动条状态，数据类型{@link ArkUI_ScrollBarDisplayMode}，默认值ARKUI_SCROLL_BAR_DISPLAY_MODE_AUTO。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 滚动条状态，数据类型{@link ArkUI_ScrollBarDisplayMode}。\n
     *
     */
    NODE_SCROLL_BAR_DISPLAY_MODE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_SCROLL,
    /**
     * @brief 设置滚动条的宽度，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 滚动条宽度，单位vp，默认值4。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 滚动条宽度，单位vp。\n
     *
     */
    NODE_SCROLL_BAR_WIDTH,
    /**
     * @brief 设置滚动条的颜色，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .data[0].u32： 滚动条颜色，0xargb类型。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .data[0].u32： 滚动条颜色，0xargb类型。\n
     *
     */
    NODE_SCROLL_BAR_COLOR,
    /**
     * @brief 设置滚动方向，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：滚动方向，数据类型{@link ArkUI_ScrollDirection}，默认值ARKUI_SCROLL_DIRECTION_VERTICAL。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：滚动方向，数据类型{@link ArkUI_ScrollDirection}。\n
     *
     */
    NODE_SCROLL_SCROLL_DIRECTION,
    /**
     * @brief 设置边缘滑动效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 边缘滑动效果，参数类型{@link ArkUI_EdgeEffect}，默认值ARKUI_EDGE_EFFECT_NONE；\n
     * .value[1]?.i32： 可选值，组件内容大小小于组件自身时，设置是否开启滑动效果，开启为1，关闭为0，默认值1。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 边缘滑动效果，参数类型{@link ArkUI_EdgeEffect}；\n
     * .value[1].i32： 组件内容大小小于组件自身时，设置是否开启滑动效果，开启为1，关闭为0。\n
     *
     */
    NODE_SCROLL_EDGE_EFFECT,
    /**
     * @brief 设置是否支持滚动手势，当设置为false时，无法通过手指或者鼠标滚动，但不影响控制器的滚动接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 是否支持滚动手势，默认值true。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 是否支持滚动手势。\n
     *
     */
    NODE_SCROLL_ENABLE_SCROLL_INTERACTION,
    /**
     * @brief 设置摩擦系数，手动划动滚动区域时生效，只对惯性滚动过程有影响，对惯性滚动过程中的链式效果有间接影响。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 摩擦系数，默认值：非可穿戴设备为0.6，可穿戴设备为0.9。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 摩擦系数。
     *
     */
    NODE_SCROLL_FRICTION,
    /**
     * @brief 设置Scroll组件的限位滚动模式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： Scroll组件限位滚动时的对其方式，数据类型{@link ArkUI_ScrollSnapAlign}，默认值ARKUI_SCROLL_SNAP_ALIGN_NONE；\n
     * .value[1].i32： 在Scroll组件限位滚动模式下，该属性设置为false后，运行Scroll在开头和第一个限位点间自由滑动。默认值true，仅在限位点为多个时生效；\n
     * .value[2].i32： 在Scroll组件限位滚动模式下，该属性设置为false后，运行Scroll在最后一个限位点和末尾间自由滑动。默认值true，仅在限位点为多个时生效；\n
     * .value[3...].f32： Scroll组件限位滚动时的限位点，限位点即为Scroll组件能滑动停靠的偏移量。可以1个或多个。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： Scroll组件限位滚动时的对其方式，数据类型{@link ArkUI_ScrollSnapAlign}；\n
     * .value[1].i32： 在Scroll组件限位滚动模式下，该属性设置为false后，运行Scroll在开头和第一个限位点间自由滑动；\n
     * .value[2].i32： 在Scroll组件限位滚动模式下，该属性设置为false后，运行Scroll在最后一个限位点和末尾间自由滑动；\n
     * .value[3...].f32： Scroll组件限位滚动时的限位点，限位点即为Scroll组件能滑动停靠的偏移量。\n
     *
     */
    NODE_SCROLL_SNAP,

    /**
     * @brief Scroll嵌套滚动选项，支持属性设置，属性重置和属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：可滚动组件往末尾端滚动时的嵌套滚动，参数类型{@link ArkUI_ScrollNestedMode}。\n
     * .value[1].i32：可滚动组件往起始端滚动时的嵌套滚动，参数类型{@link ArkUI_ScrollNestedMode}。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：可滚动组件往末尾端滚动时的嵌套滚动，参数类型{@link ArkUI_ScrollNestedMode}。\n
     * .value[1].i32：可滚动组件往起始端滚动时的嵌套滚动，参数类型{@link ArkUI_ScrollNestedMode}。
     *
     */
    NODE_SCROLL_NESTED_SCROLL,
    /**
     * @brief Scroll滑动到指定位置，支持属性设置，属性重置和属性获取。
     * 
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32：水平滑动偏移，单位为vp。\n
     * .value[1].f32：垂直滑动偏移，单位为vp。\n
     * .value[2]?.i32：可选值，滚动时长，单位为毫秒。\n
     * .value[3]?.i32：可选值，滚动曲线，参数类型{@link ArkUI_AnimationCurve}。默认值为ARKUI_CURVE_EASE。\n
     * .value[4]?.i32：可选值，是否使能默认弹簧动效，默认值为0不使能。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：水平滑动偏移，单位为vp。\n
     * .value[1].f32：垂直滑动偏移，单位为vp。\n
     *
     */
    NODE_SCROLL_OFFSET,

    /**
     * @brief Scroll滚动到容器边缘，支持属性设置，属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：容器边缘，参数类型{@link ArkUI_ScrollEdge}。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：容器是否位于边缘，-1：表示未处于边缘，如果处于边缘状态参数类型{@link ArkUI_ScrollEdge}。
     *
     */
    NODE_SCROLL_EDGE,

    /**
     * @brief 设置是否支持滑动翻页，支持属性设置，属性重置和属性获取接口。
     * 
     * 如果同时设置了划动翻页enablePaging和限位滚动scrollSnap，则scrollSnap优先生效，enablePaging不生效。\n
     * \n
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 是否支持划动翻页，默认值false。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32： 是否支持划动翻页。\n
     *
     */
    NODE_SCROLL_ENABLE_PAGING,

    /**
     * @brief 滚动到下一页或者上一页。
     * 
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32 是否向下翻页。1表示向下翻页，0表示向上翻页。\n
     * .value[1]?.i32 是否开启翻页动画效果。1有动画，0无动画。默认值：0。\n
     *
     */
    NODE_SCROLL_PAGE,

    /**
     * @brief 滑动指定距离。
     * 
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：水平方向滚动距离，单位为vp; \n
     * .value[1].f32: 竖直方向滚动距离，单位为vp。 \n
     *
     */
    NODE_SCROLL_BY,

    /**
     * @brief 设置List组件排列方向，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：List组件排列方向，数据类型{@link ArkUI_Axis}，默认值ARKUI_AXIS_VERTICAL。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：List组件排列方向，数据类型{@link ArkUI_Axis}。\n
     *
     */
    NODE_LIST_DIRECTION = MAX_NODE_SCOPE_NUM * ARKUI_NODE_LIST,
    /**
     * @brief 配合ListItemGroup组件使用，设置ListItemGroup中header和footer是否要吸顶或吸底，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：配合ListItemGroup组件使用，设置ListItemGroup中header和footer是否要吸顶或吸底。数据类型{@link ArkUI_StickyStyle}，默认值ARKUI_STICKY_STYLE_NONE。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：配合ListItemGroup组件使用，设置ListItemGroup中header和footer是否要吸顶或吸底。数据类型{@link ArkUI_StickyStyle}。
     *
     */
    NODE_LIST_STICKY,
    /**
     * @brief 设置列表项间距，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 子组件主轴方向的间隔。默认值0。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32： 子组件主轴方向的间隔。\n
     *
     */
    NODE_LIST_SPACE,


    /**
    * @brief list组件适配器，支持属性设置，属性重置和属性获取接口。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .object：使用{@link ArkUI_NodeAdapter}对象作为适配器。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
    * .object：返回值格式为{@link ArkUI_NodeAdapter}。\n
    */
    NODE_LIST_NODE_ADAPTER,

    /**
    * @brief list组件Adapter缓存数量，支持属性设置，属性重置和属性获取接口。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32：配合List组件Adapter使用，设置adapter中的缓存数量\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：adapter中的缓存数量。\n
    */
    NODE_LIST_CACHED_COUNT,

    /**
     * @brief 滑动到指定index。
     * 
     * 开启smooth动效时，会对经过的所有item进行加载和布局计算，当大量加载item时会导致性能问题。\n
     * \n
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：要滑动到的目标元素在当前容器中的索引值。\n
     * .value[1]?.i32：设置滑动到列表项在列表中的索引值时是否有动效，1表示有动效，0表示没有动效。默认值：0。\n
     * .value[2]?.i32：指定滑动到的元素与当前容器的对齐方式，参数类型{@link ArkUI_ScrollAlignment}, 默认值：ARKUI_SCROLL_ALIGNMENT_START。\n
     *
     */
    NODE_LIST_SCROLL_TO_INDEX,

    /**
    * @brief 设置List交叉轴方向宽度大于ListItem交叉轴宽度 * lanes时，
    * ListItem在List交叉轴方向的布局方式，支持属性设置，属性重置和属性获取接口。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：交叉轴方向的布局方式。参数类型{@link ArkUI_ListItemAlign} \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：交叉轴方向的布局方式。参数类型{@link ArkUI_ListItemAlign}  \n
    */
    NODE_LIST_ALIGN_LIST_ITEM,

    /**
     * @brief Swiper是否开启循环，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制是否开启循环，0表示不循环，1表示循环，默认值为1。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制是否开启循环，0表示不循环，1表示循环，默认值为1。 \n
     *
     */
    NODE_SWIPER_LOOP = MAX_NODE_SCOPE_NUM * ARKUI_NODE_SWIPER,
    /**
     * @brief Swiper子组件是否自动播放，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制子组件是否自动播放，0表示不自动播放，1表示自动播放，默认值为0。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：控制子组件是否自动播放，0表示不自动播放，1表示自动播放，默认值为0。 \n
     *
     */
    NODE_SWIPER_AUTO_PLAY,
    /**
     * @brief Swiper是否显示导航点指示器，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：是否显示导航点指示器，0表示不显示导航点指示器，1表示显示导航点指示器，默认值为1。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：是否显示导航点指示器，0表示不显示导航点指示器，1表示显示导航点指示器，默认值为1。 \n
     *
     */
    NODE_SWIPER_SHOW_INDICATOR,
    /**
     * @brief 设置Swiper自动播放时播放的时间间隔，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：使用自动播放时播放的时间间隔，单位为毫秒。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：使用自动播放时播放的时间间隔，单位为毫秒。 \n
     *
     */
    NODE_SWIPER_INTERVAL,
    /**
     * @brief 设置Swiper是否为纵向滑动，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：是否为纵向滑动，0表示横向滑动，1表示纵向滑动，默认值为0。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：是否为纵向滑动，0表示横向滑动，1表示纵向滑动，默认值为0。 \n
     *
     */
    NODE_SWIPER_VERTICAL,

    /**
     * @brief 设置Swiper子组件切换的动画时长，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：子组件切换的动画时长，单位为毫秒, 默认值为400。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：子组件切换的动画时长，单位为毫秒, 默认值为400。 \n
     *
     */
    NODE_SWIPER_DURATION,

    /**
     * @brief 设置Swiper的动画曲线，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置动画曲线参数，参数类型{@link ArkUI_AnimationCurve}，默认值为ARKUI_CURVE_LINEAR。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置动画曲线参数，参数类型{@link ArkUI_AnimationCurve}，默认值为ARKUI_CURVE_LINEAR。 \n
     *
     */
    NODE_SWIPER_CURVE,

    /**
     * @brief 设置Swiper子组件与子组件之间间隙，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：子组件与子组件之间间隙数值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：子组件与子组件之间间隙数值。 \n
     *
     */
    NODE_SWIPER_ITEM_SPACE,

    /**
     * @brief 设置Swiper当前在容器中显示的子组件的索引值，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件的索引值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件的索引值。 \n
     *
     */
    NODE_SWIPER_INDEX,

    /**
     * @brief 设置Swiper一页内元素显示个数，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件的索引值。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件的索引值。 \n
     *
     */
    NODE_SWIPER_DISPLAY_COUNT,

    /**
     * @brief 设置Swiper禁用组件滑动切换功能，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：是否禁用组件滑动切换功能，0表示不禁用滑动切换功能，1表示禁用滑动切换功能，默认值为0。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：是否禁用组件滑动切换功能，0表示不禁用滑动切换功能，1表示禁用滑动切换功能，默认值为0。 \n
     *
     */
    NODE_SWIPER_DISABLE_SWIPE,

    /**
     * @brief 设置Swiper是否显示导航点箭头，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置是否显示导航点箭头，参数类型{@link ArkUI_SwiperArrow}， \n
     * 默认值为ARKUI_SWIPER_ARROW_HIDE。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：设置是否显示导航点箭头，参数类型{@link ArkUI_SwiperArrow}， \n
     * 默认值为ARKUI_SWIPER_ARROW_HIDE。 \n
     *
     */
    NODE_SWIPER_SHOW_DISPLAY_ARROW,

    /**
     * @brief 设置Swiper的边缘滑动效果，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32: 边缘滑动效果，参数类型{@link ArkUI_EdgeEffect}， \n
     * 默认值为ARKUI_EDGE_EFFECT_SPRING。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32: 边缘滑动效果，参数类型{@link ArkUI_EdgeEffect}， \n
     *
     */
    NODE_SWIPER_EDGE_EFFECT_MODE,

    /**
    * @brief swiper组件适配器，支持属性设置，属性重置和属性获取接口。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .object：使用{@link ArkUI_NodeAdapter}对象作为适配器。\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
    * .object：返回值格式为{@link ArkUI_NodeAdapter}。 \n
    */
    NODE_SWIPER_NODE_ADAPTER,

    /**
    * @brief swiper组件Adapter缓存数量，支持属性设置，属性重置和属性获取接口。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32：配合swiper组件Adapter使用，设置adapter中的缓存数量\n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
    * .value[0].i32：adapter中的缓存数量。 \n
    */
    NODE_SWIPER_CACHED_COUNT,

    /**
     * @brief 设置 Swiper 组件的前边距，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：前边距数值，单位为vp，默认值为0。 \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：前边距数值，单位为vp。
     */
    NODE_SWIPER_PREV_MARGIN,

    /**
     * @brief 设置 Swiper 组件的后边距，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：后边距数值，单位为vp，默认值为0。 \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：后边距数值，单位为vp。
     */
    NODE_SWIPER_NEXT_MARGIN,

    /**
     * @brief 设置 Swiper 组件的导航指示器类型，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：设置导航指示器的类型，参数类型{@link ArkUI_SwiperIndicatorType}。 \n
     * .object：参数类型为{@link ArkUI_SwiperIndicator}。 \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：导航指示器的类型，参数类型{@link ArkUI_SwiperIndicatorType}。 \n
     * .object：参数类型为{@link ArkUI_SwiperIndicator}。 \n
     *
     */
    NODE_SWIPER_INDICATOR,

    /**
    * @brief 设置Swiper组件和父组件的嵌套滚动模式。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
    * .value[0].i32：Swiper组件和父组件的嵌套滚动模式，参数类型{@link ArkUI_SwiperNestedScrollMode} \n
    * 默认值为：ARKUI_SWIPER_NESTED_SRCOLL_SELF_ONLY \n
    * \n
    * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
    * .value[0].i32：Swiper组件和父组件的嵌套滚动模式，参数类型{@link ArkUI_SwiperNestedScrollMode} \n
    */
    NODE_SWIPER_NESTED_SCROLL,

    /**
    * @brief 设置swiper组件翻至指定页面。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32：指定页面在Swiper中的索引值。\n
    * .value[1]?.i32：设置翻至指定页面时是否有动效。1表示有动效，0表示没有动效, 默认值：0。\n
    */
    NODE_SWIPER_SWIPE_TO_INDEX,

    /**
     * @brief 设置 ListItemGroup 头部组件，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .object：使用{@link ArkUI_NodeHandle}对象作为ListItemGroup头部组件。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .object：使用{@link ArkUI_NodeHandle}对象作为ListItemGroup头部组件。\n
     *
     */
    NODE_LIST_ITEM_GROUP_SET_HEADER = MAX_NODE_SCOPE_NUM * ARKUI_NODE_LIST_ITEM_GROUP,
    /**
     * @brief 设置 ListItemGroup 尾部组件，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .object：使用{@link ArkUI_NodeHandle}对象作为ListItemGroup尾部组件。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .object：使用{@link ArkUI_NodeHandle}对象作为ListItemGroup尾部组件。\n
     *
     */
    NODE_LIST_ITEM_GROUP_SET_FOOTER,
    /**
     * @brief 设置ListItem分割线样式，默认无分割线，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32： 颜色，0xargb类型；\n
     * .value[1].f32： 分割线宽，单位vp；\n
     * .value[2].f32： 分割线距离列表侧边起始端的距离，单位vp；\n
     * .value[3].f32： 分割线距离列表侧边结束端的距离，单位vp。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式：\n
     * .value[0].u32： 颜色，0xargb类型；\n
     * .value[1].f32： 分割线宽，单位vp；\n
     * .value[2].f32： 分割线距离列表侧边起始端的距离，单位vp；\n
     * .value[3].f32： 分割线距离列表侧边结束端的距离，单位vp。\n
     *
     */
    NODE_LIST_ITEM_GROUP_SET_DIVIDER,

    /**
     * @brief 设置Column子组件在水平方向上的对齐格式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在水平方向上的对齐格式，数据类型{@link ArkUI_HorizontalAlignment}， \n
     * 默认值ARKUI_HORIZONTAL_ALIGNMENT_CENTER。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在水平方向上的对齐格式，数据类型{@link ArkUI_HorizontalAlignment}。 \n
     *
     */
    NODE_COLUMN_ALIGN_ITEMS = MAX_NODE_SCOPE_NUM * ARKUI_NODE_COLUMN,
    /**
     * @brief 设置Column子组件在垂直方向上的对齐格式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在垂直方向上的对齐格式，数据类型{@link ArkUI_FlexAlignment}， \n
     * 默认值ARKUI_FLEX_ALIGNMENT_START。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在垂直方向上的对齐格式，数据类型{@link ArkUI_FlexAlignment}。 \n
     *
     */
    NODE_COLUMN_JUSTIFY_CONTENT,

    /**
     * @brief 设置Row子组件在垂直方向上的对齐格式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在垂直方向上的对齐格式，数据类型{@link ArkUI_VerticalAlignment}， \n
     * 默认值ARKUI_VERTICAL_ALIGNMENT_CENTER。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在垂直方向上的对齐格式，数据类型{@link ArkUI_VerticalAlignment}。 \n
     *
     */
    NODE_ROW_ALIGN_ITEMS = MAX_NODE_SCOPE_NUM * ARKUI_NODE_ROW,
    /**
     * @brief 设置Row子组件在水平方向上的对齐格式，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在水平方向上的对齐格式，数据类型{@link ArkUI_FlexAlignment}， \n
     * 默认值ARKUI_FLEX_ALIGNMENT_START。 \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在水平方向上的对齐格式，数据类型{@link ArkUI_FlexAlignment}。 \n
     *
     */
    NODE_ROW_JUSTIFY_CONTENT,

    /**
     * @brief 设置Flex属性，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0]?.i32：子组件在Flex容器上排列的方向{@link ArkUI_FlexDirection}，默认值为ARKUI_FLEX_DIRECTION_ROW； \n
     * .value[1]?.i32：排列规则{@link ArkUI_FlexWrap}，默认值为ARKUI_FLEX_WRAP_NO_WRAP； \n
     * .value[2]?.i32：主轴上的对齐格式{@link ArkUI_FlexAlignment}，默认值为ARKUI_FLEX_ALIGNMENT_START； \n
     * .value[3]?.i32：交叉轴上的对齐格式{@link ArkUI_ItemAlignment}，默认值为ARKUI_ITEM_ALIGNMENT_START； \n
     * .value[4]?.i32：	交叉轴中有额外的空间时，多行内容的对齐方式{@link ArkUI_FlexAlignment}，默认值为ARKUI_FLEX_ALIGNMENT_START； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：子组件在Flex容器上排列的方向的枚举值； \n
     * .value[1].i32：排列规则的枚举值； \n
     * .value[2].i32：主轴上的对齐格式的枚举值； \n
     * .value[3].i32：交叉轴上的对齐格式的枚举值； \n
     * .value[4].i32：	交叉轴中有额外的空间时，多行内容的对齐方式的枚举值； \n
     *
     */
    NODE_FLEX_OPTION = MAX_NODE_SCOPE_NUM * ARKUI_NODE_FLEX,

    /**
     * @brief 设置组件是否正在刷新，支持属性设置，属性获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32：参数类型为1或者0。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32：参数类型为1或者0。
     *
     */
    NODE_REFRESH_REFRESHING = MAX_NODE_SCOPE_NUM * ARKUI_NODE_REFRESH,
    /**
     * @brief 设置下拉区域的自定义内容，支持属性设置和重置。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .object：参数类型{@Link ArkUI_NodeHandle}。
     *
     */
    NODE_REFRESH_CONTENT,

    /**
     * @brief 定义瀑布流组件布局主轴方向，支持属性设置、重置和获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32 主轴方向，参数类型{@Link ArkUI_FlexDirection}。
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].i32 主轴方向，参数类型{@Link ArkUI_FlexDirection}。
     *
     */
    NODE_WATER_FLOW_LAYOUT_DIRECTION = MAX_NODE_SCOPE_NUM * ARKUI_NODE_WATER_FLOW,

    /**
     * @brief 设置当前瀑布流组件布局列的数量，不设置时默认1列，支持属性设置、重置和获取。
     * 例如，'1fr 1fr 2fr' 是将父组件分3列，将父组件允许的宽分为4等份，第一列占1份，第二列占1份，第三列占2份。
     * 可使用columnsTemplate('repeat(auto-fill,track-size)')根据给定的列宽track-size自动计算列数，
     * 其中repeat、auto-fill为关键字，track-size为可设置的宽度，支持的单位包括px、vp、%或有效数字，默认单位为vp。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .string: 布局列的数量.\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .string: 布局列的数量.\n
     *
     */
    NODE_WATER_FLOW_COLUMN_TEMPLATE,

    /**
     * @brief 设置当前瀑布流组件布局行的数量，不设置时默认1行，支持属性设置、重置和获取。
     * 例如，'1fr 1fr 2fr'是将父组件分三行，将父组件允许的高分为4等份，第一行占1份，第二行占一份，第三行占2份。
     * 可使用rowsTemplate('repeat(auto-fill,track-size)')根据给定的行高track-size自动计算行数，
     * 其中repeat、auto-fill为关键字，track-size为可设置的高度，支持的单位包括px、vp、%或有效数字，默认单位为vp。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .string: 布局行的数量.\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .string: 布局行的数量.\n
     *
     */
    NODE_WATER_FLOW_ROW_TEMPLATE,

    /**
     * @brief 设置列与列的间距，支持属性设置、重置和获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32: 列与列的间距, 单位vp.\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32: 列与列的间距, 单位vp.\n
     *
     */
    NODE_WATER_FLOW_COLUMN_GAP,

    /**
     * @brief 设置行与行的间距，支持属性设置、重置和获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].f32: 行与行的间距, 单位vp.\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32: 行与行的间距, 单位vp.\n
     *
     */
    NODE_WATER_FLOW_ROW_GAP,

    /**
     * @brief 设置FlowItem分组配置信息，支持属性设置、重置和获取。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .value[0].i32: 从0开始计算的索引，会转换为整数，表示要开始改变分组的位置.\n
     * .object: 参数格式为{@ArkUI_WaterFlowSectionOption}.\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .object: 返回值格式为{@ArkUI_WaterFlowSectionOption}.\n
     *
     */
    NODE_WATER_FLOW_SECTION_OPTION,

    /**
    * @brief waterFlow组件适配器，支持属性设置，属性重置和属性获取接口。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .object：使用{@link ArkUI_NodeAdapter}对象作为适配器。\n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .object: 返回值格式为{@link ArkUI_NodeAdapter}.\n
    */
    NODE_WATER_FLOW_NODE_ADAPTER,

    /**
    * @brief waterFlow组件Adapter缓存数量，支持属性设置，属性重置和属性获取接口。
    *
    * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
    * .value[0].i32：配合waterFlow组件Adapter使用，设置adapter中的缓存数量\n
    */
    NODE_WATER_FLOW_CACHED_COUNT,
    /**
     * @brief 设置当前瀑布流子组件的约束尺寸属性，组件布局时，进行尺寸范围限制，支持属性设置，属性重置和属性获取接口。
     *
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].f32：最小宽度，使用-1表示不设置； \n
     * .value[1].f32：最大宽度，使用-1表示不设置； \n
     * .value[2].f32：最小高度，使用-1表示不设置； \n
     * .value[3].f32：最大高度，使用-1表示不设置； \n
     * \n
     * 属性获取方法返回值{@link ArkUI_AttributeItem}格式： \n
     * .value[0].f32：最小宽度，使用-1表示不设置； \n
     * .value[1].f32：最大宽度，使用-1表示不设置； \n
     * .value[2].f32：最小高度，使用-1表示不设置； \n
     * .value[3].f32：最大高度，使用-1表示不设置； \n
     *
     */
    NODE_WATER_FLOW_ITEM_CONSTRAINT_SIZE,
    /**
     * @brief 设置瀑布流组件末尾的自定义显示组件。
     *
     * 属性设置方法{@link ArkUI_AttributeItem}参数格式： \n
     * .object：参数类型{@link ArkUI_NodeHandle}。
     *
     */
    NODE_WATER_FLOW_FOOTER,
    /**
     * @brief 滑动到指定index。
     * 
     * 开启smooth动效时，会对经过的所有item进行加载和布局计算，当大量加载item时会导致性能问题。\n
     * \n
     * 属性设置方法参数{@link ArkUI_AttributeItem}格式：\n
     * .value[0].i32：要滑动到的目标元素在当前容器中的索引值。\n
     * .value[1]?.i32：设置滑动到列表项在列表中的索引值时是否有动效，1表示有动效，0表示没有动效。默认值：0。\n
     * .value[2]?.i32：指定滑动到的元素与当前容器的对齐方式，参数类型{@link ArkUI_ScrollAlignment}。默认值为：ARKUI_SCROLL_ALIGNMENT_START。\n
     *
     */
    NODE_WATER_FLOW_SCROLL_TO_INDEX,
} ArkUI_NodeAttributeType;

#define MAX_COMPONENT_EVENT_ARG_NUM 12
/**
 * @brief 定义组件回调事件的参数类型。
 *
 * @since 12
 */
typedef struct {
    /** 数据数组对象。*/
    ArkUI_NumberValue data[MAX_COMPONENT_EVENT_ARG_NUM];
} ArkUI_NodeComponentEvent;

/**
 * @brief 定义组件回调事件使用字符串参数的类型。
 *
 * @since 12
 */
typedef struct {
    /** 字符串数据。*/
    const char* pStr;
} ArkUI_StringAsyncEvent;

/**
 * @brief 提供NativeNode组件支持的事件类型定义。
 *
 * @since 12
 */
typedef enum {
    /**
     * @brief 手势事件类型。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_UIInputEvent}。
     */
    NODE_TOUCH_EVENT = 0,

    /**
     * @brief 挂载事件。
     *
     * 触发该事件的条件 ：组件挂载显示时触发此回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。
     */
    NODE_EVENT_ON_APPEAR,
    /**
     * @brief 卸载事件。
     *
     * 触发该事件的条件 ：组件卸载时触发此回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。
     */
    NODE_EVENT_ON_DISAPPEAR,

    /**
     * @brief 组件区域变化事件
     *
     * 触发该事件的条件：组件区域变化时触发该回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含12个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>：表示过去目标元素的宽度，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>：表示过去目标元素的高度，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[2].f32</b>：表示过去目标元素左上角相对父元素左上角的位置的x轴坐标，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[3].f32</b>：表示过去目标元素左上角相对父元素左上角的位置的y轴坐标，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[4].f32</b>：表示过去目标元素目标元素左上角相对页面左上角的位置的x轴坐标，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[5].f32</b>：表示过去目标元素目标元素左上角相对页面左上角的位置的y轴坐标，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[6].f32</b>：表示最新目标元素的宽度，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[7].f32</b>：表示最新目标元素的高度，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[8].f32</b>：表示最新目标元素左上角相对父元素左上角的位置的x轴坐标，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[9].f32</b>：表示最新目标元素左上角相对父元素左上角的位置的y轴坐标，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[10].f32</b>：表示最新目标元素目标元素左上角相对页面左上角的位置的x轴坐标，类型为number，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[11].f32</b>：表示最新目标元素目标元素左上角相对页面左上角的位置的y轴坐标，类型为number，单位vp。\n
     */
    NODE_EVENT_ON_AREA_CHANGE,
    /**
     * @brief 获焦事件。
     *
     * 触发该事件的条件：组件获焦时触发此回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。
     */
    NODE_ON_FOCUS,
    /**
     * @brief 失去焦点事件。
     *
     * 触发该事件的条件：组件失去焦点时触发此回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。
     */
    NODE_ON_BLUR,
    /**
     * @brief 组件点击事件。
     *
     * 触发该事件的条件：组件被点击时触发此回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含12个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>：点击位置相对于被点击元素原始区域左上角的X坐标，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>：点击位置相对于被点击元素原始区域左上角的Y坐标，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[2].f32</b>：事件时间戳。触发事件时距离系统启动的时间间隔，单位微妙。\n
     * <b>ArkUI_NodeComponentEvent.data[3].i32</b>：事件输入设备，1表示鼠标，2表示触屏，4表示按键。\n
     * <b>ArkUI_NodeComponentEvent.data[4].f32</b>：点击位置相对于应用窗口左上角的X坐标，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[5].f32</b>：点击位置相对于应用窗口左上角的Y坐标，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[6].f32</b>：点击位置相对于应用屏幕左上角的X坐标，单位vp。\n
     * <b>ArkUI_NodeComponentEvent.data[7].f32</b>：点击位置相对于应用屏幕左上角的Y坐标，单位vp。\n
     */
    NODE_ON_CLICK,
    /**
     * @brief 组件自定义事件拦截。
     *
     * 触发该事件的条件：组件被触摸时触发此回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_UIInputEvent}。\n
     */
    NODE_ON_TOUCH_INTERCEPT,
    /**
     * @brief 组件可见区域变化事件。
     *
     * 触发该事件的条件：组件可见面积与自身面积的比值接近设置的阈值时触发回调。\n
     * 传入参数{@link ArkUI_AttributeItem}格式： \n
     * .value[0...].f32: 阈值数组，阈值表示组件可见面积与组件自身面积的比值。每个阈值的取值范围均为[0.0, 1.0]\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：组件可见面积与自身面积的比值与上次变化相比的情况，变大为1，变小为0。\n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>：触发回调时组件可见面积与自身面积的比值。\n
     */
    NODE_EVENT_ON_VISIBLE_AREA_CHANGE,
    /**
     * @brief 鼠标进入或退出组件事件。
     *
     * 触发该事件的条件：鼠标进入或退出组件时触发回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：鼠标是否悬浮在组件上，鼠标进入时为1，退出时为0。\n
     */
    NODE_ON_HOVER,
    /**
     * @brief 组件点击事件。
     *
     * 触发该事件的条件：组件被鼠标按键点击或者鼠标在组件上悬浮移动时触发该回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_UIInputEvent}。\n
     */
    NODE_ON_MOUSE,
    /**
     * @brief 文本设置TextDataDetectorConfig且识别成功时，触发onDetectResultUpdate回调。
     *
     * 触发该事件的条件：文本设置TextDataDetectorConfig且识别成功后。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_StringAsyncEvent}。\n
     * {@link ArkUI_StringAsyncEvent}中包含1个参数：\n
     * <b>ArkUI_StringAsyncEvent.pStr</b>：表示文本识别的结果，Json格式。\n
     *
     */
    NODE_TEXT_ON_DETECT_RESULT_UPDATE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TEXT,
    /**
     * @brief 图片加载成功事件。
     *
     * 触发该事件的条件 ：图片数据加载成功和解码成功均触发该回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含9个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示加载状态，0表示数据加载成功，1表示解码成功。\n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>：表示图片的宽度，单位px。\n
     * <b>ArkUI_NodeComponentEvent.data[2].f32</b>：表示图片的高度，单位px。\n
     * <b>ArkUI_NodeComponentEvent.data[3].f32</b>：表示当前组件的宽度，单位px。\n
     * <b>ArkUI_NodeComponentEvent.data[4].f32</b>：表示当前组件的高度，单位px。\n
     * <b>ArkUI_NodeComponentEvent.data[5].f32</b>：图片绘制区域相对组件X轴位置，单位px。\n
     * <b>ArkUI_NodeComponentEvent.data[6].f32</b>：图片绘制区域相对组件Y轴位置，单位px。\n
     * <b>ArkUI_NodeComponentEvent.data[7].f32</b>：图片绘制区域宽度，单位px。\n
     * <b>ArkUI_NodeComponentEvent.data[8].f32</b>：图片绘制区域高度，单位px。\n
     */
    NODE_IMAGE_ON_COMPLETE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_IMAGE,
    /**
     * @brief 图片加载失败事件。
     *
     * 触发该事件的条件：图片加载异常时触发该回调。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>错误码信息：\n
     * 401: 图片路径参数异常，无法获取到图片数据。\n
     * 103101: 图片格式不支持。\n
     */
    NODE_IMAGE_ON_ERROR,
    /**
     * @brief SVG图片动效播放完成事件。
     *
     * 触发该事件的条件：带动效的SVG图片动画结束时触发。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。
     */
    NODE_IMAGE_ON_SVG_PLAY_FINISH,
    /**
     * @brief 开关状态发生变化时触发给事件。
     *
     * 触发该事件的条件：开关状态发生变化。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：当前开关状态，1表示开，0表示关。
     *
     */
    NODE_TOGGLE_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TOGGLE,
    /**
     * @brief textInput输入内容发生变化时触发该事件。
     *
     * 触发该事件的条件：输入内容发生变化时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_StringAsyncEvent}。\n
     * {@link ArkUI_StringAsyncEvent}中包含1个参数：\n
     * <b>ArkUI_StringAsyncEvent.pStr</b>：输入的文本内容。
     *
     */
    NODE_TEXT_INPUT_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TEXT_INPUT,
    /**
     * @brief textInput按下输入法回车键触发该事件。
     *
     * 触发该事件的条件：按下输入法回车键。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：输入法回车键类型。
     *
     */
    NODE_TEXT_INPUT_ON_SUBMIT,
    /**
     * @brief 长按输入框内部区域弹出剪贴板后，点击剪切板剪切按钮，触发该回调。
     *
     * 触发该事件的条件：长按输入框内部区域弹出剪贴板后，点击剪切板剪切按钮。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_StringAsyncEvent}。\n
     * {@link ArkUI_StringAsyncEvent}中包含1个参数：\n
     * <b>ArkUI_StringAsyncEvent.pStr</b>：剪切的文本内容。
     *
     */
    NODE_TEXT_INPUT_ON_CUT,
    /**
     * @brief 长按输入框内部区域弹出剪贴板后，点击剪切板粘贴按钮，触发该回调。
     *
     * 触发该事件的条件：长按输入框内部区域弹出剪贴板后，点击剪切板粘贴按钮。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_StringAsyncEvent}。\n
     * {@link ArkUI_StringAsyncEvent}中包含1个参数：\n
     * <b>ArkUI_StringAsyncEvent.pStr</b>：粘贴的文本内容。
     *
     */
    NODE_TEXT_INPUT_ON_PASTE,
    /**
     * @brief 文本选择的位置发生变化时，触发该回调。
     *
     * 触发该事件的条件：文本选择的位置发生变化时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示所选文本的起始位置。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：表示所选文本的结束位置。\n
     *
     */
    NODE_TEXT_INPUT_ON_TEXT_SELECTION_CHANGE,

     /**
     * @brief 输入状态变化时，触发该回调。
     *
     * 触发该事件的条件：输入状态变化时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示true表示正在输入。\n
     *
     */
    NODE_TEXT_INPUT_ON_EDIT_CHANGE,

    /**
     * @brief 设置NODE_TEXT_INPUT_INPUT_FILTER，正则匹配失败时触发。
     *
     * 触发该事件的条件：正则匹配失败时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_StringAsyncEvent}。\n
     * {@link ArkUI_StringAsyncEvent}中包含1个参数：\n
     * <b>ArkUI_StringAsyncEvent.pStr</b>：表示正则匹配失败时，被过滤的内容。\n
     *
     */
    NODE_TEXT_INPUT_ON_INPUT_FILTER_ERROR,
    /**
     * @brief 文本内容滚动时，触发该回调。
     *
     * 触发该事件的条件：文本内容滚动时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示文本在内容区的横坐标偏移。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：表示文本在内容区的纵坐标偏移。\n
     *
     */
    NODE_TEXT_INPUT_ON_CONTENT_SCROLL,
    /**
     * @brief 输入内容发生变化时，触发该回调。
     *
     * 触发该事件的条件：输入内容发生变化时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_StringAsyncEvent}。\n
     * {@link ArkUI_StringAsyncEvent}中包含1个参数：\n
     * <b>ArkUI_StringAsyncEvent.pStr</b>：当前输入的文本内容。
     *
     */
    NODE_TEXT_AREA_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TEXT_AREA,
    /**
     * @brief 长按输入框内部区域弹出剪贴板后，点击剪切板粘贴按钮，触发该回调。
     *
     * 触发该事件的条件：长按输入框内部区域弹出剪贴板后，点击剪切板粘贴按钮。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_StringAsyncEvent}。\n
     * {@link ArkUI_StringAsyncEvent}中包含1个参数：\n
     * <b>ArkUI_StringAsyncEvent.pStr</b>：粘贴的文本内容。
     *
     */
    NODE_TEXT_AREA_ON_PASTE,
    /**
     * @brief 文本选择的位置发生变化时，触发该回调。
     *
     * 触发该事件的条件：文本选择的位置发生变化时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示所选文本的起始位置。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：表示所选文本的结束位置。\n
     *
     */
    NODE_TEXT_AREA_ON_TEXT_SELECTION_CHANGE,

    /**
     * @brief 输入状态变化时，触发该回调。
     *
     * 触发该事件的条件：输入状态变化时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示true表示正在输入。\n
     *
     */
    NODE_TEXT_AREA_ON_EDIT_CHANGE,

    /**
     * @brief textArea按下输入法回车键触发该事件。
     *
     * 触发该事件的条件：按下输入法回车键。keyType为ARKUI_ENTER_KEY_TYPE_NEW_LINE时不触发\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：输入法回车键类型。
     *
     */
    NODE_TEXT_AREA_ON_SUBMIT,
    /**
     * @brief 设置NODE_TEXT_AREA_INPUT_FILTER，正则匹配失败时触发。
     *
     * 触发该事件的条件：正则匹配失败时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_StringAsyncEvent}。\n
     * {@link ArkUI_StringAsyncEvent}中包含1个参数：\n
     * <b>ArkUI_StringAsyncEvent.pStr</b>：表示正则匹配失败时，被过滤的内容。\n
     *
     */
    NODE_TEXT_AREA_ON_INPUT_FILTER_ERROR,
    /**
     * @brief 文本内容滚动时，触发该回调。
     *
     * 触发该事件的条件：文本内容滚动时。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示文本在内容区的横坐标偏移。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：表示文本在内容区的纵坐标偏移。\n
     *
     */
    NODE_TEXT_AREA_ON_CONTENT_SCROLL,

    /**
     * @brief 定义ARKUI_NODE_CHECKBOX当选中状态发生变化时，触发该回调。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>1:表示已选中, 0: 表示未选中\n
     */
    NODE_CHECKBOX_EVENT_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_CHECKBOX,

    /**
     * @brief 定义ARKUI_NODE_DATE_PICKER列表组件的滚动触摸事件枚举值。
     *
     * 触发该事件的条件：选择日期时触发该事件。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含3个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示选中时间的年。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：表示选中时间的月，取值范围：[0-11]。\n
     * <b>ArkUI_NodeComponentEvent.data[2].i32</b>：表示选中时间的天。\n
     */
    NODE_DATE_PICKER_EVENT_ON_DATE_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_DATE_PICKER,

    /**
     * @brief 定义ARKUI_NODE_TIME_PICKER列表组件的滚动触摸事件枚举值。
     *
     * 触发该事件的条件：选择时间时触发该事件。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示选中时间的时，取值范围：[0-23]。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：表示选中时间的分，取值范围：[0-59]。\n
     */
    NODE_TIME_PICKER_EVENT_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TIME_PICKER,

    /**
     * @brief 定义ARKUI_NODE_TEXT_PICKER列表组件的滚动触摸事件枚举值。
     *
     * 触发该事件的条件 ：选择时间时触发该事件。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0...11].i32</b>表示选中数据的维度。\n
     */
    NODE_TEXT_PICKER_EVENT_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_TEXT_PICKER,

    /**
     * @brief 定义NODE_CALENDAR_PICKER选中日期时触发的事件。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * <b>ArkUI_NodeComponent.data[0].u32</b>选中的年。\n
     * <b>ArkUI_NodeComponent.data[1].u32</b>选中的月。\n
     * <b>ArkUI_NodeComponent.data[2].u32</b>选中的日。\n
     */
    NODE_CALENDAR_PICKER_EVENT_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_CALENDAR_PICKER,

    /**
     * @brief 定义ARKUI_NODE_SLIDER拖动或点击时触发事件回调。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>：当前滑动进度值。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：事件触发的相关状态值\n
     */
    NODE_SLIDER_EVENT_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_SLIDER,

    /**
     * @brief 定义ARKUI_NODE_RADIO拖动或点击时触发事件回调。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：单选框的状态。\n
     */
    NODE_RADIO_EVENT_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_RADIO,

    /**
     * @brief 定义ARKUI_NODE_SWIPER当前元素索引变化时触发事件回调。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示当前显示元素的索引。\n
     */
    NODE_SWIPER_EVENT_ON_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_SWIPER,

    /**
     * @brief 定义ARKUI_NODE_SWIPER切换动画开始时触发回调。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含5个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示当前显示元素的索引。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：表示切换动画目标元素的索引。\n
     * <b>ArkUI_NodeComponentEvent.data[2].f32</b>：表示主轴方向上当前显示元素相对Swiper起始位置的位移。\n
     * <b>ArkUI_NodeComponentEvent.data[3].f32</b>：表示主轴方向上目标元素相对Swiper起始位置的位移。\n
     * <b>ArkUI_NodeComponentEvent.data[4].f32</b>：表示离手速度。\n
     */
    NODE_SWIPER_EVENT_ON_ANIMATION_START,

    /**
     * @brief 定义ARKUI_NODE_SWIPER切换动画结束是触发回调。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示当前显示元素的索引。\n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>：表示主轴方向上当前显示元素相对Swiper起始位置的位移。\n
     */
    NODE_SWIPER_EVENT_ON_ANIMATION_END,

    /**
     * @brief 定义ARKUI_NODE_SWIPER在页面跟手滑动过程中，逐帧触发该回调。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：表示当前显示元素的索引。\n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>：表示主轴方向上当前显示元素相对Swiper起始位置的位移。\n
     */
    NODE_SWIPER_EVENT_ON_GESTURE_SWIPE,

    /**
     * @brief 定义滚动容器组件的滚动事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用。\n
     * 3、越界回弹。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>：表示距离上一次事件触发的X轴增量。\n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>：表示距离上一次事件触发的Y轴增量。\n
     */
    NODE_SCROLL_EVENT_ON_SCROLL = MAX_NODE_SCOPE_NUM * ARKUI_NODE_SCROLL,
    /**
     * @brief 定义滚动容器组件的滚动帧始事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动时触发，包括键鼠操作等其他触发滚动的输入设置。\n
     * 2、调用控制器接口时不触发。\n
     * 3、越界回弹不触发。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>：表示即将发生的滚动量。\n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>：表示当前滚动状态。\n
     * <b>::ArkUI_NodeComponentEvent</b>中包含1个返回值：\n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>：事件处理函数中可根据应用场景计算实际需要的滚动量并存于data[0].f32中，Scroll将按照返回值的实际滚动量进行滚动。\n
     */
    NODE_SCROLL_EVENT_ON_SCROLL_FRAME_BEGIN,
    /**
     * @brief 定义滚动容器组件的滑动前触发事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用。\n
     * 3、越界回弹。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含3个参数: \n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>: 每帧滚动的偏移量，内容向左滚动时偏移量为正，向右滚动时偏移量为负，单位vp。 \n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>: 每帧滚动的偏移量，内容向上滚动时偏移量为正，向下滚动时偏移量为负，单位vp。 \n
     * <b>ArkUI_NodeComponentEvent.data[2].i32</b>: 当前滑动状态，参数类型{@link ArkUI_ScrollState}。\n
     */
    NODE_SCROLL_EVENT_ON_WILL_SCROLL,
    /**
     * @brief 定义滚动容器组件的滑动时触发事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用。\n
     * 3、越界回弹。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含3个参数: \n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>: 每帧滚动的偏移量，内容向左滚动时偏移量为正，向右滚动时偏移量为负，单位vp。 \n
     * <b>ArkUI_NodeComponentEvent.data[1].f32</b>: 每帧滚动的偏移量，内容向上滚动时偏移量为正，向下滚动时偏移量为负，单位vp。 \n
     * <b>ArkUI_NodeComponentEvent.data[2].i32</b>: 当前滑动状态，参数类型{@link ArkUI_ScrollState}。 \n
     */
    NODE_SCROLL_EVENT_ON_DID_SCROLL,
    /**
     * @brief 定义滚动容器组件的滚动开始事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件开始滚动时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用后开始，带过渡动效。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。\n
     */
    NODE_SCROLL_EVENT_ON_SCROLL_START,
    /**
     * @brief 定义滚动容器组件的滚动停止事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动后停止，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用后停止，带过渡动效。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。\n
     */
    NODE_SCROLL_EVENT_ON_SCROLL_STOP,
    /**
     * @brief 定义滚动容器组件的滚动边缘事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件滚动到边缘时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用。\n
     * 3、越界回弹。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数。\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>表示当前碰到的是上下左右哪个边。\n
     */
    NODE_SCROLL_EVENT_ON_SCROLL_EDGE,
    /**
     * @brief 定义滚动容器组件到底末尾位置时触发回调。
     *
     * 触发该事件的条件 ：\n
     * 1、组件到底末尾位置时触发。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。\n
     */
    NODE_SCROLL_EVENT_ON_REACH_END,
    /**
     * @brief 定义滚动容器组件到达起始位置时触发回调。
     *
     * 触发该事件的条件 ：\n
     * 1、组件到达起始位置时触发。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数。\n
     */
    NODE_SCROLL_EVENT_ON_REACH_START,
    
    /**
     * @brief 定义ARKUI_NODE_LIST有子组件划入或划出List显示区域时触发事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 列表初始化时会触发一次，List显示区域内第一个子组件的索引值或最后一个子组件的索引值有变化时会触发。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含3个参数: \n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>: List显示区域内第一个子组件的索引值. \n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>: List显示区域内最后一个子组件的索引值. \n
     * <b>ArkUI_NodeComponentEvent.data[2].i32</b>: List显示区域内中间位置子组件的索引值. \n
     */
    NODE_LIST_ON_SCROLL_INDEX = MAX_NODE_SCOPE_NUM * ARKUI_NODE_LIST,
    /**
     * @brief 定义ARKUI_NODE_LIST组件的滑动前触发事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用。\n
     * 3、越界回弹。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数: \n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>: 每帧滚动的偏移量，list内容向上滚动时偏移量为正，向下滚动时偏移量为负. \n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>: 当前滑动状态. \n
     */
    NODE_LIST_ON_WILL_SCROLL,
    /**
     * @brief 定义ARKUI_NODE_LIST组件的滑动时触发事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用。\n
     * 3、越界回弹。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数: \n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>: 每帧滚动的偏移量，list内容向上滚动时偏移量为正，向下滚动时偏移量为负. \n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>: 当前滑动状态. \n
     */
    NODE_LIST_ON_DID_SCROLL,
    /**
     * @brief 定义ARKUI_NODE_REFRESH刷新状态变更触发该事件。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含1个参数：\n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>：刷新状态。\n
     */
    NODE_REFRESH_STATE_CHANGE = MAX_NODE_SCOPE_NUM * ARKUI_NODE_REFRESH,
    /**
     * @brief 定义ARKUI_NODE_REFRESH进入刷新状态时触发该事件。
     *
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中不包含参数：\n
     */
    NODE_REFRESH_ON_REFRESH,

    /**
     * @brief 定义ARKUI_NODE_WATER_FLOW组件的滑动前触发事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用。\n
     * 3、越界回弹。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数: \n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>: 每帧滚动的偏移量，内容向上滚动时偏移量为正，向下滚动时偏移量为负. \n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>: 当前滑动状态. \n
     */
    NODE_WATER_FLOW_ON_WILL_SCROLL = MAX_NODE_SCOPE_NUM * ARKUI_NODE_WATER_FLOW,
    /**
     * @brief 定义ARKUI_NODE_WATER_FLOW组件的滑动时触发事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 1、滚动组件触发滚动时触发，支持键鼠操作等其他触发滚动的输入设置。\n
     * 2、通过滚动控制器API接口调用。\n
     * 3、越界回弹。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含2个参数: \n
     * <b>ArkUI_NodeComponentEvent.data[0].f32</b>: 每帧滚动的偏移量，内容向上滚动时偏移量为正，向下滚动时偏移量为负. \n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>: 当前滑动状态. \n
     */
    NODE_WATER_FLOW_ON_DID_SCROLL,
    /**
     * @brief 定义ARKUI_NODE_WATER_FLOW当前瀑布流显示的起始位置/终止位置的子组件发生变化时触发事件枚举值。
     *
     * 触发该事件的条件 ：\n
     * 瀑布流显示区域上第一个子组件/最后一个组件的索引值有变化就会触发。\n
     * 事件回调发生时，事件参数{@link ArkUI_NodeEvent}对象中的联合体类型为{@link ArkUI_NodeComponentEvent}。\n
     * {@link ArkUI_NodeComponentEvent}中包含3个参数: \n
     * <b>ArkUI_NodeComponentEvent.data[0].i32</b>: 当前显示的WaterFlow起始位置的索引值. \n
     * <b>ArkUI_NodeComponentEvent.data[1].i32</b>: 当前显示的瀑布流终止位置的索引值. \n
     */
    NODE_WATER_FLOW_ON_SCROLL_INDEX,
} ArkUI_NodeEventType;

/**
 * @brief 定义组件事件的通用结构类型。
 *
 * @since 12
 */
typedef struct ArkUI_NodeEvent ArkUI_NodeEvent;

/**
 * @brief 获取组件事件类型。
 * 
 * @param event 组件事件指针。
 * @return ArkUI_NodeEventType 组件事件类型。
 * @since 12
 */
ArkUI_NodeEventType OH_ArkUI_NodeEvent_GetEventType(ArkUI_NodeEvent* event);

/**
 * @brief 获取事件自定义标识ID。
 *
 * 该事件id在调用{@link registerNodeEvent}函数时作为参数传递进来，可应用于同一事件入口函数{@link registerNodeEventReceiver}分发逻辑。
 *
 * @param event 组件事件指针。
 * @return int32_t 事件自定义标识ID。
 * @since 12
 */
int32_t OH_ArkUI_NodeEvent_GetTargetId(ArkUI_NodeEvent* event);

/**
 * @brief 获取触发该组件的组件对象。
 * 
 * @param event 组件事件指针。
 * @return ArkUI_NodeHandle 触发该组件的组件对象。
 * @since 12
 */
ArkUI_NodeHandle OH_ArkUI_NodeEvent_GetNodeHandle(ArkUI_NodeEvent* event);

/**
 * @brief 获取组件事件中的输入事件（如触碰事件）数据。
 * 
 * @param event 组件事件指针。
 * @return ArkUI_UIInputEvent* 输入事件数据指针。
 * @since 12
 */
ArkUI_UIInputEvent* OH_ArkUI_NodeEvent_GetInputEvent(ArkUI_NodeEvent* event);

/**
 * @brief 获取组件事件中的数字类型数据。
 * 
 * @param event 组件事件指针。
 * @return ArkUI_NodeComponentEvent* 数字类型数据指针。
 * @since 12
 */
ArkUI_NodeComponentEvent* OH_ArkUI_NodeEvent_GetNodeComponentEvent(ArkUI_NodeEvent* event);

/**
 * @brief 获取组件事件中的字符串数据。
 * 
 * @param event 组件事件指针。
 * @return ArkUI_StringAsyncEvent* 字符串数据指针。
 * @since 12
 */
ArkUI_StringAsyncEvent* OH_ArkUI_NodeEvent_GetStringAsyncEvent(ArkUI_NodeEvent* event);

/**
 * @brief 获取组件事件中的用户自定义数据。
 *
 * 该自定义参数在调用{@link registerNodeEvent}函数时作为参数传递进来，可应用于事件触发时的业务逻辑处理。
 *
 * @param event 组件事件指针。
 * @return void* 用户自定义数据指针。
 * @since 12
 */
void* OH_ArkUI_NodeEvent_GetUserData(ArkUI_NodeEvent* event);

/**
 * @brief 自定义组件调用<b>::markDirty</b>是传递的脏区标识类型。
 *
 * @since 12
 */
typedef enum {
    /**
     * @brief 重新测算大小。
     *
     * 该flag类型触发时，同时也默认会触发重新布局。
     */
    NODE_NEED_MEASURE = 1,

    /** 重新布局位置。*/
    NODE_NEED_LAYOUT,
    /** 重新进行绘制。*/
    NODE_NEED_RENDER,
} ArkUI_NodeDirtyFlag;

/**
 * @brief 定义自定义组件事件类型。
 *
 * @since 12
 */
typedef enum {
    /** measure 类型。*/
    ARKUI_NODE_CUSTOM_EVENT_ON_MEASURE = 1 << 0,
    /** layout 类型。*/
    ARKUI_NODE_CUSTOM_EVENT_ON_LAYOUT = 1 << 1,
    /** draw 类型。*/
    ARKUI_NODE_CUSTOM_EVENT_ON_DRAW = 1 << 2,
    /** foreground 类型。*/
    ARKUI_NODE_CUSTOM_EVENT_ON_FOREGROUND_DRAW = 1 << 3,
    /** overlay 类型。*/
    ARKUI_NODE_CUSTOM_EVENT_ON_OVERLAY_DRAW = 1 << 4,
} ArkUI_NodeCustomEventType;

/**
 * @brief 定义自定义组件事件的通用结构类型。
 *
 * @since 12
 */
typedef struct ArkUI_NodeCustomEvent ArkUI_NodeCustomEvent;

/**
 * @brief 定义组件适配器对象，用于滚动类组件的元素懒加载。
 *
 * @since 12
 */
typedef struct ArkUI_NodeAdapter* ArkUI_NodeAdapterHandle;

/**
 * @brief 定义适配器事件对象。
 *
 * @since 12
 */
typedef struct ArkUI_NodeAdapterEvent ArkUI_NodeAdapterEvent;

/**
 * @brief 定义节点适配器事件枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 组件和adapter关联时产生该事件。 */
    NODE_ADAPTER_EVENT_WILL_ATTACH_TO_NODE = 1,
    /** 组件和adapter取消关联时产生该事件。*/
    NODE_ADAPTER_EVENT_WILL_DETACH_FROM_NODE = 2,
    /** Adapter需要添加新元素时获取新元素的唯一标识符时产生该事件。 */
    NODE_ADAPTER_EVENT_ON_GET_NODE_ID = 3,
    /** Adapter需要添加新元素时获取新元素的内容时产生该事件。 */
    NODE_ADAPTER_EVENT_ON_ADD_NODE_TO_ADAPTER = 4,
    /** Adapter将元素移除时产生该事件。 */
    NODE_ADAPTER_EVENT_ON_REMOVE_NODE_FROM_ADAPTER = 5,
} ArkUI_NodeAdapterEventType;

/**
* @brief 创建组件适配器对象。
*
* @since 12
*/
ArkUI_NodeAdapterHandle OH_ArkUI_NodeAdapter_Create();

/**
* @brief 销毁组件适配器对象。
*
* @param handle 组件适配器对象。
* @since 12
*/
void OH_ArkUI_NodeAdapter_Dispose(ArkUI_NodeAdapterHandle handle);

/**
* @brief 设置Adapter中的元素总数。
*
* @param handle 组件适配器对象。
* @param size 元素数量。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapter_SetTotalNodeCount(ArkUI_NodeAdapterHandle handle, uint32_t size);

/**
* @brief 获取Adapter中的元素总数。
*
* @param handle 组件适配器对象。
* @return Adapter中的元素总数。
* @since 12
*/
uint32_t OH_ArkUI_NodeAdapter_GetTotalNodeCount(ArkUI_NodeAdapterHandle handle);

/**
* @brief 注册Adapter相关回调事件。
*
* @param handle 组件适配器对象。
* @param userData 自定义数据。
* @param receiver 事件接收回调。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapter_RegisterEventReceiver(
ArkUI_NodeAdapterHandle handle, void* userData, void (*receiver)(ArkUI_NodeAdapterEvent* event));

/**
* @brief 注销Adapter相关回调事件。
*
* @param handle 组件适配器对象。
* @since 12
*/
void OH_ArkUI_NodeAdapter_UnregisterEventReceiver(ArkUI_NodeAdapterHandle handle);

/**
* @brief 通知Adapter进行全量元素变化。
*
* @param handle 组件适配器对象。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapter_ReloadAllItems(ArkUI_NodeAdapterHandle handle);

/**
* @brief 通知Adapter进行局部元素变化。
*
* @param handle 组件适配器对象。
* @param startPosition 元素变化起始位置。
* @param itemCount 元素变化数量。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapter_ReloadItem(
ArkUI_NodeAdapterHandle handle, uint32_t startPosition, uint32_t itemCount);

/**
* @brief 通知Adapter进行局部元素删除。
*
* @param handle 组件适配器对象。
* @param startPosition 元素删除起始位置。
* @param itemCount 元素删除数量。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapter_RemoveItem(
ArkUI_NodeAdapterHandle handle, uint32_t startPosition, uint32_t itemCount);

/**
* @brief 通知Adapter进行局部元素插入。
*
* @param handle 组件适配器对象。
* @param startPosition 元素插入起始位置。
* @param itemCount 元素插入数量。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapter_InsertItem(
ArkUI_NodeAdapterHandle handle, uint32_t startPosition, uint32_t itemCount);

/**
* @brief 通知Adapter进行局部元素移位。
*
* @param handle 组件适配器对象。
* @param from 元素移位起始位置。
* @param to 元素移位结束位置。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapter_MoveItem(ArkUI_NodeAdapterHandle handle, uint32_t from, uint32_t to);

/**
* @brief 获取存储在Adapter中的所有元素。
*
* 接口调用会返回元素的数组对象指针，该指针指向的内存数据需要开发者手动释放。
*
* @param handle 组件适配器对象。
* @param items 适配器内节点数组。
* @param size 元素数量。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapter_GetAllItems(ArkUI_NodeAdapterHandle handle, ArkUI_NodeHandle** items, uint32_t* size);

/**
* @brief 获取注册事件时传入的自定义数据。
*
* @param event 适配器事件对象。
* @since 12
*/
void* OH_ArkUI_NodeAdapterEvent_GetUserData(ArkUI_NodeAdapterEvent* event);

/**
* @brief 获取事件类型。
*
* @param event 适配器事件对象。
* @return 事件类型。
* @since 12
*/
ArkUI_NodeAdapterEventType OH_ArkUI_NodeAdapterEvent_GetType(ArkUI_NodeAdapterEvent* event);

/**
* @brief 获取需要销毁的事件中待销毁的元素。
*
* @param event 适配器事件对象。
* @return 待销毁的元素。
* @since 12
*/
ArkUI_NodeHandle OH_ArkUI_NodeAdapterEvent_GetRemovedNode(ArkUI_NodeAdapterEvent* event);

/**
* @brief 获取适配器事件时需要操作的元素序号。
*
* @param event 适配器事件对象。
* @return 元素序号。
* @since 12
*/
uint32_t OH_ArkUI_NodeAdapterEvent_GetItemIndex(ArkUI_NodeAdapterEvent* event);

/**
* @brief 获取使用该适配器的滚动类容器节点。
*
* @param event 适配器事件对象。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
ArkUI_NodeHandle OH_ArkUI_NodeAdapterEvent_GetHostNode(ArkUI_NodeAdapterEvent* event);

/**
* @brief 设置需要新增到Adapter中的组件。
*
* @param event 适配器事件对象。
* @param node 待添加的组件。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapterEvent_SetItem(ArkUI_NodeAdapterEvent* event, ArkUI_NodeHandle node);

/**
* @brief 设置生成的组件标识。
*
* @param event 适配器事件对象。
* @param id 设置返回的组件标识。
* @return 0 - 成功。
*         401 - 函数参数异常。
* @since 12
*/
int32_t OH_ArkUI_NodeAdapterEvent_SetNodeId(ArkUI_NodeAdapterEvent* event, int32_t id);

/**
 * @brief ArkUI提供的Native侧Node类型接口集合。
 *
 * Node模块相关接口需要在主线程上调用。
 *
 * @version 1
 * @since 12
 */
typedef struct {
    /** 结构体版本。 */
    int32_t version;

    /**
     * @brief 基于{@link ArkUI_NodeType}生成对应的组件并返回组件对象指针。
     *
     * @param type 创建指定类型的UI组件节点。
     * @return 返回创建完成的组件操作指针，如果创建失败返回NULL。
     */
    ArkUI_NodeHandle (*createNode)(ArkUI_NodeType type);

    /**
     * @brief 销毁组件指针指向的组件对象。
     *
     * @param node 组件指针对象。
     */
    void (*disposeNode)(ArkUI_NodeHandle node);

    /**
     * @brief 将组件挂载到某个父节点之下。
     *
     * @param parent 父节点指针。
     * @param child 子节点指针。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106103 - 禁止对BuilderNode生成的节点，进行设置属性、重置属性、设置事件与新增或修改子节点操作。
     */
    int32_t (*addChild)(ArkUI_NodeHandle parent, ArkUI_NodeHandle child);

    /**
     * @brief 将组件从父节点中移除。
     *
     * @param parent 父节点指针。
     * @param child 子节点指针。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106103 - 禁止对BuilderNode生成的节点，进行设置属性、重置属性、设置事件与新增或修改子节点操作。
     */
    int32_t (*removeChild)(ArkUI_NodeHandle parent, ArkUI_NodeHandle child);

    /**
     * @brief 将组件挂载到某个父节点之下，挂载位置在<b>sibling</b>节点之后。
     *
     * @param parent 父节点指针。
     * @param child 子节点指针。
     * @param sibling 前一个兄弟节点指针，如果为空则插入位置在最前面。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106103 - 禁止对BuilderNode生成的节点，进行设置属性、重置属性、设置事件与新增或修改子节点操作。
     */
    int32_t (*insertChildAfter)(ArkUI_NodeHandle parent, ArkUI_NodeHandle child, ArkUI_NodeHandle sibling);

    /**
     * @brief 将组件挂载到某个父节点之下，挂载位置在<b>sibling</b>节点之前。
     *
     * @param parent 父节点指针。
     * @param child 子节点指针。
     * @param sibling 后一个兄弟节点指针，如果为空则插入位置在最后面。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106103 - 禁止对BuilderNode生成的节点，进行设置属性、重置属性、设置事件与新增或修改子节点操作。
     */
    int32_t (*insertChildBefore)(ArkUI_NodeHandle parent, ArkUI_NodeHandle child, ArkUI_NodeHandle sibling);

    /**
     * @brief 将组件挂载到某个父节点之下，挂载位置由<b>position</b>指定。
     *
     * @param parent 父节点指针。
     * @param child 子节点指针。
     * @param postion 插入位置，如果插入位置为负数或者不存在，则默认插入位置在最后面。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106103 - 禁止对BuilderNode生成的节点，进行设置属性、重置属性、设置事件与新增或修改子节点操作。
     */
    int32_t (*insertChildAt)(ArkUI_NodeHandle parent, ArkUI_NodeHandle child, int32_t position);

    /**
     * @brief 属性设置函数。
     *
     * @param node 需要设置属性的节点对象。
     * @param attribute 需要设置的属性类型。
     * @param item 需要设置的属性值。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106102 - 系统中未找到Native接口的动态实现库。
     *         106103 - 禁止对BuilderNode生成的节点，进行设置属性、重置属性、设置事件与新增或修改子节点操作。
     */
    int32_t (*setAttribute)(ArkUI_NodeHandle node, ArkUI_NodeAttributeType attribute, const ArkUI_AttributeItem* item);

    /**
     * @brief 属性获取函数。
     *
     * 该接口返回的指针是ArkUI框架内部的缓冲区指针，不需要开发者主动调用delete释放内存，但是需要在该函数下一次被调用前使用，否则可能会被其他值所覆盖。
     *
     * @param node 需要获取属性的节点对象。
     * @param attribute 需要获取的属性类型。
     * @return 当前属性类型的属性值，失败返回空指针。
     */
    const ArkUI_AttributeItem* (*getAttribute)(ArkUI_NodeHandle node, ArkUI_NodeAttributeType attribute);

    /**
     * @brief 重置属性函数。
     *
     * @param node 需要重置属性的节点对象。
     * @param attribute 需要重置的属性类型。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106102 - 系统中未找到Native接口的动态实现库。
     *         106103 - 禁止对BuilderNode生成的节点，进行设置属性、重置属性、设置事件与新增或修改子节点操作。
     */
    int32_t (*resetAttribute)(ArkUI_NodeHandle node, ArkUI_NodeAttributeType attribute);

    /**
     * @brief 注册节点事件函数。
     *
     * @param node 需要注册事件的节点对象。
     * @param eventType 需要注册的事件类型。
     * @param targetId 自定义事件ID，当事件触发时在回调参数<@link ArkUI_NodeEvent>中携带回来。
     * @param userData 自定义事件参数，当事件触发时在回调参数<@link ArkUI_NodeEvent>中携带回来。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106102 - 系统中未找到Native接口的动态实现库。
     *         106103 - 禁止对BuilderNode生成的节点，进行设置属性、重置属性、设置事件与新增或修改子节点操作。
     */
    int32_t (*registerNodeEvent)(
        ArkUI_NodeHandle node, ArkUI_NodeEventType eventType, int32_t targetId, void* userData);

    /**
     * @brief 反注册节点事件函数。
     *
     * @param node 需要反注册事件的节点对象。
     * @param eventType 需要反注册的事件类型。
     */
    void (*unregisterNodeEvent)(ArkUI_NodeHandle node, ArkUI_NodeEventType eventType);

    /**
     * @brief 注册事件回调统一入口函数。
     *
     * ArkUI框架会统一收集过程中产生的组件事件并通过注册的eventReceiver函数回调给开发者。\n
     * 重复调用时会覆盖前一次注册的函数。\n 
     * 避免直接保存ArkUI_NodeEvent对象指针，数据会在回调结束后销毁。\n
     * 如果需要和组件实例绑定，可以使用addNodeEventReceiver函数接口。\n
     *
     * @param eventReceiver 事件回调统一入口函数。
     */
    void (*registerNodeEventReceiver)(void (*eventReceiver)(ArkUI_NodeEvent* event));

    /**
     * @brief 反注册事件回调统一入口函数。
     *
     */
    void (*unregisterNodeEventReceiver)();

    /**
     * @brief 强制标记当前节点需要重新测算，布局或者绘制。
     *
     * 系统属性设置更新场景下ArkUI框架会自动标记脏区并重新执行测算，布局或者绘制，不需要开发者主动调用该函数。
     *
     * @param node 需要标记脏区的节点对象。
     * @param dirtyFlag 脏区类型。
     */
    void (*markDirty)(ArkUI_NodeHandle node, ArkUI_NodeDirtyFlag dirtyFlag);

    /**
     * @brief 获取子节点的个数。
     *
     * @param node 目标节点对象。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    uint32_t (*getTotalChildCount)(ArkUI_NodeHandle node);

    /**
     * @brief 获取子节点。
     *
     * @param node 目标节点对象。
     * @param position 子组件的位置。
     * @return 返回组件的指针，如果没有返回NULL
     */
    ArkUI_NodeHandle (*getChildAt)(ArkUI_NodeHandle node, int32_t position);

    /**
     * @brief 获取第一个子节点。
     *
     * @param node 目标节点对象。
     * @return 返回组件的指针，如果没有返回NULL
     */
    ArkUI_NodeHandle (*getFirstChild)(ArkUI_NodeHandle node);

    /**
     * @brief 获取最后一个子节点。
     *
     * @param node 目标节点对象。
     * @return 返回组件的指针，如果没有返回NULL
     */
    ArkUI_NodeHandle (*getLastChild)(ArkUI_NodeHandle node);

    /**
     * @brief 获取上一个兄弟节点。
     *
     * @param node 目标节点对象。
     * @return 返回组件的指针，如果没有返回NULL
     */
    ArkUI_NodeHandle (*getPreviousSibling)(ArkUI_NodeHandle node);

    /**
     * @brief 获取下一个兄弟节点。
     *
     * @param node 目标节点对象。
     * @return 返回组件的指针，如果没有返回NULL
     */
    ArkUI_NodeHandle (*getNextSibling)(ArkUI_NodeHandle node);

    /**
     * @brief 注册自定义节点事件函数。事件触发时通过registerNodeCustomEventReceiver注册的自定义事件入口函数返回。
     *
     * @param node 需要注册事件的节点对象。
     * @param eventType 需要注册的事件类型。
     * @param targetId 自定义事件ID，当事件触发时在回调参数<@link ArkUI_NodeCustomEvent>中携带回来。
     * @param userData 自定义事件参数，当事件触发时在回调参数<@link ArkUI_NodeCustomEvent>中携带回来。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     *         106102 - 系统中未找到Native接口的动态实现库。
     */
    int32_t (*registerNodeCustomEvent)(
        ArkUI_NodeHandle node, ArkUI_NodeCustomEventType eventType, int32_t targetId, void* userData);

    /**
     * @brief 反注册自定义节点事件函数。
     *
     * @param node 需要反注册事件的节点对象。
     * @param eventType 需要反注册的事件类型。
     */
    void (*unregisterNodeCustomEvent)(ArkUI_NodeHandle node, ArkUI_NodeCustomEventType eventType);

    /**
     * @brief 注册自定义节点事件回调统一入口函数。
     *
     * ArkUI框架会统一收集过程中产生的自定义组件事件并通过注册的registerNodeCustomEventReceiver函数回调给开发者。\n
     * 重复调用时会覆盖前一次注册的函数。\n
     * 避免直接保存ArkUI_NodeCustomEvent对象指针，数据会在回调结束后销毁。\n
     * 如果需要和组件实例绑定，可以使用addNodeCustomEventReceiver函数接口。\n
     *
     * @param eventReceiver 事件回调统一入口函数。
     */
    void (*registerNodeCustomEventReceiver)(void (*eventReceiver)(ArkUI_NodeCustomEvent* event));

    /**
     * @brief 反注册自定义节点事件回调统一入口函数。
     *
     */
    void (*unregisterNodeCustomEventReceiver)();

    /**
     * @brief 在测算回调函数中设置组件的测算完成后的宽和高。
     *
     * @param node 目标节点对象。
     * @param width 设置的宽。
     * @param height 设置的高。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*setMeasuredSize)(ArkUI_NodeHandle node, int32_t width, int32_t height);

    /**
     * @brief 在布局回调函数中设置组件的位置。
     *
     * @param node 目标节点对象。
     * @param positionX x轴坐标。
     * @param positionY y轴坐标。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*setLayoutPosition)(ArkUI_NodeHandle node, int32_t positionX, int32_t positionY);

    /**
     * @brief 获取组件测算完成后的宽高尺寸。
     *
     * @param node 目标节点对象。
     * @return ArkUI_IntSize 组件的宽高。
     */
    ArkUI_IntSize (*getMeasuredSize)(ArkUI_NodeHandle node);

    /**
     * @brief 获取组件布局完成后的位置。
     *
     * @param node 目标节点对象。
     * @return ArkUI_IntOffset 组件的位置。
     */
    ArkUI_IntOffset (*getLayoutPosition)(ArkUI_NodeHandle node);

    /**
     * @brief 对特定组件进行测算，可以通过getMeasuredSize接口获取测算后的大小。
     *
     * @param node 目标节点对象。
     * @param Constraint 约束尺寸。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*measureNode)(ArkUI_NodeHandle node, ArkUI_LayoutConstraint* Constraint);

    /**
     * @brief 对特定组件进行布局并传递该组件相对父组件的期望位置。
     *
     * @param node 目标节点对象。
     * @param positionX x轴坐标。
     * @param positionY y轴坐标。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*layoutNode)(ArkUI_NodeHandle node, int32_t positionX, int32_t positionY);

    /**
     * @brief 在组件上添加组件事件回调函数，用于接受该组件产生的组件事件。
     *
     * 不同于registerNodeEventReceiver的全局注册函数，该函数允许在同一个组件上添加多个事件接受器。\n
     * 该函数添加的监听回调函数触发时机会先与registerNodeEventReceiver注册的全局回调函数。\n
     * 避免直接保存ArkUI_NodeEvent对象指针，数据会在回调结束后销毁。\n
     *
     * @param node 用于添加组件事件回调函数的对象。
     * @param eventReceiver 组件事件回调函数。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*addNodeEventReceiver)(ArkUI_NodeHandle node, void (*eventReceiver)(ArkUI_NodeEvent* event));

    /**
     * @brief 在组件上删除注册的组件事件回调函数。
     *
     * @param node 用于删除组件事件回调函数的对象。
     * @param eventReceiver 待删除的组件事件回调函数。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*removeNodeEventReceiver)(ArkUI_NodeHandle node, void (*eventReceiver)(ArkUI_NodeEvent* event));

    /**
     * @brief 在组件上添加自定义事件回调函数，用于接受该组件产生的自定义事件（如布局事件，绘制事件）。
     *
     * 不同于registerNodeCustomEventReceiver的全局注册函数，该函数允许在同一个组件上添加多个事件接受器。\n
     * 该函数添加的监听回调函数触发时机会先与registerNodeCustomEventReceiver注册的全局回调函数。\n
     * 避免直接保存ArkUI_NodeCustomEvent对象指针，数据会在回调结束后销毁。\n
     *
     * @param node 用于添加组件自定义事件回调函数的对象。
     * @param eventReceiver 组件自定义事件回调函数。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*addNodeCustomEventReceiver)(ArkUI_NodeHandle node, void (*eventReceiver)(ArkUI_NodeCustomEvent* event));

    /**
     * @brief 在组件上删除注册的自定义事件回调函数。
     *
     * @param node 用于删除组件自定义事件回调函数的对象。
     * @param eventReceiver 待删除的组件自定义事件回调函数。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*removeNodeCustomEventReceiver)(
        ArkUI_NodeHandle node, void (*eventReceiver)(ArkUI_NodeCustomEvent* event));

    /**
     * @brief 在组件上保存自定义数据。
     *
     * @param node 用于保存自定义数据的组件。
     * @param userData 要保存的自定义数据。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*setUserData)(ArkUI_NodeHandle node, void* userData);

    /**
     * @brief 获取在组件上保存的自定义数据。
     *
     * @param node 保存了自定义数据的组件。
     * @return 自定义数据。
     */
    void* (*getUserData)(ArkUI_NodeHandle node);

    /**
     * @brief 指定组件的单位。
     *
     * @param node 用于指定单位的组件。
     * @param unit 单位类型{@link ArkUI_LengthMetricUnit}，默认为 ARKUI_LENGTH_METRIC_UNIT_DEFAULT。
     * @return 0 - 成功。
     *         401 - 函数参数异常。
     */
    int32_t (*setLengthMetricUnit)(ArkUI_NodeHandle node, ArkUI_LengthMetricUnit unit);
} ArkUI_NativeNodeAPI_1;

/**
* @brief 通过自定义组件事件获取测算过程中的约束尺寸。
*
* @param event 自定义组件事件。
* @return  约束尺寸指针。
* @since 12
*/
ArkUI_LayoutConstraint* OH_ArkUI_NodeCustomEvent_GetLayoutConstraintInMeasure(ArkUI_NodeCustomEvent* event);

/**
* @brief 通过自定义组件事件获取在布局阶段期望自身相对父组件的位置。
*
* @param event 自定义组件事件。
* @return  期望自身相对父组件的位置。
* @since 12
*/
ArkUI_IntOffset OH_ArkUI_NodeCustomEvent_GetPositionInLayout(ArkUI_NodeCustomEvent* event);

/**
* @brief 通过自定义组件事件获取绘制上下文。
*
* @param event 自定义组件事件。
* @return  绘制上下文。
* @since 12
*/
ArkUI_DrawContext* OH_ArkUI_NodeCustomEvent_GetDrawContextInDraw(ArkUI_NodeCustomEvent* event);

/**
* @brief 通过自定义组件事件获取自定义事件ID。
*
* @param event 自定义组件事件。
* @return  自定义事件ID。
* @since 12
*/
int32_t OH_ArkUI_NodeCustomEvent_GetEventTargetId(ArkUI_NodeCustomEvent* event);

/**
* @brief 通过自定义组件事件获取自定义事件参数。
*
* @param event 自定义组件事件。
* @return  自定义事件参数。
* @since 12
*/
void* OH_ArkUI_NodeCustomEvent_GetUserData(ArkUI_NodeCustomEvent* event);

/**
* @brief 通过自定义组件事件获取组件对象。
*
* @param event 自定义组件事件。
* @return  组件对象。
* @since 12
*/
ArkUI_NodeHandle OH_ArkUI_NodeCustomEvent_GetNodeHandle(ArkUI_NodeCustomEvent* event);

/**
* @brief 通过自定义组件事件获取事件类型。
*
* @param event 自定义组件事件。
* @return  组件自定义事件类型。
* @since 12
*/
ArkUI_NodeCustomEventType OH_ArkUI_NodeCustomEvent_GetEventType(ArkUI_NodeCustomEvent* event);

#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_NODE_H
/** @}*/
