/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_INPUT_MANAGER_H
#define OH_INPUT_MANAGER_H

/**
 * @addtogroup input
 * @{
 *
 * @brief 提供多模态输入域的C接口。
 *
 * @since 12
 */

/**
 * @file oh_input_manager.h
 *
 * @brief 提供事件注入和关键状态查询等功能。
 *
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @library liboh_input.so
 * @since 12
 */

#include <stdint.h>

#include "oh_key_code.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 按键状态的枚举值。
 *
 * @since 12
 */
enum Input_KeyStateAction {
    /** 默认状态 */
    KEY_DEFAULT = -1,
    /** 按键按下 */
    KEY_PRESSED = 0,
    /** 按键抬起 */
    KEY_RELEASED = 1,
    /** 按键开关使能 */
    KEY_SWITCH_ON = 2,
    /** 按键开关去使能 */
    KEY_SWITCH_OFF = 3
};

/**
 * @brief 定义按键信息，用于标识按键行为。例如，“Ctrl键”信息中包含按键键值和按键状态。
 *
 * @since 12
 */
struct Input_KeyState;

/**
 * @brief 错误码。
 *
 * @since 12
 */
typedef enum {
    /** 操作成功 */
    INPUT_SUCCESS = 0,
    /** 权限验证失败 */
    INPUT_PERMISSION_DENIED = 201,
    /** 非系统应用 */
    INPUT_NOT_SYSTEM_APPLICATION = 202,
    /** 参数检查失败 */
    INPUT_PARAMETER_ERROR = 401
} Input_Result;

/**
 * @brief 查询按键状态的枚举对象。
 *
 * @param keyState 按键状态的枚举对象。
 *
 * @return 如果操作成功，@return返回{@Link Input_Result#INPUT_SUCCESS}; 
 * 否则返回{@Link Input_Result}中定义的其他错误代码。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetKeyState(struct Input_KeyState* keyState);

/**
 * @brief 创建按键状态的枚举对象。
 *
 * @return 如果操作成功，@return返回一个{@link Input_KeyState}指针对象
 * 否则返回空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_KeyState* OH_Input_CreateKeyState();

/**
 * @brief 销毁按键状态的枚举对象。
 *
 * @param keyState 按键状态的枚举对象。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyKeyState(struct Input_KeyState* keyState);

/**
 * @brief 设置按键状态对象的键值。
 *
 * @param keyState 按键状态的枚举对象。
 * @param keyCode 按键键值。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyCode(struct Input_KeyState* keyState, int32_t keyCode);

/**
 * @brief 获取按键状态对象的键值。
 * 
 * @param keyState 按键状态的枚举对象。
 * @return 返回按键状态对象的键值。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyCode(struct Input_KeyState* keyState);

/**
 * @brief 设置按键状态对象的按键是否按下。
 * 
 * @param keyState 按键状态的枚举对象。
 * @param keyAction 按键是否按下。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyPressed(struct Input_KeyState* keyState, int32_t keyAction);

/**
 * @brief 获取按键状态对象的按键是否按下。
 * 
 * @param keyState 按键状态的枚举对象。
 * @return 返回按键状态对象的按键按下状态。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyPressed(struct Input_KeyState* keyState);

/**
 * @brief 设置按键状态对象的按键开关。
 * 
 * @param keyState 按键状态的枚举对象。
 * @param keySwitch 按键开关。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeySwitch(struct Input_KeyState* keyState, int32_t keySwitch);

/**
 * @brief 获取按键状态对象的按键开关。
 * 
 * @param keyState 按键状态的枚举对象。
 * @return 返回按键状态对象的按键开关。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeySwitch(struct Input_KeyState* keyState);

#ifdef __cplusplus
}
#endif
/** @} */

#endif /* OH_INPUT_MANAGER_H */
