/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_DESCRIPTOR_H
#define OHOS_AVSESSION_DESCRIPTOR_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief 音视频媒体会话，提供系统内媒体的统一控制能力。
 *
 * 功能包括媒体会话，媒体会话管理，媒体会话控制。
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_descriptor.h
 *
 * @brief 会话的相关描述信息。
 *
 * @since 9
 * @version 1.0
 */

#include "parcel.h"
#include "element_name.h"

namespace OHOS::AVSession {
/**
 * @brief 描述分布式设备的相关信息。
 *
 * @since 9
 * @version 1.0
 */
struct OutputDeviceInfo {
    /** 是否连接 */
    bool isRemote_ {};
    /** 分布式设备的id集合 */
    std::vector<std::string> deviceIds_;
    /** 分布式设备的名称集合 */
    std::vector<std::string> deviceNames_;
};

/**
 * @brief 会话的相关描述信息。
 *
 * @since 9
 * @version 1.0
 */
struct AVSessionDescriptor {
    /**
    * @brief 将会话相关描述信息写进包里。
    *
    * @param out 写入的会话相关描述信息对象{@link Parcel}。
    * @return 成功返回true；失败则返回false。
    * @see ReadFromParcel
    * @since 9
    * @version 1.0
    */
    bool WriteToParcel(Parcel& out) const;
    /**
    * @brief 对会话相关描述信息进行解包。
    *
    * @param in 读出的会话相关描述信息对象{@link Parcel}。
    * @return 成功返回true；失败则返回false。
    * @see WriteToParcel
    * @since 9
    * @version 1.0
    */
    bool ReadFromParcel(Parcel &in);
    /** 会话的id */
    std::string sessionId_;
    /** 会话的类型 */
    int32_t sessionType_ {};
    /** 会话的自定义名称 */
    std::string sessionTag_;
    /** 会话所属应用的信息包含bundleName，abilityName等 */
    AppExecFwk::ElementName elementName_;
    /** 进程id */
    pid_t pid_ {};
    /** 用户id */
    pid_t uid_ {};
    /** 会话是否为激活状态 */
    bool isActive_ {};
    /** 会话是否是最新的会话 */
    bool isTopSession_ {};
    /** 是否是第三方应用 */
    bool isThirdPartyApp_ {};
    /** 分布式设备相关信息 */
    OutputDeviceInfo outputDeviceInfo_;
};

/**
 * @brief 会话基础信息描述。
 *
 * @since 9
 * @version 1.0
 */
struct AVSessionBasicInfo {
    /** 设备名称 */
    std::string deviceName_ {};
    /** 设备id */
    std::string networkId_ {};
    /** 供应商id */
    std::string vendorId_ {};
    /** 设备类型 */
    std::string deviceType_ {};
    /** 系统版本 */
    std::string systemVersion_ {};
    /** 会话版本 */
    int32_t sessionVersion_ {};
    /** 备注信息*/
    std::vector<int32_t> reserve_ {};
    /** 特征信息 */
    std::vector<int32_t> feature_ {};
    /** 会话元数据 */
    std::vector<int32_t> metaDataCap_ {};
    /** 支持播放状态数组 */
    std::vector<int32_t> playBackStateCap_ {};
    /** 系统控制命令 */
    std::vector<int32_t> controlCommandCap_ {};
    /** 扩展能力 */
    std::vector<int32_t> extendCapability_ {};
    /** 系统时间 */
    int32_t systemTime_ {};
    /** 扩展信息 */
    std::vector<int32_t> extend_ {};
};
}  // namespace OHOS::AVSession
#endif // OHOS_AVSESSION_DESCRIPTOR_H