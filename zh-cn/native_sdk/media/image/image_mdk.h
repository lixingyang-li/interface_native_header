/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供image接口的访问。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 2.0
 */

/**
 * @file image_mdk.h
 *
 * @brief 声明访问图像剪辑矩形、大小、格式和组件数据的函数。
 *
 * @since 10
 * @version 2.0
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_MDK_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_MDK_H_
#include <cstdint>
#include "napi/native_api.h"
#include "image_mdk_common.h"
namespace OHOS {
namespace Media {
#ifdef __cplusplus
extern "C" {
#endif

struct ImageNative_;

/**
 * @brief 为图像接口定义native层图像对象。
 *
 * @since 10
 * @version 2.0
 */
typedef struct ImageNative_ ImageNative;

/**
 * @brief 图像格式枚举值。
 *
 * @since 10
 * @version 2.0
 */
enum {
    /** YCBCR422 semi-planar 格式 */
    OHOS_IMAGE_FORMAT_YCBCR_422_SP = 1000,
    /** JPEG 编码格式 */
    OHOS_IMAGE_FORMAT_JPEG = 2000
};

/**
 * @brief 图像组成类型枚举值。
 *
 * @since 10
 * @version 2.0
 */
enum {
    /** 亮度信息 */
    OHOS_IMAGE_COMPONENT_FORMAT_YUV_Y = 1,
    /** 色度信息 */
    OHOS_IMAGE_COMPONENT_FORMAT_YUV_U = 2,
    /** 色差值信息 */
    OHOS_IMAGE_COMPONENT_FORMAT_YUV_V = 3,
    /** Jpeg 格式 */
    OHOS_IMAGE_COMPONENT_FORMAT_JPEG = 4,
};

/**
 * @brief 定义图像矩形信息。
 *
 * @since 10
 * @version 2.0
 */
struct OhosImageRect {
    /** 矩形x坐标值 */
    int32_t x;
    /** 矩形y坐标值 */
    int32_t y;
    /** 矩形宽度值，用pixels表示 */
    int32_t width;
    /** 矩形高度值，用pixels表示 */
    int32_t height;
};

/**
 * @brief 定义图像组成信息。
 *
 * @since 10
 * @version 2.0
 */
struct OhosImageComponent {
    /** 像素数据地址 */
    uint8_t* byteBuffer;
    /** 内存中的像素数据大小 */
    size_t size;
    /** 像素数据类型 */
    int32_t componentType;
    /** 像素数据行宽 */
    int32_t rowStride;
    /** 像素数据的像素大小 */
    int32_t pixelStride;
};

/**
 * @brief 从输入的JavaScript Native API <b>图像</b> 对象中解析 native {@link ImageNative} 对象。
 *
 * @param env 表示指向 JNI 环境的指针。
 * @param source 表示 JavaScript Native API <b>图像</b> 对象。
 * @return 如果操作成果返回 {@link ImageNative} 指针对象，如果操作失败返回空指针。
 * @see ImageNative, OH_Image_Release
 * @since 10
 * @version 2.0
 */
ImageNative* OH_Image_InitImageNative(napi_env env, napi_value source);

/**
 * @brief 获取native {@link ImageNative} 对象 {@link OhosImageRect} 信息。
 *
 * @param native 表示指向 {@link ImageNative} native层对象的指针。
 * @param rect 表示作为转换结果的 {@link OhosImageRect} 对象指针。
 * @return 如果操作成功返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果JNI环境异常返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_SURFACE_GET_PARAMETER_FAILED {@link IRNdkErrCode}；
 * 如果参数错误返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}。
 * @see ImageNative, OhosImageRect
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_ClipRect(const ImageNative* native, struct OhosImageRect* rect);

/**
 * @brief 获取native {@link ImageNative} 对象的 {@link OhosImageSize} 信息。
 *
 * @param native 表示 {@link ImageNative} native对象的指针。
 * @param size 表示作为转换结果的 {@link OhosImageSize} 对象的指针。
 * @return 如果操作成功返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果JNI环境异常返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_SURFACE_GET_PARAMETER_FAILED {@link IRNdkErrCode}；
 * 如果参数错误返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}。
 * @see ImageNative, OhosImageSize
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Size(const ImageNative* native, struct OhosImageSize* size);

/**
 * @brief 获取native {@link ImageNative} 对象的图像格式。
 *
 * @param native 表示 {@link ImageNative} native对象的指针。
 * @param format 表示作为转换结果的图像格式对象的指针。
 * @return 如果操作成功返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果JNI环境异常返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_SURFACE_GET_PARAMETER_FAILED {@link IRNdkErrCode}；
 * 如果参数错误返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}。
 * @see ImageNative
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Format(const ImageNative* native, int32_t* format);

/**
 * @brief 从 native {@link ImageNative} 对象中获取 {@link OhosImageComponent}。
 *
 * @param native 表示 {@link ImageNative} native对象的指针。
 * @param componentType 表示所需组件的组件类型。
 * @param componentNative 表示转换结果的 {@link OhosImageComponent} 对象的指针。
 * @return 如果操作成功返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果JNI环境异常返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_SURFACE_GET_PARAMETER_FAILED {@link IRNdkErrCode}；
 * 如果参数错误返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}。
 * @see ImageNative, OhosImageComponent
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_GetComponent(const ImageNative* native,
    int32_t componentType, struct OhosImageComponent* componentNative);

/**
 * @brief 释放 {@link ImageNative} native对象。
 * Note: 这个方法无法释放 JavaScript Native API <b>Image</b> 对象，
 * 而是释放被 {@link OH_Image_InitImageNative} 解析的 {@link ImageNative} native 对象。
 *
 * @param native 表示 {@link ImageNative} native对象的指针。
 * @return 如果操作成功返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果JNI环境异常返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果参数错误返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}。
 * @see ImageNative, OH_Image_InitImageNative
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Release(ImageNative* native);
#ifdef __cplusplus
};
#endif
/** @} */
} // namespace Media
} // namespace OHOS
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_MDK_H_
