/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OS_ACCOUNT_H
#define OS_ACCOUNT_H

/**
 * @addtogroup OsAccount
 * @{
 *
 * @brief 描述OsAccount向应用提供系统帐号能力。
 * @since 12
 */
/**
 * @file os_account.h
 *
 * @brief 声明访问和管理系统帐号信息的API。
 * @library libos_account.so
 * @syscap SystemCapability.Account.OsAccount
 * @since 12
 */

#include <stddef.h>
#include "os_account_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 获取调用方进程所属的系统帐号的名称。
 *
 * @param buffer 系统帐号名称字符数组，应该为名称和结束字符('\0')留出空间。
 * @param buffer_size 系统帐号名称字符数组的大小。
 * @return 返回OS_ACCOUNT_ERR_OK表示成功；返回OS_ACCOUNT_ERR_INTERNAL_ERROR表示内部错误；
 *         返回OS_ACCOUNT_ERR_INVALID_PARAMETER指示buf为NULL指针或名称的大小(包括结束字符('\0'))大于buffer_size。
 * @syscap SystemCapability.Account.OsAccount
 * @since 12
 */
OsAccount_ErrCode OH_OsAccount_GetName(char *buffer, size_t buffer_size);

#ifdef __cplusplus
};
#endif

/** @} */
#endif /* OS_ACCOUNT_H */